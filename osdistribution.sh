# Check if Raspbian, Ubuntu, others
CHECKPLATFORM="Others"
if [ -f "/etc/os-release" ]
then
        source /etc/os-release
        if [ "$ID" = "raspbian" ]
        then
                CHECKPLATFORM="Raspbian"
        elif [ "$ID" = "ubuntu" ]
        then
                CHECKPLATFORM="Ubuntu"
        fi
fi


