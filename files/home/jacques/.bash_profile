# .bash_profile
# Contain things that need to be defined at login time only.

# Run the .bashrc if it is readable
[ -r ~/.bashrc ] && . ~/.bashrc

# Permit Group to change files.
umask 0002

# User specific environment and startup programs
export BASH_ENV="$HOME/.bashrc"
[ -d $HOME/.local/bin ] && export PATH="$PATH:$HOME/.local/bin"

# Define Main Development Directories
if [ -x /opt/sa/bin/sa_def_env.sh ] ; then . /opt/sa/bin/sa_def_env.sh ; fi

# Load Private Key with Keychain and list them
[ -r $HOME/.ssh/id_rsa ]            && /usr/bin/keychain $HOME/.ssh/id_rsa 
[ -r $HOME/.ssh/id_ecdsa ]          && /usr/bin/keychain $HOME/.ssh/id_ecdsa
[ -r $HOME/.ssh/id_ed25519 ]        && /usr/bin/keychain $HOME/.ssh/id_ed25519
[ -r $HOME/.ssh/id_rsa_linternux ]  && /usr/bin/keychain $HOME/.ssh/id_rsa_linternux
[ -f $HOME/.keychain/$HOSTNAME-sh ] && . $HOME/.keychain/$HOSTNAME-sh
ssh-add -l

# Display O/S Logo and SysInfo
[ -x /usr/bin/fastfetch ] && /usr/bin/fastfetch
