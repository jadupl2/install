set tabstop=4
set autoindent
set ruler laststatus=4
set cursorline
set showmatch
set incsearch
set hlsearch
au bufnewfile *.sh 0r /home/jacques/.vim/sh_header.temp
