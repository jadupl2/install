# Contain things you want set in every shell you open.
# Like Aliases, functions, ...
# J.Duplessis - Juin 2023 
# 2023_06_24 bashrc v1.0 Common .bashrc - Initial Version
#@2023_10_10 bashrc v1.1 Shorter Revise version
# --------------------------------------------------------------------------------------------------

# Source global definitions
if [ -f /etc/bashrc ]; then . /etc/bashrc ; fi

# Setup Linux Prompt
case "$TERM" in 
  xterm-color|*-256color)
    PS1="\[\033[01;31m\]\u\[\033[00m\]@\[\033[01;33m\]\h\[\033[00m\]:\[\033[01;37m\]\w\[\033[00m\]"
    ;;
  vt100) 
    PS1="\[\033[01;31m\]\u\[\033[00m\]@\[\033[01;34m\]\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]"
    ;;
  *) 
    PS1="\u@\h:\w"
    ;;
esac
if [ "$(whoami)" = 'root' ] ; then export PS1="${PS1} # " ; else export PS1="${PS1} $ " ; fi


# Command line like vi editor
#set -o vi
export EDITOR="/usr/bin/vim"
export SUDO_EDITOR="$EDITOR"

# Include date and Time in History file
export HISTTIMEFORMAT="%d/%m/%y %T "
export HISTFILESIZE=20000
export HISTSIZE=10000

# User specific aliases and functions
alias bc='bc -l'
alias os='lsb_release -a'
alias ls='ls --color'
alias lsd='ls -l | grep "^d"'
alias ll='ls --color -ltr'
alias la='ls --color -ltra'
alias rm='rm -I --preserve-root'
alias chown='chown --preserve-root'
alias chmod='chmod --preserve-root'
alias chgrp='chgrp --preserve-root'
alias ports='netstat -tulanp'
alias c='clear'
alias grpe='grep --color=auto'
alias grep='grep --color=auto'
alias myip="ip -br -c a"
alias space="du -kx . | sort -rn | less"
alias size="sudo du -xB M --max-depth=2 . | sort -rn | head -n 15"
alias pss='ps xawf -eo pid,user,cgroup,args'
alias srv_run='systemctl list-units      --type=service --state=running --no-pager'
alias srv_act='systemctl list-units      --type=service --state=active  --no-pager'
alias srv_ena='systemctl list-unit-files --type=service --state=enabled --no-pager'
alias supd='sudo $SADMIN/bin/sadm_osupdate.sh'
alias supdate='sudo apt update && sudo apt upgrade'
alias smem="ps ax -o comm,%mem,user,pid --sort=-%mem | head -10"
alias snetshow="netstat -ntu|awk '{print $5}'|cut -d: -f1 -s|sort|uniq -c|sort -nk1 -r"
#
#

