#!/bin/sh
# ==============================================================================
# Kickstart Postinstall Script 
# IMPORTANT: This script expect to run in a chrooted environment.
# Jacques Duplessis - July 2008
# Version 1.1
#
# ==============================================================================
#
# change JAVA_HOME line iv slacenv.sh to 
# JAVA_HOME=`ls -1d /opt/ibm/java* | sort | tail -1`
# /etc/skel in slac_file = delete
# Put new IBM java n java software and create java.rpm  link
# remove /root/.vnc
#
# ==============================================================================
#set -x 


# GLOBAL VARIABLE DEFINITION
# ==============================================================================

# NFS MOUNT POINT
SRC_DIR=/mnt/nfs    									; export SRC_DIR

# NETWORK INFORMATION
GATEWAY=192.168.1.1                         			; export GATEWAY
NETMASK="255.255.255.0"                     			; export NETMASK
NAME_SERVERS="192.168.1.101 24.200.241.37"  			; export NAME_SERVERS
DOMAIN=maison.ca                            			; export DOMAIN
ETHDEV=`netstat -rn | tail -1 | awk '{ print $NF }'` 	; export ETHDEV

IPADDR=`ifconfig -a|grep 255.255|head -1|awk '{ print $2 }'|awk -F: '{ print $2 }'` 
HOSTNAME=`host $IPADDR | awk '{ print $NF }' | cut -d. -f1`
export HOSTNAME IPADDR
    
# WHERE STANDARD CONFIG FILES ARE LOCATED
STD_FILES=$SRC_DIR/batcave/files               			; export STD_FILES

# WHERE SOFTWARE TO INSTALL RESIDE
SOFTWARE=$SRC_DIR/software               			    ; export SOFTWARE

# WHERE INSTALL LOG DIRECTORY AND NAME WILL BE CREATED
LOG_DIR=/root                               			; export LOG_DIR
LOG_FILE=$LOG_DIR/postinstall.log           			; export LOG_FILE
touch $LOG_FILE

# Storix Server (Without Domain Name)
STORIX_SERVER=nomad.maison.ca                  			; export STORIX_SERVER

# REDHAT PROXY SERVER NAME
RHN_PROXY=nomad.maison.ca                   			; export RHN_PROXY





# ==============================================================================
#                F U N C T I O  N S     D E C L A R A T I O N 
# ==============================================================================

# Write infornation into the log
# ==============================================================================
write_log()
{
  echo -e "`date` - $1" >> $LOG_FILE
}



# Write Environnement Variables Used in the Script to log
# ==============================================================================
write_env()
{
	write_log "SRC_DIR       = $SRC_DIR" 
	write_log "STD_FILES     = $STD_FILES"  
	write_log "SOFTWARE      = $SOFTWARE" 
	write_log "LOG_DIR       = $LOG_DIR" 
	write_log "LOG_FILE      = $LOG_FILE" 
	write_log "STORIX_SERVER = $STORIX_SERVER" 
	write_log "DOMAIN        = $DOMAIN"
	write_log "HOSTNAME      = $HOSTNAME"
	write_log "IPADDR        = $IPADDR"
	write_log "NETMASK       = $NETMASK" 
	write_log "GATEWAY       = $GATEWAY" 
	write_log "NAME_SERVERS  = $NAME_SERVERS" 
	write_log "RHN_PROXY     = $RHN_PROXY" 
}


# Disable Linux Service 
# ==============================================================================
disable_service()
{
  write_log "Disabling service '$1' ..."
  chkconfig $1 off
}


# Enable Linux Service 
# ==============================================================================
enable_service()
{
  write_log "Enabling service '$1' ..."
  chkconfig $1 on
}


# Making Directory With the same Protection & Priv. found in slac_files dir 
# ==============================================================================
make_dir()
{
  #write_log "Creating and/or updating permissions on directory $2 ..."
  if [ ! -d "$2" ] 
     then write_log "Creating & updating permission on directory $2 ..."
          if [ ! -d "$1" ] 
             then OWNER="root.root"        ; PROT="755" 
             else OWNER=`stat "$1" -c%U.%G`; PROT=` stat "$1" -c%a`
          fi
          mkdir -p "$2" && chown $OWNER "$2" && chmod $PROT "$2"
          write_log "mkdir -p $2 && chown $OWNER $2 && chmod $PROT $2"
     else write_log "Directory $2 already exist" 
  fi 
}




# Create the /etc/resolv.conf file
# ==============================================================================
setup_dns()
{
    write_log "\n========== Setup DNS"
    echo "domain $DOMAIN"      >/etc/resolv.conf
    write_log "domain $DOMAIN" 
    echo "search $DOMAIN"     >>/etc/resolv.conf
    write_log "search $DOMAIN"  
    for ns in $NAME_SERVERS
        do
        echo "nameserver $ns" >>/etc/resolv.conf
        write_log  "nameserver $ns" 
        done

}



# Setup Host name
# ==============================================================================
setup_hostname()
{
    write_log "\n========== Setup HostName"
    if [ "$HOSTNAME" = "" ]
       then echo "Unable to find hostname from IP address. Assuming no_dns..."
            HOSTNAME="IP_NOT_IN_DNS"
    fi
    write_log "Current system ID:"
    write_log " - HOSTNAME = $HOSTNAME"
    write_log " - IPADDR   = $IPADDR"
}


# Setup network
# ==============================================================================
setup_network()
{
    write_log "\n========== Setup Network"

    # Update network file
    echo "NETWORKING=yes"              >/etc/sysconfig/network
    echo "HOSTNAME=$HOSTNAME.$DOMAIN" >>/etc/sysconfig/network
    write_log "New /etc/sysconfig/network content:"
    cat /etc/sysconfig/network | while read wline ; do write_log "$wline"; done

    # setup /etc/sysconfig/network-scripts/ifcfg-ETHDEV
    org_file="/etc/sysconfig/network-scripts/ifcfg-${ETHDEV}" 
    tmp_file="/tmp/ifcfg-${ETHDEV}" 
    cp $org_file $tmp_file
    echo "GATEWAY=$GATEWAY"                                   >>$tmp_file
    echo "NETMASK=$NETMASK"                                   >>$tmp_file
    echo "ETHTOOL_OPTS=\"speed 100 duplex full autoneg off\"" >>$tmp_file
    grep -vi "ONBOOT" $tmp_file > $org_file
    echo "ONBOOT=yes" >> $org_file
    write_log "New /etc/sysconfig/network-scripts/ifcfg-${ETHDEV} content:"
    cat $org_file | while read wline ; do write_log "$wline"; done
    chmod 0644 $org_file
}


# Setup the /etc/hosts file
# ==============================================================================
setup_host_file()
{
    write_log "\n========== Setup /etc/hosts"
    grep '^#' /etc/hosts >/tmp/hosts.new
    cat /tmp/hosts.new >/etc/hosts
    cat <<EOF >>/etc/hosts
127.0.0.1   localhost.localdomain localhost
$IPADDR     $HOSTNAME.$DOMAIN $HOSTNAME
EOF

    write_log "New /etc/hosts content:"
    cat /etc/hosts | while read wline ; do write_log "$wline"; done
}



# Setup Services
# ==============================================================================
setup_services()
{
    write_log "\n========== Setup Services"
    disable_service apmd
    disable_service autofs
    disable_service iptables
    disable_service mdmonitor
    disable_service mdmpd
    disable_service nfs
    disable_service smartd
    disable_service gpm
    disable_service bluetooth
    disable_service hidd
    disable_service ip6tables
    enable_service sshd
    enable_service ntpd
    enable_service rsh
    enable_service rsync
    enable_service vsftpd
    enable_service snmpd
}


# Create Standard Life Standard System Groups
# ==============================================================================
create_groups()
{
    write_log "\n========== Create Standard System Groups"
    write_log "groupadd -g 1000 glx-users-no"
    groupadd -g 1000 glx-users-no

    write_log "groupadd -g 1001 glx-admin-ao"
    groupadd -g 1001 glx-admin-ao

    write_log "groupadd -g 1002 gdb-dba-users-ao"
    groupadd -g 1002 gdb-dba-users-ao

    write_log "groupadd -g 601 slam"
    groupadd -g 601 slam

    write_log "groupadd -g 602 storix"
    groupadd -g 602 storix

    write_log "groupadd -g 603 mqm"
    groupadd -g 603 mqm
}



# Activate fail login count
# ==============================================================================
activate_failed_login_count()
{
    write_log "\n========== Activate failed login count"
    wfile="/var/log/faillog"
    touch $wfile
    chmod 664 $wfile
    write_log "Create file $wfile" 
}


# Change default group used when creating a new user
# ==============================================================================
change_default_system_group()
{
    write_log "\n========== Change default system group"
    write_log "Changing default group in /etc/useradd..."
    grep -vE "GROUP|^$" /etc/default/useradd >/tmp/useradd.postinstall
    echo "GROUP=1000" >>/tmp/useradd.postinstall
    cat /tmp/useradd.postinstall >/etc/default/useradd
    write_log "New /etc/default/useradd content:"
    cat /etc/default/useradd | while read wline ; do write_log "$wline"; done
}



# Create Standard users
# ==============================================================================
create_users()
{
    write_log "\n========== Create Standard users"

    write_log "useradd -c SLAM User -g slam -u 601 slam" 
    useradd -c "SLAM User" -g slam -u 601 slam

    write_log "useradd -c Storix User -g storix -u 602 storix"
    useradd -c "Storix User" -g storix -u 602 storix
}





# Install Storix Software
# ==============================================================================
install_storix()
{
    write_log "\n========== Install Storix Software"
    if [ $HOSTNAME = $STORIX_SERVER ]
       then write_log "IMPORTANT: This system is Storix Network Administrator."
            write_log "           Please install Storix manually."
       else write_log "Installing Storix Client..."
            mkdir /tmp/storix_install
            cd /tmp/storix_install
            tar -xvf $SOFTWARE/storix/storix_current_version.tar >> $LOG_FILE 2>&1
            ./stinstall -c -d /storix -p 9181 -s 9182  >> $LOG_FILE 2>&1
            echo "$STORIX_SERVER.$DOMAIN" >/storix/config/admin_servers
            cd /
            rm -rf /tmp/storix_install
            echo "/tmp/*" >/storix/config/exclude_list
            /opt/storix/bin/stconfigure -t c -a $STORIX_SERVER.$DOMAIN  >> $LOG_FILE 2>&1
            write_log "Storix Client installed ..."
    fi
}


# Install TSM Client Software
# ==============================================================================
install_tsm_client()
{
    write_log "\n========== Install TSM Client Software"
    mkdir /tmp/tsm_install
    cd /tmp/tsm_install
    tar -xvf $SOFTWARE/tsm/tsm_client/tsm_client_current_version.tar
    rpm -Uvh TIVsm-API.i386.rpm  >> $LOG_FILE
    rpm -Uvh TIVsm-BA.i386.rpm  >> $LOG_FILE
    cd /
    rm -rf /tmp/tsm_install
    #touch /dsmerror.log
    write_log "TSM Client installed ..."
}




# Install TSM Oracle TDP Client Software
# ==============================================================================
install_tsm_tdp()
{
    write_log "\n========== Install TSM TDP Software"
    mkdir /tmp/tsm_tdp
    cd /tmp/tsm_tdp
    tar -xvf $SOFTWARE/tsm/tdp_oracle/tdp_oracle_current.tar
    rpm -Uvh TDP-Oracle.i386.rpm  >> $LOG_FILE
    RC=$? ; write_log "Return Code is $RC" 
    cd /
    rm -rf /tmp/tsm_tdp
    chmod 2775 /var/adsmlog
    chown root.gdb-dba-users-ao /var/adsmlog
    write_log "TSM TDP installed ..."
}





# Install Qpage Sftware
# ==============================================================================
install_qpage()
{
    write_log "\n========== Install Qpage Software"
    write_log "rpm -Uvh $SOFTWARE/qpage/qpage_current_version.rpm" 
    rpm -Uvh $SOFTWARE/qpage/qpage_current_version.rpm >> $LOG_FILE
    RC=$? ; write_log "Return Code is $RC" 
    disable_service qpage
    write_log "Deleting qpage user and group ..."
    userdel qpage
    groupdel qpage
}



# Install Java Software
# ==============================================================================
install_java()
{
    write_log "\n========== Install Java Software"
    write_log "rpm -Uvh $SOFTWARE/java/java.rpm" 
    rpm -Uvh $SOFTWARE/java/java.rpm >> $LOG_FILE
    RC=$? ; write_log "Return Code is $RC" 

    # Make sure we used to IBM Version
    if [ -f /usr/bin/java ] ; then mv /usr/bin/java /usr/bin/java.org ; fi
    JAVA_BIN=`find /opt -type f -name java | sort -r | head -1` 
    ln -s ${JAVA_BIN} /usr/bin/java
    write_log "JAVA_BIN = $JAVA_BIN" 
}



# Install HP Agent Software
# ==============================================================================
install_hpagent()
{
    write_log "\n========== Install HP Agent Software"
    rpm -ivh $SOFTWARE/hp/hpasm/hpasm-7.7.0-115.rhel4.i386.rpm  >> $LOG_FILE
    RC=$? ; write_log "Return Code is $RC" 
    rm -f /sbin/hpasm
    enable_service hpasm
}



# Install Enterprise Security Manager
# ==============================================================================
install_esm()
{
    write_log "\n========== Enterprise Security Manager "
    ESMDIR="$SOFTWARE/esm/esm_current_version"
    $ESMDIR/esmsetup -a -p 1,2,3,4,5,6,7,8,9,10,11,12,13,14 -d /axent/symantec/esm -u root -g root -t $ESMDIR/esm.tgz -M sxmq1020 -O 5600 -U esm -W rlse60 -N $HOSTNAME.$DOMAIN -b 
    mv /esm/system/localhost.localdomain /esm/system/$HOSTNAME.$DOMAIN
}



# Install Additionnal Software
# ==============================================================================
install_std_software()
{
    write_log "\n========== Install Additionnal Software for Version 5 "
    for package in `ls $SOFTWARE/std_rpm/5.0`
        do
        write_log "================================================="
        write_log "rpm -Uvh $SOFTWARE/std_rpm/5.0/$package"
        rpm -Uvh $SOFTWARE/std_rpm/5.0/$package  >> $LOG_FILE
        RC=$? ; write_log "Return Code is $RC\n" 
        done
}



# Install Additionnal Software
# ==============================================================================
remove_unwanted_files()
{
    write_log "\n========== Remove unwanted files"
    write_log "rm -f /etc/profile.d/krb5.\*" 
    rm -f /etc/profile.d/krb5.*

    write_log "/usr/share/man/man1/cancel.1.gz" 
    rm -f /usr/share/man/man1/cancel.1.gz

    write_log "/etc/cron.daily/00-logwatch" 
    rm -f /etc/cron.daily/00-logwatch
}



# Make rsh and rsync work
# ==============================================================================
setup_rsh_rsync()
{
    write_log "\n========== Make rsh / rsync to work"
    write_log "echo rsh   >>/etc/securetty" 
    echo "rsh"   >>/etc/securetty
    write_log "echo rsync >>/etc/securetty" 
    echo "rsync" >>/etc/securetty
}



# Create /mnt mount point
# ==============================================================================
create_mnt_mount_point()
{
    write_log "\n========== Create /mnt mount Point"
    write_log "mkdir -p /mnt/iso    ; chmod 755 /mnt/iso    ; chown root.root /mnt/iso"
    mkdir -p /mnt/iso       ; chmod 755 /mnt/iso    ; chown root.root /mnt/iso

    write_log "mkdir /mnt/usb       ; chmod 755 /mnt/usb    ; chown root.root /mnt/usb" 
    mkdir /mnt/usb       ; chmod 755 /mnt/usb    ; chown root.root /mnt/usb

    write_log "mkdir /mnt/nfs1      ; chmod 755 /mnt/nfs1   ; chown root.root /mnt/nfs1"
    mkdir /mnt/nfs1      ; chmod 755 /mnt/nfs1   ; chown root.root /mnt/nfs1

    write_log "mkdir /mnt/nfs2      ; chmod 755 /mnt/nfs2   ; chown root.root /mnt/nfs2"
    mkdir /mnt/nfs2      ; chmod 755 /mnt/nfs2   ; chown root.root /mnt/nfs2
    
    write_log "mkdir /mnt/usbkey    ; chmod 755 /mnt/usbkey ; chown root.root /mnt/usbkey"
    mkdir /mnt/usbkey    ; chmod 755 /mnt/usbkey ; chown root.root /mnt/usbkey

    write_log "mkdir /mnt/cdrom     ; chmod 755 /mnt/cdrom  ; chown root.root /mnt/cdrom"
    mkdir /mnt/cdrom     ; chmod 755 /mnt/cdrom  ; chown root.root /mnt/cdrom
}



# Remove uneeded user and group accounts
# ==============================================================================
remove_unwanted_users_and_groups()
{
    write_log "\n========== Remove unwanted users and groups"
    write_log "userdel news"
    userdel news
    write_log "userdel games"
    userdel games
    write_log "userdel gopher"
    userdel gopher
    write_log "userdel sync"
    userdel sync
    write_log "userdel shutdown"
    userdel shutdown
    write_log "userdel halt"
    userdel halt
    write_log "userdel operator"
    userdel operator
    write_log "groupdel news"
    groupdel news
}



# Create any needed directory - Create any dir found in slac_files on system
# ==============================================================================
create_cie_std_directory()
{
    write_log "\n========== Create Cie Standard Directories"
    find $STD_FILES -type d -print | while read srcdir
         do
         if [[ $srcdir == $STD_FILES ]] ; then continue ; fi
         #write_log "make_dir ${srcdir#$STD_FILES}" 
         make_dir "$srcdir" "${srcdir#$STD_FILES}"
         done
}



# Copy the customized files to their appropriate location
# ==============================================================================
copy_cie_std_files()
{
    write_log "\n========== Copy Cie Standard Files on the system"
    find $STD_FILES \( -type f -o -type l \) -print | while read srcfile
        do
        FILE_EXIST="NO" 
        if [ -f "${srcfile#$STD_FILES}" ]
           then OWNER=`stat "${srcfile#$STD_FILES}" -c%U.%G`
                PROT=` stat "${srcfile#$STD_FILES}" -c%a`
                FILE_EXIST="YES" 
           else OWNER="root.root" 
                PROT="755"
                FILE_EXIST="NO" 
        fi                
        cp -Ppf "$srcfile" "${srcfile#$STD_FILES}"
        write_log "cp -Ppf $srcfile ${srcfile#$STD_FILES} RC=$?"
        if [ "$FILE_EXIST" == "YES" ] 
           then chown $OWNER ${srcfile#$STD_FILES} ; chmod $PROT  ${srcfile#$STD_FILES}
                write_log "chown $OWNER ${srcfile#$STD_FILES} ;chmod $PROT ${srcfile#$STD_FILES}" 
        fi
        done
}



# If needed ssh2 keys for aix will exist
# ==============================================================================
create_ssh2_keys_for_aix()
{
    write_log "\n========== Creating ssh2 keys for AIX communication ..."
    write_log "Starting OpenSSH server - Create linux keys ..."
    /etc/init.d/sshd start
    write_log "Stopping OpenSSH server..."
    /etc/init.d/sshd stop
    write_log "Generating SSH2 compatible public key from OpenSSH public key."
    write_log "Key will be saved to /etc/ssh/$HOSTNAME.$DOMAIN.ssh-dss.pub"
    write_log "ssh-keygen -e -f /etc/ssh/ssh_host_dsa_key.pub >/etc/ssh/$HOSTNAME.$DOMAIN.ssh-dss.pub"
    ssh-keygen -e -f /etc/ssh/ssh_host_dsa_key.pub >/etc/ssh/$HOSTNAME.$DOMAIN.ssh-dss.pub
}



# Make sure rc.startup get executed
# Now that slacapp has been copied, enable service
# ==============================================================================
enable_slacapp_service()
{
    write_log "\n========== Making sure rc.startup executed at startup ..."
    write_log "Enabling slacapp service" 
    write_log "Before Inserting slacapp service"
    chkconfig --list | grep slacapp >> $LOG_FILE
    chkconfig --del slacapp 
    chkconfig --add slacapp
    write_log "After Inserting"
    chkconfig --list | grep slacapp >> $LOG_FILE
    find /etc/rc.d -name "*slacapp" >> $LOG_FILE
}




# Fix some permissions
# ==============================================================================
secure_some_files_permissions()
{
    write_log "\n========== Secure some files permissions ..."
    chmod 750 /root
    chmod 600 /root/.forward
    chmod 600 /root/.rhosts
    chmod 600 /root/.shosts
    chmod 700 /root/.ssh
    chmod 600 /root/.ssh/authorized_keys
    chmod 644 /root/.ssh/known_hosts
    chmod 600 /etc/sudoers
    chmod 644 /etc/vsftpd/ftpusers
    chmod 600 /etc/vsftpd/user_list
    chmod 600 /etc/ldap.conf
    chmod 600 /etc/ssh/sshd_config
    chmod 750 /sysadmin/bin/*
    chmod 750 /sysadmin/sam/*
}



# Add sysadmin users on the system
# ==============================================================================
create_sysadmin_users()
{
    write_log "\n========== Create sysadmin users " 
 
    if [ "$DOMAIN" = "maison.ca" ]
       then write_log "useradd -c Jacques Duplessis    -g glx-admin-ao jacques"
            useradd -c "Jacques Duplessis"    -g glx-admin-ao jacques
cat <<EOF | chpasswd -e
jacques:\$1\$77rM/QYQ\$p.RKyklJffsYy8JdTYBlH0
EOF
            return 0
    fi

    write_log "useradd -c Jacques Duplessis    -g glx-admin-ao jadupl2"
    useradd -c "Jacques Duplessis"    -g glx-admin-ao jadupl2

    write_log "useradd -c Patrick Gervais      -g glx-admin-ao pagerv1"
    useradd -c "Patrick Gervais"      -g glx-admin-ao pagerv1

    write_log "useradd -c Huguette Hammal      -g glx-admin-ao huhamm1"
    useradd -c "Huguette Hammal"      -g glx-admin-ao huhamm1

    write_log "useradd -c Ammar Bouhroud       -g glx-admin-ao ambouh1"
    useradd -c "Ammar Bouhroud"       -g glx-admin-ao ambouh1

    write_log "useradd -c George Sonnenwirth   -g glx-admin-ao gesonn1"
    useradd -c "George Sonnenwirth"   -g glx-admin-ao gesonn1

    write_log "useradd -c Richard Mochnazewski -g glx-admin-ao rimoch1"
    useradd -c "Richard Mochnazewski" -g glx-admin-ao rimoch1

    write_log "useradd -c Was Monitor Account  -g glx-users-no wasmon" 
    useradd -c "Was Monitor Account"  -g glx-users-no wasmon


    # Set default passwords for system administrators
	# IMPORTANT: Do not forget to escape $ characters in the encrypted passwords!
	# If this is not done, the shell will interpret these as variables.
cat <<EOF | chpasswd -e
jadupl2:\$1\$77rM/QYQ\$p.RKyklJffsYy8JdTYBlH0
pagerv1:\$1\$dsI7Gz2w\$zBuWbnngJ3CDe7MpZW7YG0
huhamm1:\$1\$QfccUCz/\$AREoh1ng/7B/0dcQvbNex0
ambouh1:\$1\$ZJu/CrKk\$AFAvLQWbYBCqQjWTSgbRJ0
gesonn1:\$1\$S/DCBbQS\$CKVFDT7zlGTTbJP1MmP8L.
rimoch1:\$1\$3xMpNjHc\$cvV/znJsoZRMceLIqItlW0
EOF
}



# Install SLAM
# ==============================================================================
install_slam()
{
    write_log "\n========== Install slam software " 
    cp $SOFTWARE/slam/slam.tar /sysadmin/slam
    cd /sysadmin/slam
    tar -xvf slam.tar 
    echo "" | ./slam_install.sh >> $LOG_FILE
    rm slam_install.*
    rm slam.tar
    cd /
    touch /sysadmin/slam/cfg/use_scp.txt  # make sure scp is used not rcp
    chmod -R 775 /sysadmin/slam
}



# Customize TSM Files - Must do that AFTER copy cie std files
# ==============================================================================
setup_hostname_in_tsm_files()
{
    write_log "\n========== setup_hostname_in_tsm_files"
    TSM_BIN="/opt/tivoli/tsm/client/ba/bin" ; export TSM_BIN
    write_log "Changing Hostname in $TSM_BIN/dsm.sys"
    sed -e "s/MYNODENAME/$HOSTNAME/g" <$TSM_BIN/dsm.sys.generic >$TSM_BIN/dsm.sys

    TSM_TDP="/opt/tivoli/tsm/client/oracle/bin" ; export TSM_TDP  
    write_log "Changing Hostname in $TSM_TDP/tdpo.opt" 
    sed -e "s/MYNODENAME/$HOSTNAME/g" <$TSM_TDP/tdpo.opt.generic >$TSM_TDP/tdpo.opt
}



# Customize Inittab file
# ==============================================================================
setup_inittab()
{
    write_log "\n========== Customizing /etc/inittab file"

# Remove CTRL-ALT-DEL
    write_log "Remove CTRL-ALT_DEL = Reboot"
    cp /etc/inittab /etc/inittab.original
    cat /etc/inittab.original | sed -e 's/ca::ctrlaltdel/# ca::ctrlaltdel/g' >/etc/inittab

# Insert TSM Scheduler 
    write_log "Inserting TSM Scheduler ..."
cat <<EOF >>/etc/inittab
# TSM Scheduler
#tsm:35:respawn:/opt/tivoli/tsm/client/ba/bin/dsmc sched >/var/adsmlog/dsmc_sched.log 2>&1
EOF
}




# Create crontab file for root user
# ==============================================================================
create_crontab()
{
    write_log "\n========== Create crontab file for root"
cat <<EOF >/tmp/postinstall.crontab
#
# Cron does not get a Path when it is run - So We put a standard one here
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:
#
#
# Once a day at 6am - Generate System Configuration for this machine
00 06 * * * /sysadmin/sysinfo/sysinfo3 >/sysadmin/sysinfo/cron_sysinfo3.log 2>&1
#
# Run Slam Client every 5 minutes
*/5 * * * * /sysadmin/slam/bin/slam_slave.sh >/sysadmin/slam/log/slam_slave.log 2>&1
#
# Once a day - Delete files in /tmp that have not been modified for 35 days
30 09 * * * find /tmp -type f -mtime +35 -exec rm -f {} \; >/dev/null 2>&1
#
# Once a day at 5am - Save VG Information for DR
00 05 * * * /sysadmin/sam/drsavevg.sh >/var/adsmlog/drsavevg.log 2>&1
#
# Once a day just before midnight - Create daily Linux performance data file
55 23 * * * /sysadmin/bin/create_linux_perfdata.sh
#
#
EOF
    crontab /tmp/postinstall.crontab
    crontab -l >> $LOG_FILE
}


# Register at RedHat by going through RH Proxy Server (lxmq0007)
# ==============================================================================
register_server()
{
    write_log "\n========== Registering Server at Red Hat "

    # RHEL4 
    #wget -O- http://$RHN_PROXY/pub/bootstrap/config_rhn_client.sh | bash
    #rpm --import /usr/share/rhn/RPM-GPG-KEY # V4

    # RHEL 5
    #wget -O- http://$RHN_PROXY/pub/bootstrap_script | bash
    #/usr/sbin/rhnreg_ks --activationkey=$RHKEY  # V5
}



# ==============================================================================
#             S T A R T   O F   T H E   M A I N   S C R I P T 
# ==============================================================================
write_log "Start of postinstall script "
write_env				    # Write ENv. Variable to log
setup_dns                                   # Set up /etc/resolv.conf
setup_network                               # Setup ifcfg-eth0 & network file
setup_hostname                              # Set HOSTNAME and IPADDR Var.
setup_host_file                             # Setup /etc/hosts file
setup_services                              # Disable/Enable Services
change_default_system_group                 # Make Group 1000 the default group
create_groups                               # Create std slac needed group
create_users                                # Create slac standard users
create_sysadmin_users                       # Create Sysadmin Users
activate_failed_login_count                 # Create /var/log/faillog 
install_storix                              # Install Storix Software
#install_qpage                               # Install & setup package
install_java                                # Install Java Software
#
if [ "$DOMAIN" = "batcave.ca" ]  
   then install_tsm_client                  # Install Latest TSM Client
        install_tsm_tdp                     # Install Latest TSM TDP 
        install_hpagent                     # Install HP Agent 
        install_esm                         # Install Enterprise Security Manager
fi
#
install_std_software                        # Install Standard Add. Software
remove_unwanted_files                       # remove unwanted files
setup_rsh_rsync                             # Enable rsync and rsh
create_mnt_mount_point                      # Create /mnt mount point
remove_unwanted_users_and_groups            # Remove unneeded users and groups
create_cie_std_directory                    # Create dir found in slac_files 
copy_cie_std_files                          # Copy slac_files file onto system
create_ssh2_keys_for_aix                    # If needed ssh2 keys for aix will exist
enable_slacapp_service                      # Make sure rc.startup get executed
secure_some_files_permissions               # chmod .rhosts .ssh sudoers ...
install_slam                                # install Slam Software
setup_hostname_in_tsm_files                 # Must do that AFTER copy cie std files
setup_inittab                               # Disable CTRL-ALT-DEL & TSM 
create_crontab                              # Create crontab file for root user
#
if [ "$DOMAIN" = "maison.ca" ]  
   then register_server    		    # Register host at RedHat through Proxy
fi 
#
write_log "End of Postscript file"
exit 0
