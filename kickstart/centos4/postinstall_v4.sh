#!/bin/sh
#
# Kickstart Postinstall Script
# Jacques Duplessis
# Revised 16 Septembre 2006


#####################################################################
# GLOBAL VARIABLES
#####################################################################

SRC_DIR=/mnt/nfs
MY_FILES=$SRC_DIR/my_files
SOFTWARE=$SRC_DIR/software
LOG_DIR=/root
LOG_FILE=$LOG_DIR/postinstall.log
GATEWAY=192.168.1.99
NAME_SERVERS="192.168.1.122 24.200.243.250 24.201.245.106"
STORIX_ADMIN_SERVER=venus
DOMAIN=maison.ca

# Redirect stdout and stderr to log file
exec 1>$LOG_FILE
exec 2>&1


#####################################################################
# FUNCTIONS
#####################################################################

disable_service()
{
  echo "Disabling service '$1' ..."
  chkconfig $1 off
}

enable_service()
{
  echo "Enabling service '$1' ..."
  chkconfig $1 on
}

make_dir()
{
  echo "Creating and/or updating permissions on directory $1 "
  mkdir -p "$2" && chown `stat "$1" -c%U.%G` "$2" && chmod `stat "$1" -c%a` "$2"
  echo "mkdir -p $2 && chown `stat "$1" -c%U.%G` $2 && chmod `stat $1 -c%a` $2"
}

copy_file()
{
  echo "Copying file $1 to $2 ..."
  cp -Ppf "$1" "$2"
}


#####################################################################
# MAIN
#####################################################################

echo "Start of postinstall script"
echo ""

#
# First of all, setup resolver else we won't be able to determine who we are.
#
echo "===================================================================="
echo "Setting up DNS resolver..."
echo "domain $DOMAIN" >/etc/resolv.conf
for ns in $NAME_SERVERS
do
  echo "nameserver $ns" >>/etc/resolv.conf
done


#
# Get current system ID
#
IPADDR=`ifconfig eth0 | grep "inet addr" | awk '{ print $2 }' | cut -d: -f2`
HOSTNAME=`host $IPADDR | awk '{ print $NF }' | cut -d. -f1`
if [ "$HOSTNAME" = "" ]
then
  echo "NOTE: Unable to find hostname from IP address. Assuming rh3std..."
  HOSTNAME=ramses4
fi
echo "Current system ID:"
echo "HOSTNAME=$HOSTNAME.$DOMAIN"
echo "IPADDR=$IPADDR"


#
# Fix /etc/sysconfig/network
#
echo -e "\n===================================================================="
echo "NETWORKING=yes" >/etc/sysconfig/network
echo "HOSTNAME=$HOSTNAME.$DOMAIN" >>/etc/sysconfig/network
echo "New /etc/sysconfig/network content:"
cat /etc/sysconfig/network


#
# Fix /etc/sysconfig/network-scripts/ifcfg-eth0
#
echo -e "\n===================================================================="
echo "GATEWAY=$GATEWAY" >>/etc/sysconfig/network-scripts/ifcfg-eth0
echo "ETHTOOL_OPTS=\"speed 100 duplex full autoneg off\"" >> /etc/sysconfig/network-scripts/ifcfg-eth0
echo "New /etc/sysconfig/network-scripts/ifcfg-eth0 content:"
echo "===================================================================="
cat /etc/sysconfig/network-scripts/ifcfg-eth0


#
# Fix /etc/hosts
#
echo -e "\n===================================================================="
echo "Fixing /etc/hosts..."
grep '^#' /etc/hosts >/tmp/hosts.new
cat /tmp/hosts.new >/etc/hosts
cat <<EOF >>/etc/hosts
127.0.0.1   localhost.localdomain localhost
$IPADDR     $HOSTNAME.$DOMAIN $HOSTNAME
EOF
echo "New /etc/hosts content:"
cat /etc/hosts


#
# Disable the following services
#
echo -e "\n===================================================================="
echo "Disabling Services that we do not want" 
disable_service apmd
disable_service arptables_jf
disable_service autofs
disable_service iptables
disable_service isdn
disable_service mdmonitor
disable_service mdmpd
disable_service pcmcia
disable_service nfs
disable_service vncserver
disable_service smartd
disable_service gpm


#
# Enable the following services
#
echo -e "\n===================================================================="
echo "Enabling Services that we want" 
enable_service sshd
enable_service ntpd
enable_service rsh
enable_service rsync
enable_service vsftpd
enable_service snmpd


#
# Disable logwatch cron job
#
echo -e "\n===================================================================="
echo "Disabling logwatch..."
echo "rm -f /etc/cron.daily/00-logwatch" 
rm -f /etc/cron.daily/00-logwatch




#
# Add slam, storix, dba and basic admin and user groups
#
echo -e "\n===================================================================="
echo "Adding some groups..."
groupadd -g 1000 glx-users-no
groupadd -g 1001 glx-admin-ao
groupadd -g 1002 gdb-dba-users-ao
groupadd -g 601 slam
groupadd -g 602 storix
groupadd -g 603 mqm
groupadd -g 604 maison
groupadd -g 605 staff


#
# Change default group used when creating a new user
#
echo -e "\n===================================================================="
echo "Changing default group in /etc/useradd..."
grep -v GROUP /etc/default/useradd >/tmp/useradd.postinstall
echo "GROUP=1000" >>/tmp/useradd.postinstall
cat /tmp/useradd.postinstall >/etc/default/useradd
echo "New /etc/default/useradd content:"
cat /etc/default/useradd


#
# Add slam, storix, oracle, mqm  and sys. admin users
#
echo -e "\n===================================================================="
echo "Adding oracle, slam and storix user accounts..."
useradd -c "Oracle User" -g gdb-dba-users-ao -u 1002 oracle
useradd -c "SLAM User" -g slam -u 601 slam
useradd -c "Storix User" -g storix -u 602 storix
useradd -c "MQSeries Users" -g mqm -u 603 mqm
useradd -c "Jacques Duplessis" -g staff -u 605 jacques


#
# Install Storix Client
#
echo -e "\n===================================================================="
if [ $HOSTNAME = $STORIX_ADMIN_SERVER ]
then
  echo "IMPORTANT: This system is the Storix Network Administrator."
  echo "           Please install Storix manually."
else
  echo "Installing Storix Client..."
  mkdir /tmp/storix_install
  cd /tmp/storix_install
  tar -xvf $SOFTWARE/storix/storix_current_version.tar
  ./stinstall -c -d /storix -p 9181 -s 9182
  echo "$STORIX_ADMIN_SERVER" >/storix/config/admin_servers
  cd /
  rm -rf /tmp/storix_install
  echo "/tmp/*" >/storix/config/exclude_list
  /opt/storix/bin/stconfigure -t c -a $STORIX_ADMIN_SERVER.$DOMAIN
fi



#
# Install TSM Client
#
echo -e "\n===================================================================="
echo "Installing TSM Client..."
mkdir /tmp/tsm_install
cd /tmp/tsm_install
tar -xvf $SOFTWARE/tsm/tsm_client/tsm_client_current_version.tar
rpm -Uvh TIVsm-API.i386.rpm
rpm -Uvh TIVsm-BA.i386.rpm
cd /
rm -rf /tmp/tsm_install
touch /dsmerror.log

#
# Install TSM Oracle TDP client
#
echo -e "\n===================================================================="
echo "Installing TSM Oracle Client..."
mkdir /tmp/tsm_tdp
cd /tmp/tsm_tdp
tar -xvf $SOFTWARE/tsm/tdp_oracle/tdp_oracle_current.tar
rpm -Uvh TDP-Oracle.i386.rpm
cd /
rm -rf /tmp/tsm_tdp
touch /dsmerror.log
chmod 2775 /var/adsmlog
chown root.gdb-dba-users-ao /var/adsmlog

#
# Install qpage
#
echo -e "\n===================================================================="
echo "Installing qpage..."
rpm -Uvh $SOFTWARE/qpage/qpage_current_version.rpm
echo "Disabling qpage Service..."
disable_service qpage
echo "Deleting qpage user..."
userdel qpage
groupdel qpage

#
# Install JRE
#
echo -e "\n===================================================================="
echo "Installing Java Runtime..."
rpm -Uvh $SOFTWARE/java/jre_current_version.rpm
mv /usr/bin/java /usr/bin/java.org
ln -s /usr/java/j2re1.4.2_07/bin/java /usr/bin/java

#
# Install some "standard" package
#
echo -e "\n===================================================================="
echo "Installing Standard Packages..."
for package in `ls $SOFTWARE/std_rpm/4.0`
do
    echo -e "\nrpm -Uvh $SOFTWARE/std_rpm/4.0/$package"
    rpm -Uvh $SOFTWARE/std_rpm/4.0/$package
done


#
# Remove some unwanted files
#
echo -e "\n===================================================================="
echo "Deleting some unwanted files..."
echo "rm -f /etc/profile.d/krb5.\*" 
rm -f /etc/profile.d/krb5.*
rm -f /usr/share/man/man1/cancel.1.gz


#
# Required so that rsh and rsync work with root
#
echo -e "\n===================================================================="
echo "Adding rsh and rsync to /etc/securetty..."
echo "rsh"   >>/etc/securetty
echo "rsync" >>/etc/securetty

#
# Create SLAC Standard Directory in /mnt
#
echo -e "\n===================================================================="
echo "Creating Standard mount point in /mnt"
mkdir /mnt/iso       ; chmod 755 /mnt/iso    ; chown root.root /mnt/iso
mkdir /mnt/usb       ; chmod 755 /mnt/usb    ; chown root.root /mnt/usb
mkdir /mnt/nfs       ; chmod 755 /mnt/nfs    ; chown root.root /mnt/nfs
mkdir /mnt/nfs1      ; chmod 755 /mnt/nfs1   ; chown root.root /mnt/nfs1
mkdir /mnt/nfs2      ; chmod 755 /mnt/nfs2   ; chown root.root /mnt/nfs2
mkdir /mnt/usbkey    ; chmod 755 /mnt/usbkey ; chown root.root /mnt/usbkey
mkdir /mnt/cdrom     ; chmod 755 /mnt/cdrom  ; chown root.root /mnt/cdrom
ls -l /mnt



#
# Remove uneeded user and group accounts
echo -e "\n===================================================================="
echo "Removing uneeded user and group accounts..."
userdel news
userdel games
userdel gopher
userdel sync
userdel shutdown
userdel halt
userdel netdump
userdel operator
userdel hpsmh
groupdel news
groupdel games


#
# Removed block-major-43, 134 and 135 error on boot
#
#echo "===================================================================="
#echo "alias block-major-43 off" 	>>/etc/modules.conf
#echo "alias block-major-132 off" 	>>/etc/modules.conf
#echo "alias block-major-133 off" 	>>/etc/modules.conf
#echo "alias block-major-134 off" 	>>/etc/modules.conf
#echo "alias block-major-135 off" 	>>/etc/modules.conf
#echo "alias char-major-10-224 off" 	>>/etc/modules.conf


#
# Create any needed directory
#
echo -e "\n===================================================================="
echo "Create any needed Directory" 
find $MY_FILES -type d -print | while read srcdir
do
  if [[ $srcdir == $MY_FILES ]]
  then
    continue
  fi
  make_dir "$srcdir" "${srcdir#$MY_FILES}"
done


#
# Copy the customized files to their appropriate location
#
echo -e "\n===================================================================="
echo "Copy file from $srcfile to $MY_FILES Directory" 
find $MY_FILES \( -type f -o -type l \) -print | while read srcfile
do
  copy_file "$srcfile" "${srcfile#$MY_FILES}"
done


#
# Generate SSH2 compatible public key from OpenSSH public key 
#
echo -e "\n===================================================================="
echo "Starting OpenSSH server to generate host keys..."
/etc/init.d/sshd start
echo "Stopping OpenSSH server..."
/etc/init.d/sshd stop
echo "Generating SSH2 compatible public key from OpenSSH public key..."
echo "Key will be saved to /etc/ssh/$HOSTNAME.$DOMAIN.ssh-dss.pub"
ssh-keygen -e -f /etc/ssh/ssh_host_dsa_key.pub >/etc/ssh/$HOSTNAME.$DOMAIN.ssh-dss.pub


#
#
# Now that slacapp has been copied, enable service
#
echo -e "\n===================================================================="
echo "Enabling slacapp service" 
echo -e "\nBefore adding slacapp service"
chkconfig --list | grep slacapp
chkconfig --del slacapp
chkconfig --add slacapp
echo -e "\nAfter add slacapp service"
chkconfig --list | grep slacapp
find /etc/rc.d -name "*slacapp"



#
# Fix some permissions
#
echo -e "\n===================================================================="
echo "Fixing permissions on certain files..."
chmod 750 /root
chmod 600 /etc/sudoers
chown -R root.root /root
chmod 600 /root/.forward
chmod 700 /root/.ssh
chmod 600 /root/.ssh/authorized_keys
chmod 600 /root/.forward
chmod 600 /etc/vsftpd.ftpusers
chmod 600 /etc/vsftpd.user_list
chmod 600 /root/.vnc/passwd
chmod 600 /etc/ldap.conf
chmod 600 /etc/ssh/sshd_config


#
# Add sys. admin users
#
#echo "===================================================================="
#echo "Adding sys. admin users..."


#
# Set default passwords for system administrators
#
echo -e "\n===================================================================="
echo "Setting sys. admin user passwords..."
# IMPORTANT: Do not forget to escape $ characters in the encrypted passwords!
#            If this is not done, the shell will interpret these as variables.
cat <<EOF | chpasswd -e
jacques:\$1\$77rM/QYQ\$p.RKyklJffsYy8JdTYBlH0
EOF


#
# Fix NODENAME in dsm.sys and tdpo.opt
#
echo -e "\n===================================================================="
echo "Setting node node name in dsm.sys and tdpo.opt..."
sed -e "s/MYNODENAME/$HOSTNAME/g" </opt/tivoli/tsm/client/ba/bin/dsm.sys.generic >/opt/tivoli/tsm/client/ba/bin/dsm.sys
sed -e "s/MYNODENAME/$HOSTNAME/g" </opt/tivoli/tsm/client/oracle/bin/tdpo.opt.generic >/opt/tivoli/tsm/client/oracle/bin/tdpo.opt


#
# Install/Update SLAM + Install Perl Modules
#
echo -e "\n===================================================================="
echo "Installing/Updating SLAM Client..."
cp $SOFTWARE/slam/slam.tar /sysadmin/slam
cd /sysadmin/slam
tar -xvf slam.tar
echo "" | ./slam_install.sh
rm slam_install.*
rm slam.tar
cd /
echo ""
echo ""

#
# Remove CTRL-ALT-DEL reboot feature from inittab
#
echo -e "\n===================================================================="
echo "Adding TSM and removing ctrlaltdel in /etc/inittab"
cp /etc/inittab /etc/inittab.original
cat /etc/inittab.original | sed -e 's/ca::ctrlaltdel/# ca::ctrlaltdel/g' >/etc/inittab
cat <<EOF >>/etc/inittab
# TSM Scheduler
#tsm:35:respawn:/opt/tivoli/tsm/client/ba/bin/dsmc sched >/var/adsmlog/dsmc_sched.log 2>&1
EOF

#
# Create root crontab
#
echo -e "\n===================================================================="
echo "Creating root crontab..."
cat <<EOF >/tmp/postinstall.crontab
# Cron does not get a Path when it is run - So We put a standard one here
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:
SNPP_SERVER=qpserver.maison.ca
#
#
# Generate System Configuration for this machine
00 06 * * * /sysadmin/sysinfo/sysinfo3 >/sysadmin/sysinfo/cron_sysinfo3.log 2>&1
#
# Slam CLIENT
*/5 * * * * /sysadmin/slam/bin/slam_slave.sh >/sysadmin/slam/log/slam_slave.log 2>&1
#
# Delete files in /tmp that have not been modified for 35 days
30 09 * * * find /tmp -type f -mtime +35 -exec rm -f {} \; >/dev/null 2>&1
#
# Save VG Information for DR
00 05 * * * /sysadmin/sam/drsavevg.sh >/var/adsmlog/drsavevg.log 2>&1
#
# Create daily Linux performance data file for perf. graphs
55 23 * * * /sysadmin/bin/create_linux_perfdata.sh
#
EOF
crontab /tmp/postinstall.crontab



#
echo -e "\n===================================================================="
echo "End of postinstall script"
exit 0
