#!/usr/bin/env bash

# ------------------------------------------------------------------
# Script Name:   DebPostInstall
# Description:   A Bash Script to automate essential
#                post-installation tasks on Debian and Ubuntu
# Website:       https://gist.github.com/ostechnix
# Version:       1.0
# Usage:         sudo ./debpostinstall.sh
# ------------------------------------------------------------------

# Update the system
echo "Updating the system..."
apt-get update && apt-get full-upgrade -y
apt-get autoremove -y && apt-get autoclean -y

# Install necessary packages
echo "Installing necessary packages..."
apt-get install -y sudo openssh-server ufw systemd-timesyncd vim htop net-tools curl wget git

# Prompt for username
read -p "Enter the username for the new user: " USERNAME

# Check if the user already exists
if id "$USERNAME" &>/dev/null; then
    echo "User $USERNAME already exists. Skipping user creation."
else
    # Prompt for password
    read -s -p "Enter the password for the new user: " PASSWORD
    echo
    read -s -p "Confirm the password for the new user: " PASSWORD_CONFIRM
    echo

    # Check if passwords match
    if [ "$PASSWORD" != "$PASSWORD_CONFIRM" ]; then
        echo "Passwords do not match. Exiting."
        exit 1
    fi

    # Add a new user account with sudo access and set the password
    echo "Adding new user account..."
    useradd -m -s /bin/bash -G sudo $USERNAME
    echo "$USERNAME:$PASSWORD" | chpasswd
fi

# Prompt for public SSH key
read -p "Enter the public SSH key for the new user: " SSH_KEY

# Add a public SSH key for the new user account, avoiding duplicates
echo "Adding public SSH key..."
mkdir -p /home/$USERNAME/.ssh
if ! grep -qFx "$SSH_KEY" /home/$USERNAME/.ssh/authorized_keys; then
    echo "$SSH_KEY" >> /home/$USERNAME/.ssh/authorized_keys
    echo "SSH key added successfully."
else
    echo "SSH key already exists in authorized_keys file."
fi
chmod 700 /home/$USERNAME/.ssh
chmod 600 /home/$USERNAME/.ssh/authorized_keys
chown -R $USERNAME:$USERNAME /home/$USERNAME/.ssh

# Disable password authentication to the server
echo "Disabling password authentication..."
sed -i 's/#PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config
systemctl restart sshd

# Deny root login to the server
echo "Denying root login..."
sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin no/' /etc/ssh/sshd_config
systemctl restart sshd

# Setup Uncomplicated Firewall (UFW)
echo "Setting up Uncomplicated Firewall (UFW)..."
ufw allow OpenSSH
ufw --force enable

# Create Swap file based on machine's installed memory
echo "Creating Swap file..."
TOTAL_MEM=$(free -m | awk '/^Mem:/{print $2}')
if [ "$TOTAL_MEM" -le 2048 ]; then
    SWAP_SIZE=1024
elif [ "$TOTAL_MEM" -le 8192 ]; then
    SWAP_SIZE=2048
else
    SWAP_SIZE=4096
fi

dd if=/dev/zero of=/swapfile bs=1M count=$SWAP_SIZE
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo '/swapfile none swap sw 0 0' | tee -a /etc/fstab

# Setup the timezone for the server (Default value is "Asia/Kolkata")
echo "Setting up timezone..."
read -p "Enter the timezone for the server (default is Asia/Kolkata): " TIMEZONE
TIMEZONE=${TIMEZONE:-"Asia/Kolkata"}
timedatectl set-timezone $TIMEZONE

# Set up time synchronization with systemd-timesyncd
echo "Setting up time synchronization with systemd-timesyncd..."
systemctl enable systemd-timesyncd
systemctl start systemd-timesyncd

echo "Post-installation tasks completed successfully!"
