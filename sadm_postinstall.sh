#!/bin/bash
# ================================================================================================
# Redhat / CentOS / Fedora / Rocky / Alma / Ubuntu / Raspbian / Debian / Mint Postinstall Script
# Jacques Duplessis - October 2015
# Version 2.0
#
# ================================================================================================
# Version 2.0 - October 2015 - Jacques Duplessis
#               RHEL 7 Adaptation
# Version 2.5 - December 2015 - Jacques Duplessis
#               Adapted for Fedora 25
# Version 2.7 - January 2017 - Jacques Duplessis
#               Adapted for Ubuntu - Now Working for RedHat/CentOS/Fedora/Ubuntu
#               Tested on Ubuntu 16.04 - Jan 2017
# Version 2.8 - January 2017 - Jacques Duplessis
#               Tested on Ubuntu 14.04 - Jan 2017
# Version 2.9 - January 2017 - Jacques Duplessis
#               Standardize this script to run with all distribution supported
# Version 3.0 - January 2017 - Jacques Duplessis
#               Fix bugs and Tested on Debian V7 and V8 - Retested Fedora 24-25 - CentOS 7
# Version 3.1 - May 2017 - Jacques Duplessis
#               Test to work on RaspBian
#               XRDP Configuration Added fro Raspbian
#               User home directory was not created in Ubuntu/Debian/Raspbian
# Version 3.2 - May 2017 - Jacques Duplessis
#               Disable Multiple Logs - Back to one installation Log
# Version 3.3 - May 2017 - Jacques Duplessis
#               Raspberry Pi Post Installation Validated and Working
# Version 3.4 - May 2017 - Jacques Duplessis
#               Add -y command line switch to run without prompt
# Version 3.5   June 2017 - Jacques Duplessis
#               Added function for Fedora in order to load the loop module at boot time
#               Add Microsoft VsCode Repository and install VsCode at installation
# Version 3.6   Sept 2017 - Jacques Duplessis
#               V3.6 Activate Storix Installation on Ubuntu,Debian,LinuxMint
# Version 3.7   Sept 2017 - Jacques Duplessis
# 2018_07_19    v3.8    Added Chown root for /etc /etc/rear /etc/cfg2html 
# 2019_05_17 Update: v3.9 Adapted for RHEL/CentOS 8
# 2019_05_27 Update: v4.0 Bug fix for RHEL/CentOS 8
# 2019_10_06 Update: v4.1 Install chrony & Okular, remove Minecraft & Wolfram on Raspbian, Optimize.
# 2019_10_06 Update: v4.2 Change 'pi' user shell to nologin on Raspbian
# 2019_10_29 Update: v4.3 Major Code Review, send log by email to sysadmin at the end
# 2019_11_28 Update: v4.4 Static IP config to /etc/dhcpcd.conf not /etc/network/interfaces(Raspbian)
# 2019_11_28 Update: v4.5 Add Static IP CIDR that is needed to configure /etc/dhcpcd.conf (Raspbian)
# 2019_12_18 Update: v4.6 Fix update of dhcpcd on Raspbian, Decrease some part of logging.
# 2019_12_22 Update: v4.7 Bug fixes and enhancements.
# 2020_01_18 Fix: v4.8 Typo error that make script crash up front.
# 2020_02_25 Update: v4.9 Major Code revision Update
# 2020_03_20 Update: v4.10 Services Activation/De-activation revisited.
# 2020_04_19 Fixes: v4.11 Bugs Fixes (crontab,chown,service list,vscode,...) & Enhancements.
# 2020_04_21 Fixes: v4.12 Fix problem installing additional EPEL needed repo & Send log by Email
# 2020_05_23 New: v4.13 Isolate the network setup per O/S and version, for a better control.
#@2020_06_11 New: v4.14 Evaluate swap Space Size and add swap file if not big enough
#@2020_06_16 New: v4.15 Major revamp of the code, add -a to specify system ip address
#@2020_06_17 New: v4.16 Add -n to specify the hostname we want to assign to system to.
#@2020_06_18 New: v4.17 Now set Time Zone to Montreal
#@2020_06_20 New: v4.18 Fixes some bugs for Raspbian and Ubuntu
#@2020_06_23 New: v4.19 Fixes email wasn't send with log at the end and others fixes.
#@2020_06_24 New: v4.20 Probably final fix for Raspbian.
#@2020_07_08 Fix: v4.21 Network connection setup for Fedora, CentOS and Redhat using std Conn name.
#@2020_07_09 Update: v4.22 Disable SELinux on RedHat, CentOS and Fedora.
#@2020_07_10 Update: v4.23 Assign a temporary password to sadmin user.
#@2020_07_10 Update: v4.24 Add masking sleep, suspend and hibernation on server (not laptop)
#@2020_07_10 Update: v4.25 Add installation of packages needed by virtualbox Guest Additions Ext.
#@2020_07_11 Update: v4.26 Optimize startup, Misc. enhancements and bug fixes.
#@2020_07_12 Update: v4.27 Now set the 'root' password and reset SYS_ACCOUNT password.
#@2020_07_13 Update: v4.28 On Fedora, Mask the tmp.mount target to make /tmp reside to disk not mem.
#@2020_07_16 Update: v4.29 Minors corrections for Debian v10 
#@2020_07_16 Update: v4.30 Added rsync, atd, and iperf3 to install & some bug fixes.
#@2020_07_20 Update: v4.31 Added tree to installation and minors improvements.
#@2020_11_12 Update: v4.32 Change SwapSpace for Fedora,default crontab & Upd. disabling CTRL-ALT-DEL
#@2020_11_13 Update: v4.33 Change System crontab from sadm_system to sa_system.
#@2020_11_18 Update: v4.34 Added command concerning zram usage on Fedora 33 amd up.
#@2020_11_24 Update: v4.35 Minor correction to firewall masking 
#@2020_12_11 Update: v4.36 Add 'PermitUserEnvironment yes' to sshd_config & create ~/.ssh/environment
#@2020_12_15 Update: v4.37 Add 'AddressFamily inet' to sshd_config to restrict SSH to IP v4 Only.    
#@2020_12_21 Update: v4.38 Add '-l' command line switch for laptop (Leave hibernation,supend Active)
#@2020_12_26 Update: v4.39 Install 'dolphin' by default & change sysadmin email address
#@2020_12_26 Update: v4.40 If SYS-ACCOUNT exist, make sure that it's added to the group sadmin.
#@2020_12_29 Update: v4.41 Don't run function 's93_turn_off_graphical_login', Ubuntu hang on boot.
#@2021_02_09 Update: v4.42 Change 01-network-manager-all.yaml line "dhcp4:false"to "dhcp4:No"
#@2021_07_19 Update: v4.43 Change sshd log level from 'debug' to 'info' (too much entries)
#@2021_08_28 Update: v4.44 Introduction of "sadm_postinstall.cfg" installation configuration file. 
#@2021_08_28 Fix: v4.45 Fix network setting when running on Ubuntu
#@2021_08_31 Update v4.46 Add config line in rear config file
#@2021_09_03 Update v4.47 Update Storix client installation to use standard tcp/ip port
#@2021_09_03 Update v4.48 Prevent NetworkManager to update /etc/resolv.conf 
#@2021_09_03 setup v4.49 Add SCRIPT_DIR var for config file & file printing after mod. code
#@2021_11_02 setup v4.50 Fix problem when installing Raspberry Pi 2 W
#@2022_02_17 setup v4.51 Fix problem creating or not Network Manager configuration file.
#@2022_02_26 setup v4.52 Wireless ip was loaded correctly from config file
#@2022_03_06 setup v4.53 Update network config file setup 
#@2022_03_17 setup v4.54 Add Rocky, Alma Linux and Rapbian 64 bits logic
#@2022_04_07 setup v4.55 Changed for lsb_release depreciated in RHEL 9 
#@2022_04_14 setup v4.56 If lsb_release is present use it.
#@2022_04_19 setup v4.57 Include inxi during install
#@2022_05_05 setup v4.58 Update / /root permission after tar restore
#@2022_06_18 setup v4.59 Use mutt instead of mail to send install log, 
#@2022_07_20 setup v4.60 Revise, enhance and fix postfix setup
#@2022_10_27 setup v4.61 Change Ubuntu network setting to use nmcli setting.
#@2022_10_27 setup v4.62 Fix problem with postfix configuration and other enhancements.
#@2022_11_26 setup v4.63 Fix postfix password problem, git config, tar file, selinux state in cfg.
#@2022_12_06 setup v4.64 Correct problem when network connection name contains spaces (nmcli).
#@2022_12_06 setup v4.65 Add SADMIN logs to final email.
#@2023_02_05 setup v4.66 Fix EPEL installation, SELINUX, systemd_resolved, log, email
#@2023_02_12 setup v4.67 Fix problem configuring apt list for vscode under Linux Mint.
#@2023_02_13 setup v4.68 Make /etc/resolv.conf immutable after removing link and change content.
#@2023_02_13 setup v4.69 Was not creating additionnal swap space on Linux Mint.
#@2023_02_13 setup v4.70 Create sudoer file for sadmin and jacques users.
#@2023_04_13 setup v4.71 Modify sudoer file - Add secure path for sadmin and sa.
#@2023_04_18 setup v4.72 Change postfix config file /etc/postfix/main.cf using 'postconf' command.
#@2023_04_27 setup v4.73 Add 'keychain' to the list of package to install.
#@2023_04_29 setup v4.74 Fix problem with postfix config (forgot the []).
#@2023_06_23 setup v4.75 Adjust disabling ctrl-alt-del function
#@2023_07_10 setup v4.76 Add sone security setting in /etc/ssh/sshd_config file.
#@2023_07_21 setup v4.77 Try to solve email at the end of execution.
#@2023_07_25 setup v4.78 Replace 'dolphin' by 'krusader' and fix minor bugs (Postfix, sshd_config). 
#@2023_11_07 setup v4.79 Original modified file are now backup as '.org' before making the change.
#@2023_11_07 setup v4.80 Change minimum/maximum swap size (MAXSWAP="4096" MINSWAP="2048").
#@2023_11_07 setup v4.81 Install and enable rsyslog.
#@2023_11_07 setup v4.82 Add "usermod -a -G ssl-cert xrdp" to xrdp configuration on Debian
#@2023_11_07 setup v4.83 Install and configure flatpak/flathub package.
#@2023_12_26 setup v4.84 Change name of suder file from 033_sadmin-nopasswd to 033_sadmin
#@2024_01_02 setup v4.85 Many fix to ameliorate functionality.
#@2024_02_22 setup v4.86 Fix postfix config - Mail relay name
#@2024_05_22 setup v4.87 Install 'screenfetch' instead of 'neofetch'.
#@2024_07_03 setup v4.88 Use shd rsa 4096 bit, Remove rhel7 code, shorten log size.
#@2024_08_27 setup v4.89 Minor corrections and clean info written to script log.
#@2024_11_24 setup v4.90 Add O/S name in the post-install (TGZ_NAME)
# =================================================================================================
# Note for me: Add in /etc/skel default .bashrc .bash_profile
#set -x
# $ echo "password" | openssl passwd -6 -stdin
# $6$vCgAGI7fgJ0eTyRc$4BoTRAwQ.Q2hgyv6WW5pwairkhcpDYykSC3u.6uH75YXYg
# echo 'password' | passwd --stdin jacques
#wwwip()
#{  
#    dig TXT +short o-o.myaddr.l.google.com @ns1.google.com | sed 's/"//g'
#}
 


# Global Variables Definition
# ==================================================================================================
export PIVER="4.90"                                                     # PostInstall Version
export PN=${0##*/}                                                      # Script Name(with extension)
export INST=$(echo "$SADM_PN" |cut -d'.' -f1)                           # Script Name(without ext.)
export SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd)"
export DASH_LINE=$(printf %80s |tr " " "=")                              # 80 '=' sign line
export STAR_LINE=$(printf %60s |tr " " "*")                              # 60 '*' sign line
export SRC_DIR=/mnt/inst                                                # Local NFS Mount point
export CFG_FILE="${SCRIPT_DIR}/sadm_postinstall.cfg"                    # Postinstall config file
export GATEWAY=192.168.1.1                                              # Def. Net Gateway
export NETMASK="255.255.255.0"                                          # Def. Network Mask
export NETCIDR="24"                                                     # NetMask CIDR Notation
export NAME_SERVERS="192.168.1.2 192.168.1.3 192.168.1.4"               # Def. DNS Servers
export NTP_SERVERS="192.168.1.126 192.168.1.127"                        # Define NTP Servers IP
export NETWORK="192.168.1.0"                                            # Network
export NETWORK_WITH_MASK="${NETWORK}/${NETCIDR}"                        # Network with Network Mask
export BROADCAST="192.168.1.255"                                        # Network Broadcast
export DOMAIN=maison.ca                                                 # Def. Domain
export STD_FILES=$SRC_DIR/files                                         # Dir. to Extract on server
export LOG_DIR=/root                                                    # Output Log Dir.
export LOG_FILE=$LOG_DIR/sadm_postinstall.log                           # Output Log File
export ERR_FILE=$LOG_DIR/sadm_postinstall_error.log                     # Output Error File
export SCR_FILE=$LOG_DIR/sadm_script.log                                # Script Screen Output Log
export MAIL_RELAYHOST="[smtp.gmail.com]:587"                            # Mail Gateway relay
export TMP_FILE1="/tmp/postinstall_1.$$"                                # Temp File 1
export TMP_FILE2="/tmp/postinstall_2.$$"                                # Temp File 2
export SYS_EMAIL="duplessis.jacques@gmail.com"                          # SysAdmin Email
export SYS_ACCOUNT="jacques"                                            # SysAdmin Account
export SYS_NAME="Jacques Duplessis"                                     # SysAdmin Name
export DEBUG=5                                                          # Debug Level 0=None
export PROMPT="ON"                                                      # Want a confirmation before
export IPADDR=""                                                        # Wired IP Address of system
export WIPADDR=""                                                       # System Wireless IP Address 
export NOLOGIN=""                                                       # Contain 'nologin' path
export MAXSWAP="3072"                                                   # 3072=3GB Max for Swap Size
export MINSWAP="1024"                                                   # 1024=1GB Min for Swap Size
export NEW_CONNAME="sadm_wired"                                         # StdNetwork Connection name
export LAPTOP="N"                                                       # Not Installing on Laptop
export BATNAS="batnas.maison.ca"                                        # ds410 alias name
export BATBAK="batbak.maison.ca"                                        # ds409 alias name
export RASNAS="rasnas.maison.ca"                                        # RaspberryPi NAS alias name

# System Arch. x86_64,i686,aarch64,armv6l,armv7l,armv8l
export SYS_ARCH=$(uname -m)
export OS_FILE="/etc/os-release"

# Array of O/S Supported & Package Family
export OS_SUPPORTED=( 'REDHAT' 'CENTOS'    'FEDORA' 'ALMA' 'ROCKY'
                      'DEBIAN' 'RASPBIAN'  'UBUNTU' 'MINT' 'AIX' )
export REDHAT_FAMILY=('REDHAT' 'CENTOS'    'FEDORA' 'ALMA' 'ROCKY' )
export DEBIAN_FAMILY=('DEBIAN' 'RASPBIAN'  'UBUNTU' 'MINT' )
export AIX_FAMILY=(   'AIX' )

# Foreground Color
if [ -z "$TERM" ] || [ "$TERM" = "dumb" ]
    then export BLACK=""                                                # Black color
         export MAGENTA=""                                              # Magenta color
         export RED=""                                                  # Red color
         export GREEN=""                  l                             # Green color
         export YELLOW=""                                               # Yellow color
         export BLUE=""                                                 # Blue color
         export CYAN=""                                                 # Cyan color
         export WHITE=""                                                # White color
         export CLREOL=""                                               # Clear to End of Line
         export CLREOS=""                                               # Clear to End of Screen
         export BOLD=""                                                 # Set Bold Attribute
         export BELL=""                                                 # Ring the Bell
         export REVERSE=""                                              # Reverse Video On
         export UNDERLINE=""                                            # Set UnderLine On
         export HOME=""                                                 # Home Cursor
         export UP=""                                                   # Cursor up
         export DOWN=""                                                 # Cursor down
         export RIGHT=""                                                # Cursor right
         export LEFT=""                                                 # Cursor left
         export CLRSCR=""                                               # Clear Screen
         export BLINK=""                                                # Blinking on
         export NORMAL=""                                               # Reset Screen Attribute         
    else export BLACK=$(tput setaf 0)      2>/dev/null                  # Black color
         export RED=$(tput setaf 1)        2>/dev/null                  # Red color
         export GREEN=$(tput setaf 2)      2>/dev/null                  # Green color
         export YELLOW=$(tput setaf 3)     2>/dev/null                  # Yellow color
         export BLUE=$(tput setaf 4)       2>/dev/null                  # Blue color
         export MAGENTA=$(tput setaf 5)    2>/dev/null                  # Magenta color
         export CYAN=$(tput setaf 6)       2>/dev/null                  # Cyan color
         export WHITE=$(tput setaf 7)      2>/dev/null                  # White color
         export CLREOL=$(tput el)          2>/dev/null                  # Clear to End of Line
         export CLREOS=$(tput ed)          2>/dev/null                  # Clear to End of Screen
         export BOLD=$(tput bold)          2>/dev/null
         export BELL=$(tput bel)           2>/dev/null                  # Ring the Bell
         export REVERSE=$(tput rev)        2>/dev/null                  # Reverse Video 
         export UNDERLINE=$(tput sgr 0 1)  2>/dev/null                  # UnderLine
         export HOME=$(tput home)          2>/dev/null                  # Home Cursor
         export UP=$(tput cuu1)            2>/dev/null                  # Cursor up
         export DOWN=$(tput cud1)          2>/dev/null                  # Cursor down
         export RIGHT=$(tput cub1)         2>/dev/null                  # Cursor right
         export LEFT=$(tput cuf1)          2>/dev/null                  # Cursor left
         export CLRSCR=$(tput clear)       2>/dev/null                  # Clear Screen
         export BLINK=$(tput blink)        2>/dev/null                  # Blinking on
         export NORMAL=$(tput sgr0)        2>/dev/null                  # Reset Screen
fi 


which lsb_release > /dev/null 2>&1 
if [ $? -eq 0 ] 
    then SADM_LSB_RELEASE=$(which lsb_release)     # Get lsb_release cmd path   
    else if [ -f /usr/lib/dkms/lsb_release ] 
            then SADM_LSB_RELEASE="/usr/lib/dkms/lsb_release" 
            else SADM_LSB_RELEASE=""
         fi
fi 

# ==================================================================================================
# Function to write message to screen and to script log.
# ==================================================================================================
function write_log() {
    printf "%s - %s\n" "`date +"%Y-%m-%d %H:%M"`" "$1" |tee -a $LOG_FILE # write to screen & log 
}


# ==================================================================================================
# Function to write message to normal log and to error log 
# ==================================================================================================
function write_err() {
    MSG=$1                                                              # Save message received
    write_log "$MSG"                                                    # Write mess. to normal log
    printf "%s - %s\n" "`date +"%Y-%m-%d %H:%M"`" "$MSG" >> $ERR_FILE   # Write mess. to error log
}



# ==================================================================================================
# Display Question (Receive as $1) and wait for response from user, Y/y (return 1) or N/n (return 0)
# ==================================================================================================
sadm_ask() {
    wreturn=0
    wmess="$1 [y,n] ? "                                                 # Add Y/N to Mess. Rcv
    while :                                                             # While until good answer
        do
        printf "%s" "$wmess"                                            # Print "Question [Y/N] ?" 
        read answer                                                     # Read User answer
        case "$answer" in                                               # Test Answer
           Y|y ) wreturn=1                                              # Yes = Return Value of 1
                 break                                                  # Break of the loop
                 ;;
           n|N ) wreturn=0                                              # No = Return Value of 0
                 break                                                  # Break of the loop
                 ;;
             * ) echo ""                                                # Blank Line
                 ;;                                                     # Other stay in the loop
         esac
    done
    return $wreturn                                                     # Return Answer to caller 
}


# --------------------------------------------------------------------------------------------------
# Show Script command line option
#   -a (Force Wired Interface IP Address)
#   -w (Force Wireless Interface IP Address)
#   -l (Laptop Installation, leave hibernation, suspend and sleep service active)
#   -s (Install and Configure Chrony Server)
#   -y (No confirmation before beginning the post-installation)
#   -n (Force hostname)
# --------------------------------------------------------------------------------------------------
function show_usage() {
    printf "\n${YELLOW}${BOLD}${PN} v${PIVER} usage :${NORMAL}"
    printf "\n\t-a   (Force Wired interface IP Address)"
    printf "\n\t-w   (Force Wireless interface IP Address)"
    printf "\n\t-d   (Debug Level [0-9])"
    printf "\n\t-h   (Display this help message)"
    printf "\n\t-n   (Force the hostname)"
    printf "\n\t-l   (Laptop Installation, leave hibernation, suspend and sleep service active)"
    printf "\n\t-s   (Configure system as a 'chrony' server)"
    printf "\n\t-v   (Show Script Version Info)"
    printf "\n\t-y   (Run without prompting)"
    printf "\n\n" 
}


# ==================================================================================================
# Display Message (Receive as $1) and wait for Y (return 1) or N (return 0)
# ==================================================================================================
function messok() {
    wmess="$1 [y,n] ? "                                                 # Add Y/N to Mess. Rcv
    while :
        do
        printf "%s" "$wmess"                                            # Print "Question [Y/N] ?" 
        read answer                                                     # Read User answer
        case "$answer" in                                               # Test Answer
           Y|y ) wreturn=1                                              # Yes = Return Value of 1
                 break                                                  # Break of the loop
                 ;;
           n|N ) wreturn=0                                              # No = Return Value of 0
                 break                                                  # Break of the loop
                 ;;
             * ) echo ""
                 ;;                                                     # Other stay in the loop
         esac
    done
    return $wreturn
}



# ==================================================================================================
# Show title for section (Received as parameter)
# ==================================================================================================
function section_title() {
    stitle=$1
    write_log " " ; write_log " " ; write_log " " 
    write_log "${DASH_LINE}"
    write_log "Function ${stitle}" 
    write_log "${DASH_LINE}"
    write_log " "
}


# ==================================================================================================
# Show title for the Sub-Section (Received as parameter)
# ==================================================================================================
function subsection_title() {
    stitle=$1
    write_log " " 
    write_log "${STAR_LINE}"
    write_log "Sub Function ${stitle}"
    write_log "${STAR_LINE}"
    write_log " "
}



# Make a backup of file name received before modifying it - Save it under extension YYY_MM_DD_HH_MM
# ==================================================================================================
function save_original() {
    
    # Validate number of Parameter received - Should be the name of file to save
    if [ $# -ne 1 ]                                                     # Need 1 Parameter
        then write_err "[ ERROR ] Wrong Number of parameters ($#) in '${FUNCNAME}'."
             write_err "[ ERROR ] Parameters received are $*"           # Display what recv
             write_err "[ ERROR ] Post Process installation aborted"    # Advise Aborting
             exit 1                                                     # Abort Process.
    fi

    # Get filename to copy and construct backup filename.
    export FILE2SAVE="${1}"                                             # Original File Name to save
    export FILESAVED="${1}.org"                                         # Copy of the Original file
    
    if [ ! -f "${FILE2SAVE}" ]                                          # Test if file doesn't exist
        then write_err " "
             write_err "[ WARNING ] File '${FILE2SAVE}' doesn't exist, can't save original."
             write_err " "
             return 1
    fi

    # If file with extension $SAVE_EXTENSION doesn't exist, make copy of original
    if [ ! -f "${FILESAVED}" ]                                          # If Backup doesn't exist
        then #write_log " "
             write_log "Saving original version of '$FILE2SAVE' to '$FILESAVED'"    
             cp ${FILE2SAVE} ${FILESAVED} >> $LOG_FILE 2>&1             # Do actual copy
        else write_log "Backup file '$FILESAVED' already exist, we won't overwrite it."
    fi
}



# ==================================================================================================
# Disable Linux Service
# ==================================================================================================
service_disable()
{
    if [ $SYSTEMD -eq 1 ]                                               # Using systemd not Sysinit
        then systemctl disable ${1}.service  >> $LOG_FILE 2>&1
             if [ $? -eq 0 ] 
                then write_log "systemctl disable $1  [ OK ]" 
                else write_err "systemctl disable $1  [ ERROR ]" 
             fi
        else chkconfig $1 off >/dev/null 2>&1 
             if [ $? -eq 0 ] 
                then write_log "chkconfig $1 off [ OK ]"  
                else write_err "chkconfig $1 off [ ERROR ]"  
             fi
    fi
}


# ==================================================================================================
# Enable Linux Service
# ==================================================================================================
service_enable()
{
    if [ $SYSTEMD -eq 1 ]                                               # Using systemd not Sysinit
        then systemctl enable ${1}.service >> $LOG_FILE 2>&1             
             if [ $? -eq 0 ] 
                then write_log "systemctl enable $1  [ OK ]" 
                else write_err "systemctl enable $1  [ ERROR ]" 
             fi
        else chkconfig $1 on    >> $LOG_FILE 2>&1
             if [ $? -eq 0 ] 
                then write_log "chkconfig $1 on [ OK ]"  
                else write_err "chkconfig $1 on [ ERROR ]"  
             fi
    fi
}


# ==================================================================================================
# Unmask Linux Service
# ==================================================================================================
service_unmask()
{
    if [ $SYSTEMD -eq 1 ]                                               # Using systemd not Sysinit
        then write_log "systemctl unmask $1"                            # write cmd to log
             systemctl unmask $1    >> $LOG_FILE 2>&1                   # use systemctl
             if [ $? -eq 0 ] 
                then write_log "Return Code = 0" 
                else write_err "[ ERROR ] Could not enable ${1}"
             fi             
             #systemctl list-unit-files | grep $1 | tee -a $LOG_FILE     # Display Service Status
        else write_log "NO unmask on SysInit" 
    fi
}


# ==================================================================================================
# Status of Linux Service
# ==================================================================================================
service_status()
{
    if [ $SYSTEMD -eq 1 ]                                               # Using systemd not Sysinit
        then write_log "systemctl status $1"                            # write cmd to log
             systemctl status ${1}.service >> $LOG_FILE 2>&1            # use systemctl
             if [ $? -eq 0 ] 
                then write_log "Return Code = 0" 
                else write_err "[ ERROR ] Could not get status of ${1}"
             fi                
        else write_log "service $1 status"                              # write cmd to log
             service $1 status >> $LOG_FILE 2>&1                        # Show service status
             if [ $? -eq 0 ] 
                then write_log "Return Code = 0" 
                else write_err "[ ERROR ] Could not get status of ${1}"
             fi                
    fi
}


# ==================================================================================================
# Stop Linux Service
# ==================================================================================================
service_stop()
{
    if [ $SYSTEMD -eq 1 ]                                               # Using systemd not Sysinit
        then write_log "systemctl stop $1"                              # write cmd to log
             systemctl stop ${1}.service    >> $LOG_FILE 2>&1           # use systemctl
             if [ $? -eq 0 ] 
                then write_log "Return Code = 0" 
                else write_err "[ ERROR ] Could not stop service ${1}"
             fi                
        else write_log "service $1 stop"                                # write cmd to log
             service $1 stop >> $LOG_FILE 2>&1                          # stop the service
             if [ $? -eq 0 ] 
                then write_log "Return Code = 0" 
                else write_err "[ ERROR ] Could not stop service ${1}"
             fi                
    fi
}



# ==================================================================================================
# Start Linux Service
# ==================================================================================================
service_start()
{
    if [ $SYSTEMD -eq 1 ]                                               # Using systemd not Sysinit
        then write_log "systemctl start $1"
             systemctl start  ${1}.service >> $LOG_FILE 2>&1
             if [ $? -eq 0 ] 
                then write_log "Return Code = 0" 
                else write_err "[ ERROR ] Could not start service ${1}"
             fi                
        else write_log "service $1 start"
             service $1 start >> $LOG_FILE 2>&1
             if [ $? -eq 0 ] 
                then write_log "Return Code = 0" 
                else write_err "[ ERROR ] Could not start service ${1}"
             fi                
    fi
}



# ==================================================================================================
# Stop/Start Linux Service
# ==================================================================================================
function service_restart()
{
    SRV=$1
    service_stop "$SRV"
    service_start "$SRV" 
}




# ==================================================================================================
# Load Installation Answers file
# ==================================================================================================
function a12_load_config_file()
{

    # Abort if configuration file doesn't exist
    if [ ! -r "$CFG_FILE" ]                                             # If no config file, abort
        then errmsg="[ ERROR ] Postinstall configuration file ($CFG_FILE) is missing"
             printf "\n${errmsg}, process aborted."
             exit 1 
    fi 

    # Final confirmation, before we begin.
    sadm_ask "Want to review the configuration file ($CFG_FILE)"
    if [ $? -eq 1 ]                                                     # Replied Yes
       then write_log "When you are ready, run this script again."
            write_log "Starting the vi editor to review the configuration file."
            sleep 2
            vi $CFG_FILE
            exit 0  
    fi 



    # Loop for reading the sadmin configuration file
    while read wline
        do
        FC=`echo "$wline" | cut -c1`
        if [ "$FC" = "#" ] || [ ${#wline} -eq 0 ] ; then continue ; fi
        #
        
        # Laptop installation Y/N ?
        echo "$wline" |grep -i "^LAPTOP" > /dev/null 2>&1               # LAPTOP Var. in CFG
        if [ $? -eq 0 ]                                                 # Seems so, 
           then XLAPTOP=`echo "$wline"  |cut -d= -f2 |tr -d ' '`        # Get CFG LAPTOP
                if [ "$LAPTOP" != "" ] ;then LAPTOP=$XLAPTOP ;fi        # LAPTOP = CFG LAPTOP
                if [ "$LAPTOP" != "Y" ] && [ "$LAPTOP" != "N" ]         # Y or N Only
                    then printf"\nLAPTOP choice is invalid ($LAPTOP).\n"
                         exit 1
                fi 
        fi 
        
        # Chrony installation Type C=Client S=Server
        echo "$wline" |grep -i "^CHRONY" > /dev/null 2>&1               # CHRONY Var. in CFG
        if [ $? -eq 0 ]                                                 # Seems so, 
           then XCHRONY=`echo "$wline"  |cut -d= -f2 |tr -d ' '`        # Get CFG CHRONY
                if [ "$CHRONY" != "" ] ;then CHRONY=$XCHRONY ;fi        # CHRONY = CFG CHRONY
                if [ "$CHRONY" != "C" ] && [ "$CHRONY" != "S" ]         # C or S Only
                    then printf"\nChrony installation type is invalid ($CHRONY).\n"
                         exit 1
                fi 
        fi 

        # Wired Network Connection name 
        echo "$wline" |grep -i "^NEW_CONNAME" > /dev/null 2>&1          # NEW_CONNAME Var. in CFG
        if [ $? -eq 0 ]                                                 # Seems so, 
           then XNEW_CONNAME=`echo "$wline"  |cut -d= -f2 |tr -d ' '`   # Get CFG NEW_CONNAME
                if [ "$XNEW_CONNAME" != "" ] ;then NEW_CONNAME=$XNEW_CONNAME ;fi      
        fi 
                           
        # Primary Network Device 
        echo "$wline" |grep -i "^ETHDEV" > /dev/null 2>&1               # ETHDEV Var. in CFG
        if [ $? -eq 0 ]                                                 # Seems so, 
           then XETHDEV=`echo "$wline"  |cut -d= -f2 |tr -d ' '`        # Get CFG ETHDEV
                if [ "$XETHDEV" != "" ] ;then ETHDEV=$XETHDEV ;fi       # ETHDEV = CFG ETHDEV
        fi 
                             
        # Network
        echo "$wline" |grep -i "^NETWORK" > /dev/null 2>&1              # NETWORK Var. in CFG
        if [ $? -eq 0 ]                                                 # Seems so, 
           then XNETWORK=`echo "$wline"  |cut -d= -f2 |tr -d ' '`       # Get CFG NETWORK
                if [ "$XNETWORK" != "" ] ;then NETWORK=$XNETWORK ;fi    # NETWORK = CFG NETWORK
        fi 
                
        # Netmask
        echo "$wline" |grep -i "^NETMASK" > /dev/null 2>&1              # NETMASK Var. in CFG
        if [ $? -eq 0 ]                                                 # Seems so, 
           then XNETMASK=`echo "$wline"  |cut -d= -f2 |tr -d ' '`       # Get CFG NETMASK
                if [ "$XNETMASK" != "" ] ;then NETMASK=$XNETMASK ;fi    # NETMASK = CFG NETMASK
        fi 
                                
        # Network CIDR
        echo "$wline" |grep -i "^NETCIDR" > /dev/null 2>&1              # NETCIDR Var. in CFG
        if [ $? -eq 0 ]                                                 # Seems so, 
           then XNETCIDR=`echo "$wline"  |cut -d= -f2 |tr -d ' '`       # Get CFG NETCIDR
                if [ "$XNETCIDR" != "" ] ;then NETCIDR=$XNETCIDR ;fi    # NETCIDR = CFG NETCIDR
        fi 
                
        # Gateway
        echo "$wline" |grep -i "^GATEWAY" > /dev/null 2>&1              # GATEWAY Var. in CFG
        if [ $? -eq 0 ]                                                 # Seems so, 
           then XGATEWAY=`echo "$wline"  |cut -d= -f2 |tr -d ' '`       # Get CFG GATEWAY
                if [ "$XGATEWAY" != "" ] ;then GATEWAY=$XGATEWAY ;fi    # GATEWAY = CFG GATEWAY
        fi 
                
        # Broadcast
        echo "$wline" |grep -i "^BROADCAST" > /dev/null 2>&1            # BROADCAST Var. in CFG
        if [ $? -eq 0 ]                                                 # Seems so, 
           then XBROADCAST=`echo "$wline"  |cut -d= -f2 |tr -d ' '`     # Get CFG BROADCAST
                if [ "$XBROADCAST" != "" ] ;then BROADCAST=$XBROADCAST ;fi # BROADCAST=CFG BROADCAST
        fi 
                 
        # Host Name
        echo "$wline" |grep -i "^HOSTNAME" > /dev/null 2>&1             # HOSTNAME Var. in CFG
        if [ $? -eq 0 ]                                                 # Seems so, 
           then XHOSTNAME=`echo "$wline"  |cut -d= -f2 |tr -d ' '`      # Get CFG Hostname
                if [ "$XHOSTNAME" != "" ] ;then HOSTNAME=$XHOSTNAME ;fi # Hostname = CFG hostname
        fi 
        
        # Domain Name
        echo "$wline" |grep -i "^DOMAIN" > /dev/null 2>&1               # DOMAIN Var. in CFG
        if [ $? -eq 0 ]                                                 # Seems so, 
           then XDOMAIN=`echo "$wline"  |cut -d= -f2 |tr -d ' '`        # Get CFG Domain
                if [ "$XDOMAIN" != "" ] ;then DOMAIN=$XDOMAIN ;fi       # Domain = CFG DomainName
        fi 

        # Wireless IP Address
        echo "$wline" |grep -i "^WIPADDR" > /dev/null 2>&1
        if [ $? -eq 0 ] 
            then XWIPADDR=`echo "$wline" |cut -d= -f2 |tr -d ' '` 
                 if [ "$XWIPADDR" != "" ] ; then WIPADDR=$XWIPADDR ; fi
        fi

        # Wired IP Address
        echo "$wline" |grep -i "^IPADDR" > /dev/null 2>&1
        if [ $? -eq 0 ] 
            then XIPADDR=`echo "$wline" |cut -d= -f2 |tr -d ' '` 
                 if [ "$XIPADDR" != "" ] ; then IPADDR=$XIPADDR ; fi
        fi

        # Domain Name Servers
        echo "$wline" |grep -i "^NAME_SERVERS" > /dev/null 2>&1
        if [ $? -eq 0 ] 
            then XNAME_SERVERS=`echo "$wline" |cut -d= -f2 | awk '{$1=$1;print}'` 
                 if [ "$XNAME_SERVERS" != "" ] ; then NAME_SERVERS=$XNAME_SERVERS ; fi
        fi

        # SELinux state desired
        echo "$wline" |grep -i "^SELINUX" > /dev/null 2>&1
        if [ $? -eq 0 ] 
            then CUR_SELINUX=`echo "$wline" |cut -d= -f2 | awk '{$1=$1;print}'` 
                 if [ "$CUR_SELINUX" != "" ] 
                    then CUR_SELINUX=`echo $CUR_SELINUX |tr '[:upper:]' '[:lower:]'`
                    else CUR_SELINUX="disabled" 
                 fi
                 if [ "$CUR_SELINUX" != "disabled" ] && [ "$CUR_SELINUX" != "permissive" ] && [ "$CUR_SELINUX" != "enforcing" ]
                    then write_log "Invalid SELINUX state '$CUR_SELINUX' specified in $CFG_FILE" 
                         write_log "Process aborted" 
                         exit 1
                 fi 
        fi

        # NTP Servers
        echo "$wline" |grep -i "^NTP_SERVERS" > /dev/null 2>&1
        if [ $? -eq 0 ] 
            then XNTP_SERVERS=`echo "$wline" |cut -d= -f2 |awk '{$1=$1;print}'` 
                 if [ "$XNTP_SERVERS" != "" ] ; then NTP_SERVERS=$XNTP_SERVERS ; fi
        fi


        # System Administrator Email
        echo "$wline" |grep -i "^SYS_EMAIL" > /dev/null 2>&1
        if [ $? -eq 0 ] 
            then XSYS_EMAIL=`echo "$wline" |cut -d= -f2 |tr -d ' '` 
                 if [ "$XSYS_EMAIL" != "" ] ; then SYS_EMAIL=$XSYS_EMAIL ; fi
        fi

        # Main User Account Name
        echo "$wline" |grep -i "^SYS_ACCOUNT" > /dev/null 2>&1
        if [ $? -eq 0 ] 
            then XSYS_ACCOUNT=`echo "$wline" |cut -d= -f2 |tr -d ' '` 
                 if [ "$XSYS_ACCOUNT" != "" ] ; then SYS_ACCOUNT=$XSYS_ACCOUNT ; fi
        fi

        # Main User Full Name
        echo "$wline" |grep -i "^SYS_NAME" > /dev/null 2>&1
        if [ $? -eq 0 ] 
            then XSYS_NAME=`echo "$wline" |cut -d= -f2 | awk '{$1=$1;print}'` 
                 if [ "$XSYS_NAME" != "" ] ; then SYS_NAME=$XSYS_NAME ; fi
        fi

        # Postinstall Log file name
        echo "$wline" |grep -i "^LOG_FILE" > /dev/null 2>&1
        if [ $? -eq 0 ] 
            then XLOG_FILE=`echo "$wline" |cut -d= -f2 |tr -d ' '` 
                 if [ "$XLOG_FILE" != "" ] ; then LOG_FILE=$XLOG_FILE ; fi
        fi

        # Postinstall Error Log file name
        echo "$wline" |grep -i "^ERR_FILE" > /dev/null 2>&1
        if [ $? -eq 0 ] 
            then XERR_FILE=`echo "$wline" |cut -d= -f2 |tr -d ' '` 
                 if [ "$XERR_FILE" != "" ] ; then ERR_FILE=$XERR_FILE ; fi
        fi

        # Internet Mail Relay Host
        echo "$wline" |grep -i "^MAIL_RELAYHOST" > /dev/null 2>&1
        if [ $? -eq 0 ] 
            then XMAIL_RELAYHOST=`echo "$wline" |cut -d= -f2 |tr -d ' '` 
                 if [ "$XMAIL_RELAYHOST" != "" ] ; then MAIL_RELAYHOST=$XMAIL_RELAYHOST ; fi
        fi

        # Internet Mail Sender Address
        echo "$wline" |grep -i "^MAIL_SENDER" > /dev/null 2>&1
        if [ $? -eq 0 ] 
            then XMAIL_SENDER=`echo "$wline" |cut -d= -f2 |tr -d ' '` 
                 if [ "$XMAIL_SENDER" != "" ] ; then MAIL_SENDER=$XMAIL_SENDER ; fi
        fi

        # Internet Mail Sender Password
        echo "$wline" |grep -i "^MAIL_PASSWD" > /dev/null 2>&1
        if [ $? -eq 0 ] 
            then XMAIL_PASSWD=`echo "$wline" |cut -d= -f2 |tr -d ' '` 
                 if [ "$XMAIL_PASSWD" != "" ] ; then MAIL_PASSWD=$XMAIL_PASSWD ; fi
        fi

        # NFS Local Mount Point
        echo "$wline" |grep -i "^SRC_DIR" > /dev/null 2>&1
        if [ $? -eq 0 ] 
            then XSRC_DIR=`echo "$wline" |cut -d= -f2 |tr -d ' '` 
                 if [ "$XSRC_DIR" != "" ] ; then SRC_DIR=$XSRC_DIR ; fi
        fi

        # NFS Local Dir. for files to copy on system
        echo "$wline" |grep -i "^STD_FILES" > /dev/null 2>&1
        if [ $? -eq 0 ] 
            then XSTD_FILES=`echo "$wline" |cut -d= -f2 |tr -d ' '` 
                 if [ "$XSTD_FILES" != "" ] ; then STD_FILES=$XSTD_FILES ; fi
        fi

        # SA Utility Installation directory
        echo "$wline" |grep -i "^SA " > /dev/null 2>&1
        if [ $? -eq 0 ] ; then SA=`echo "$wline" |cut -d= -f2 |tr -d ' '` ;fi

        done < $CFG_FILE
    return 0
}



# ==================================================================================================
# Set System Environment variables that will be used in this script.
# ==================================================================================================
function a10_set_env()
{
    # Get the active ethernet interface name 
    #export ETHDEV=`netstat -rn|grep '^0.0.0.0' |head -1 |awk '{print $NF}'` # Eth Device Name
    export ETHDEV=`ip r | grep -i default | head -1 | awk '{ print $5 }'`

    # Are we using Sysinit or systemd ?
    command -v systemctl > /dev/null 2>&1                               # Check if systemd on system
    if [ $? -eq 0 ] ; then export SYSTEMD=1 ; else export SYSTEMD=0 ;fi # SYSTEMD use or not 


    # Establish and Validate the Name of OS in Uppercase (CENTOS/REDHAT/UBUNTU/FEDORA...)
    OS_NAME=$(awk -F= '/^ID=/ {print $2}' $OS_FILE |tr -d '"' |tr '[:lower:]' '[:upper:]')         
    if [ "$OS_NAME" = "REDHATENTERPRISESERVER" ] ; then OS_NAME="REDHAT" ; fi
    if [ "$OS_NAME" = "RHEL" ]                   ; then OS_NAME="REDHAT" ; fi
    if [ "$OS_NAME" = "REDHATENTERPRISEAS" ]     ; then OS_NAME="REDHAT" ; fi
    if [ "$OS_NAME" = "REDHATENTERPRISE" ]       ; then OS_NAME="REDHAT" ; fi
    if [ "$OS_NAME" = "CENTOSSTREAM" ]           ; then OS_NAME="CENTOS" ; fi
    if [ "$OS_NAME" = "ALMALINUX" ]              ; then OS_NAME="ALMA"   ; fi
    if [ "$OS_NAME" != "REDHAT" ] && [ "$OS_NAME" != "CENTOS" ] && [ "$OS_NAME" != "FEDORA" ] &&
       [ "$OS_NAME" != "UBUNTU" ] && [ "$OS_NAME" != "DEBIAN" ] && [ "$OS_NAME" != "RASPBIAN" ] &&
       [ "$OS_NAME" != "ALMA"   ] && [ "$OS_NAME" != "ROCKY"  ] && [ "$OS_NAME" != "MINT" ]
       then printf "This distribution of Linux ($OS_NAME) is currently not supported.\n"
            exit 1
    fi

    # If on Raspberry Pi 64 Bits, make sure name of O/S is "RASBIAN" not "DEBIAN"
    if [ -f "/usr/bin/raspi-config" ] ; then OS_NAME="RASPBIAN" ; fi 
    # Is the O/S name in the O/S Supported Array
    if [[ " ${OS_SUPPORTED[*]} " =~ "${OS_NAME}" ]]
        then printf "\n"
        else printf "[ ERROR ] O/S $OS_NAME is not supported\n\n"
             exit 1
    fi

    # Determine if system is using 'rpm' or 'dpkg' 
    which rpm >/dev/null 2>&1
    if [ $? -eq 0 ] ; then export PACKTYPE="rpm" ; else export PACKTYPE="deb" ;fi 

    # Determine if running kernel is a 32 or 64 bits
    export OS_BITS=`getconf LONG_BIT`                                   # 32 or 64 Bits
    if [ $OS_BITS -ne 32 ] && [ $OS_BITS -ne 64 ]                       # If not 32 or 64 Abort
       then printf "Kernel can only be 32 or 64 bits not $OS_BITS \n"   # Advise User
            exit 1                                                      # Exit with Error
    fi

    # Get O/S Full Version Number
    if [ "$SADM_LSB_RELEASE" != "" ] && [ -x "$SADM_LSB_RELEASE" ]
       then SADM_OSFULLVER=$($SADM_LSB_RELEASE -rs)
       else if [ -f $OS_FILE ] 
                then SADM_OSFULLVER=$(awk -F= '/^VERSION_ID=/ {print $2}' $OS_FILE |tr -d '"')
                else if [ -f /etc/system-release-cpe ] 
                        then SADM_OSFULLVER=$(awk -F: '{print $5}' /etc/system-release-cpe)
                        else printf "Couldn't get O/S version\n"
                             SADM_OSFULLVER=0.0
                     fi
            fi 
    fi 
   
    # OS Major version (Redhat/CentOS/Rocky/Alma 6,7,8,9 - Fedora 35,36,37 - Ubuntu 20,21,22,24
    OS_VERSION=$(echo $SADM_OSFULLVER | awk -F. '{ print $1 }'| tr -d ' ')
    echo "Your System is running $SADM_OSNAME Version $OS_VERSION ..." >> $LOG_FILE 2>&1
    
    # Get current IP Address of System
    #export IPADDR=`ip addr show | grep global | head -1 | awk '{print $2}' |awk -F/ '{ print $1 }'`
    export IPADDR="$(hostname -I | awk '{print $1}')"
    
    # Set the Hostname as per name resolution of IP Address
    export HOSTNAME=`host $IPADDR | head -1 | awk '{ print $NF }' | cut -d. -f1`  
    
    # Chrony Default Value (Can be set to server by using command line switch -s)
    export CHRONY="C"                                                   # Chrony C=CLient S=Server

    # If not already set, then Set default SADMIN installation directory 
    if [ -z "$SADMIN" ]; then export SADMIN="/opt/sadmin" ;fi 
    if [ -z "$SA" ]    ; then export SA="/opt/sa"   ;fi 

}




# --------------------------------------------------------------------------------------------------
# Command line Options functions
# Evaluate Command Line Switch Options Upfront
# By Default (-h) Show Help Usage, (-v) Show Script Version, (-d[0-9]) Set Debug Level 
# --------------------------------------------------------------------------------------------------
function a14_cmd_options()
{
   # Optional Command line switch - (-y bypass startup confirmation)
   # -a (Force Wired Interface IP Address)
   # -w (Force Wireless Interface IP Address)
   # -l (Laptop Installation, leave hibernation, suspend and sleep service active)
   # -s (Install and Configure Chrony Server)
   # -y (No confirmation before beginning the post-installation)
   # -n (Force hostname)
    while getopts "a:w:n:d:yhs" opt ; do                                # Loop to process Switch
        case $opt in
            a) IPADDR=$OPTARG                                           # Set Wired Address of system
               ;;
            w) WIPADDR=$OPTARG                                          # Set Wireless IP Address
               ;;
            s) CHRONY="S"                                               # Configure as chrony server
               ;;
            l) LAPTOP="Y"                                               # Laptop Installation
               ;;
            n) HOSTNAME=$OPTARG                                         # Force HostName (option -n)
               ;;
            d) DEBUG=$OPTARG                                            # Get Debug Level Specified
               num=`echo "$DEBUG" | grep -E ^\-?[0-9]?\.?[0-9]+$`       # Valid is Level is Numeric
               if [ "$num" = "" ]                                       # No it's not numeric 
                  then printf "\nDebug Level specified is invalid.\n"   # Inform User Debug Invalid
                       show_usage                                       # Display Help Usage
                       exit 1                                           # Exit Script with Error
               fi
               printf "Debug Level set to ${SADM_DEBUG}."               # Display Debug Level
               ;;              
            y) PROMPT="OFF"                                             # Run Without Prompt
               ;;                                                       # No stop after each page
            h) show_usage                                               # Display Help Usage
               exit 0                                                   # Back to shell
               ;;
           \?) echo "Invalid option: -$OPTARG" >&2                      # Invalid Option Message
               show_usage                                               # Display Help Usage
               exit 1                                                   # Exit with Error
               ;;
        esac                                                            # End of case
    done
    return
}




# ==================================================================================================
# Write Environnement Variables Used in the Script.
# ==================================================================================================
function a16_show_env()
{
    # Start brand new log and error file.
    printf "\nsadm_postinstall v${PIVER}\n\n" 
    echo -e "Starting postinstall v${PIVER} on `date` on ${HOSTNAME}\n" > $LOG_FILE
    echo -e "Starting postinstall v${PIVER} on `date` on ${HOSTNAME}\n" > $ERR_FILE

    write_log "\$OS_NAME (O/S Name in Uppercase)                = $OS_NAME"
    write_log "\$OS_VERSION (O/S Major Number)                  = $OS_VERSION"
    write_log "\$OS_BITS (32 bits or 64 bits)                   = $OS_BITS"
    write_log "\$SYS_ARCH(amd64,arch7l,x86_64,aarch64,arch8l)   = $SYS_ARCH"
    write_log "\$CUR_SELINUX (permissive, disabled, enforcing)  = $CUR_SELINUX"
    write_log "\$PACKTYPE (rpm or deb)                          = $PACKTYPE" 
    write_log "\$IPADDR (Wired IP Address)                      = $IPADDR"
    write_log "\$WIPADDR (Wireless IP Address)                  = $WIPADDR"
    write_log "\$HOSTNAME (System HostName)                     = $HOSTNAME"
    write_log "\$DOMAIN (System Domain Name)                    = $DOMAIN"
    write_log "\$NETMASK (Network Mask)                         = $NETMASK"
    write_log "\$NETCIDR (Classless Inter-Domain Routing)       = $NETCIDR"
    write_log "\$GATEWAY (Network Gateway)                      = $GATEWAY"
    write_log "\$ETHDEV (Ethernet Network Device)               = $ETHDEV"
    write_log "\$BROADCAST (Network Broadcast Address)          = $BROADCAST"
    write_log "\$NETWORK (Network IP)                           = $NETWORK"
    write_log "\$NETWORK_WITH_MASK (Network/CIDR)               = $NETWORK_WITH_MASK"
    write_log "\$NAME_SERVERS (Domain Name Servers - DNS)       = $NAME_SERVERS"
    write_log "\$NTP_SERVERS (Network Time Protocol Servers)    = $NTP_SERVERS"
    write_log "\$SYS_EMAIL (SysAdmin Email)                     = $SYS_EMAIL"
    write_log "\$SYS_ACCOUNT (SysAdmin account to create)       = $SYS_ACCOUNT"
    write_log "\$SYS_NAME (SysAdmin Name)                       = $SYS_NAME"
    write_log "\$SRC_DIR (NFS mount point)                      = $SRC_DIR"
    write_log "\$STD_FILES (Files to extract on / system)       = $STD_FILES"
    write_log "\$LOG_FILE (Script log file name)                = $LOG_FILE"
    write_log "\$ERR_FILE (Script error log file name)          = $ERR_FILE"
    write_log "\$MAIL_RELAYHOST (ISP SMTP Relay Mail Host)      = $MAIL_RELAYHOST"
    write_log "\$MAIL_SENDER (Email account that send email)    = $MAIL_SENDER"
    write_log "\$MAIL_SENDER_PASSWORD (Email account password)  = $MAIL_PASSWD"
    write_log "\$SYSTEMD ((0=Sysinit 1=Systemd))                = $SYSTEMD"
    write_log "\$SADMIN (SADMIN install Dir)                    = $SADMIN"
    write_log "\$CHRONY (C=Client or S=Server)                  = $CHRONY"
    write_log "\$MINSWAP (Minimum swap size)                    = ${MINSWAP}MB"
    write_log "\$MAXSWAP (Maximum swap size)                    = ${MAXSWAP}MB"
    write_log "\$LAPTOP (Installing on a Laptop ?)              = ${LAPTOP}"
    write_log "\$BATNAS (Main NAS backup server)                = ${BATNAS}"
    write_log "\$BATBAK (Secondary NAS backup server)           = ${BATBAK}"
    write_log "\$RASNAS (Raspberry Pi NAS backup server)        = ${RASNAS}"
    write_log "\$NEW_CONNAME (Network Manager Connection Name)  = ${NEW_CONNAME}"
    write_log "\$SA (My Sysadmin directory name)                = ${SA}"
}



# --------------------------------------------------------------------------------------------------
# Validation before processing begin
# --------------------------------------------------------------------------------------------------
function a20_validation()
{
    # If -y command line switch was not specified - Display Warning & Wait for confirmation
    if [ "$PROMPT" != "OFF" ]                                           # Prompt ON,Ask Confirmation
        then printf "\nServer will reboot after running this script."
             printf "\nYou may want to review this log after the reboot %s.\n" "$LOG_FILE"
             messok "Still want to proceed"                             # Wait for user Answer (y/n)
             if [ "$?" -eq 0 ] ; then exit 1 ; fi                       # Do not proceed
    fi

    if [ ! -f "${STD_FILES}/files.tar.gz" ]
        then write_log ""
             write_log "Missing Standard Fileset - ${STD_FILES}/files.tar.gz"
             write_log "CREATE OR RECREATE THE FILE AND RUN THE INSTALLATION AGAIN"
             write_log "1- cd /install"
             write_log "2- run this script : ./sadm_create_installation_tgz.sh "
             write_log "3- Run installation again"
             exit 1
    fi
}



# --------------------------------------------------------------------------------------------------
# Disable SELinux on Redhat/CentOS/Fedora/Rocky/AlmaLinux
# --------------------------------------------------------------------------------------------------
function n08_set_selinux()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION}" 
    F="/etc/selinux/config"                                             # SELinux Config file name
    if [ -f $F ] 
        then CUR_SELINUX=`getenforce`                                   # Get SELINUX Status
             if [ "$CUR_SELINUX" = "Enforcing" ]                        # If SELinux is enforce
                 then write_log "Get SELinux status with 'getenforce'" 
                      getenforce | while read wline ;do write_log "$wline"; done 
                      write_log "Disabling SELinux temporarely with 'setenforce 0' ..."      
                      write_log "setenforce 0" 
                      setenforce 0 | while read wline ; do write_log "$wline"; done 
                      write_log "Get SELinux status with 'getenforce'" 
                      getenforce | while read wline ;do write_log "$wline"; done 
             fi 

             # Disable SELinux
             CUR_SELINUX="disabled" 
             write_log "Set SELinux state to '$CUR_SELINUX' in config file ($F) ..." 
             sed -i '/^SELINUX=/d' $F 
             echo "SELINUX=$CUR_SELINUX" >> $F 

             # Show new content    
             write_log "Updated content of $F :"                         # will print SELinux config
             write_log "**********"
             grep -Ev "^$|^#"  $F | while read wline ; do write_log "$wline"; done 
             write_log "**********"

        else write_log "SELinux is not installed (no $F)."
    fi    
}




# Setup and Display the /etc/hosts file
# ==================================================================================================
function n10_setup_host_name()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION}" 
    save_original "/etc/hosts"                                          # Backup Original

    # Verify Wired IPAddress line is already there in /etc/hosts
    grep -i "$HOSTNAME.$DOMAIN" /etc/hosts >/dev/null 2>&1
    if [ $? -ne 0 ]                                                     # If not already there
        then echo "#" >> /etc/hosts                                     # Add comment line
             echo -e "${IPADDR}\t\t${HOSTNAME}.${DOMAIN}\t\t${HOSTNAME}" >> /etc/hosts  
    fi 

    # Verify Wireless IPAddress line is already there in /etc/hosts
    if [ "$WIPADDR" != "" ]                                             # If Wireless IP Specified
       then HOSTNAMEW=`host $WIPADDR | head -1 | awk '{ print $NF }' | cut -d. -f1` # Get IP Name
            grep -i "$HOSTNAMEW.$DOMAIN" /etc/hosts >/dev/null 2>&1     # Wireless Name in /etc/host
            if [ $? -ne 0 ]                                             # If not already there
               then echo -e "${WIPADDR}\t\t${HOSTNAMEW}.${DOMAIN}\t\t${HOSTNAMEW}" >> /etc/hosts 
                    echo "#" >> /etc/hosts
            fi 
    fi 

    # Make sure batnas.maison.ca is in the /etc/hosts file (In case DNS is down.)
    grep -i "$BATNAS" /etc/hosts >/dev/null 2>&1
    if [ $? -ne 0 ]                                                     # If not already there
        then echo "#" >> /etc/hosts                                     # Add comment line
             echo "# Backup systems IP " >>/etc/hosts                   # Reason to add entry
             DNS1=`echo $NAME_SERVERS | awk '{print $1}'`
             BATNASIP=$(dig "$BATNAS" @$DNS1 +short | sort | head -1 | awk '{ print $NF }')
             #BATNASIP=$(host "$BATNAS" | head -1 | awk '{ print $NF }') # Get batnas IP
             SHORT_NAME=$(echo "$BATNAS" | awk -F\. '{print $1}')       # Remove Domain name
             echo -e "${BATNASIP}\t\t${BATNAS}\t\t${SHORT_NAME}" >> /etc/hosts  
    fi 

    # Make sure rasnas.maison.ca is in the /etc/hosts file (In case DNS is down.)
    grep -i "$RASNAS" /etc/hosts >/dev/null 2>&1
    if [ $? -ne 0 ]                                                     # If not already there
        then DNS1=`echo $NAME_SERVERS | awk '{print $1}'`
             RASNASIP=$(dig "$RASNAS" @$DNS1 +short | sort | head -1 | awk '{ print $NF }')
             #RASNASIP=$(host "$RASNAS" | head -1 | awk '{ print $NF }') # Get RASNAS IP
             SHORT_NAME=$(echo "$RASNAS" | awk -F\. '{print $1}')       # Remove Domain name
             echo -e "${RASNASIP}\t${RASNAS}\t\t${SHORT_NAME}" >> /etc/hosts  
    fi 

    # Make sure BATBAK.maison.ca is in the /etc/hosts file (In case DNS is down.)
    grep -i "$BATBAK" /etc/hosts >/dev/null 2>&1
    if [ $? -ne 0 ]                                                     # If not already there
        then DNS1=`echo $NAME_SERVERS | awk '{print $1}'`
             BATBAKIP=$(dig "$BATBAK" @$DNS1 +short | sort | head -1 | awk '{ print $NF }')
             #BATBAKIP=$(host "$BATBAK" | head -1 | awk '{ print $NF }') # Get BATBAK IP
             SHORT_NAME=$(echo "$BATBAK" | awk -F\. '{print $1}')       # Remove Domain name
             echo -e "${BATBAKIP}\t\t${BATBAK}\t\t${SHORT_NAME}" >> /etc/hosts  
    fi 

    write_log "Show updated content of /etc/hosts :"                    # will print /etc/hosts Msg
    write_log "**********"
    cat /etc/hosts | while read wline ; do write_log "$wline"; done     # Write file to Screen & Log
    write_log "**********"

    # Activate the Hostname.
    write_log " "                                                       # Log white line
    write_log "Activating Hostname"                                     # Advise User
    write_log "/bin/hostname ${HOSTNAME}.${DOMAIN}"                     # Shw user cmd executed 
    /bin/hostname ${HOSTNAME}.${DOMAIN}                                 # Set FQN Host Name
    if [ $? -eq 0 ]                                                     # If Cmd worked OK 
        then write_log "Command executed with success."                 # Advise user everything OK
        else write_log "Command executed returned an error."            # Advise user of error
    fi 
    
    if [ -f /etc/hostname ]                                             # /etc/hostname file exist ?
        then write_log " "
             save_original "/etc/hostname"                              # Backup Original
             write_log "Creating new /etc/hostname"                     # Show what we are doing
             write_log "echo '${HOSTNAME}.${DOMAIN}' > /etc/hostname"   # Show what we do 
             echo "${HOSTNAME}.${DOMAIN}" > /etc/hostname               # Set HostName
    fi 

    if [ $SYSTEMD -eq 1 ]                                               # If system running systemd
        then write_log " "                                              # Log white line
             write_log "Running : hostnamectl set-hostname ${HOSTNAME}.${DOMAIN}" # Set Systemd Hostname 
             hostnamectl set-hostname ${HOSTNAME}.${DOMAIN} > /dev/null 2>&1 
             if [ $? -eq 0 ]                                            # If Cmd worked OK 
                then write_log "Command executed with success."         # Advise user everything OK
                else write_log "Command executed returned an error."    # Advise user of error
             fi 
             write_log " " 
             write_log "hostnamectl"
             hostnamectl | tee -a $LOG_FILE 
             write_log " " 
        else write_log " " 
             write_log "hostname"
             hostname  | tee -a $LOG_FILE 
             write_log " " 
    fi
}




# ==================================================================================================
# N20 Setup System Networking
# ==================================================================================================
function n20_setup_network()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION}" 

    case "$OS_NAME" in
        "REDHAT"|"CENTOS"|"FEDORA"|"ROCKY"|"ALMA"|"UBUNTU"|"MINT")
                if [ "$OS_VERSION" -lt 7 ] && [ "$OS_NAME" != "FEDORA" ] # Old ways o Network Setup
                    then write_log "Setup network for $OS_NAME v$OS_VERSION"
                         n22_redhat_old_network_setup                   # Old RedHat,CentOS,Fedora
                    else write_log "Setup network for $OS_NAME v$OS_VERSION"
                         n23_nmcli_network_setup                        # New RedHat,CentOS,Fedora
                         write_log " "
                         service_enable NetworkManager                  # Redhat Recommend ON in V7
                fi 
                ;;
        "RASPBIAN")
                write_log "Setup network for Raspbian"
                n26_raspbian_network_setup                              # Setup Raspbian Network Dev
                ;;

        "DEBIAN")
                write_log "Setup network for Debian"
                n28_debian_network_setup                                # Setup Debian NetDev
                ;;

        "*" )
                write_log "The network setup for $OS_NAME is not supported yet."
                ;;
    esac

}




# ==================================================================================================
# N22 Old way of Setup Redhat, CentOS, Fedora Network
# ==================================================================================================
function n22_redhat_old_network_setup()
{
    subsection_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION}" 

    if [ -f /etc/sysconfig/network ] 
       then wfile="/etc/sysconfig/network"                              # Network Config File
            save_original "$wfile"                                      # Save original dhcpcd file
            write_log "--- Original $wfile content:"                    # User Header for file id.
            cat $wfile |while read wline ;do write_log "$wline" ;done   # Show Original file
            write_log " "
            echo "NETWORKING=yes"               >  $wfile
            echo "NOZEROCONF=no"                >> $wfile
            echo "HOSTNAME=$HOSTNAME.$DOMAIN"   >> $wfile
            echo "GATEWAY=$GATEWAY"             >> $wfile
            chown root:root $wfile
            chmod 644 $wfile
            write_log " "
            write_log "Updated content of $wfile :"
            write_log "**********"
            cat $wfile | while read wline ; do write_log "$wline"; done # Write file to Log
            write_log "**********"
    fi

    wfile="/etc/sysconfig/network-scripts/ifcfg-${ETHDEV}"              # Network Config file
    write_log " " 
    write_log "Original $wfile content:"
    write_log "**********"
    cat $wfile | while read wline ; do write_log "$wline"; done
    write_log "**********"
    write_log " "
    save_original "$wfile"                                              # Backup Original

    # Modify Network in /etc/sysconfig/network-scripts/ifcfg-${ETHDEV}
    cp $wfile $TMP_FILE1                                                # Tmp work file
    grep -vEi "ONBOOT|NM_CONTROLLED|BOOTPROTO" $wfile   >  $TMP_FILE1
    echo "ONBOOT=yes"                                   >> $TMP_FILE1
    echo "BOOTPROTO=none"                               >> $TMP_FILE1
    echo "NM_CONTROLLED=no"                             >> $TMP_FILE1
    echo "IPADDR=$IPADDR"                               >> $TMP_FILE1
    echo "GATEWAY=$GATEWAY"                             >> $TMP_FILE1
    echo "NETMASK=$NETMASK"                             >> $TMP_FILE1
    echo "DNS1=`echo $NAME_SERVERS | awk '{print $1}'`" >> $TMP_FILE1
    echo "DNS2=`echo $NAME_SERVERS | awk '{print $2}'`" >> $TMP_FILE1
    if [ "$DNS3" != "" ] 
       then echo "DNS3=`echo $NAME_SERVERS | awk '{print $3}'`" >> $TMP_FILE1
    fi
    #echo "ETHTOOL_OPTS=\"speed 100 duplex full autoneg off\"" >> $TMP_FILE1
    cp $TMP_FILE1 $wfile                                                # put in place new version
    chmod 0644 $wfile ; chown root:root $wfile                          # Change File Priv. & Prot.

    # Show Modified file.
    write_log " "
    write_log "Updated content of $wfile :"                
    write_log "**********"
    cat $wfile | while read wline ; do write_log "$wline"; done         # Write Output to Log
    write_log "**********"

    return 
}



# ==================================================================================================
# N23 New Network Setup for Redhat, CentOS, Fedora 
# ==================================================================================================
function n23_nmcli_network_setup()
{
    subsection_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION}" 

    # Show all available interfaces on server using the nmcli command below.
    write_log " " 
    CMD="nmcli connection show" 
    write_log "Show actual network configuration: "
    $CMD | while read wline ; do write_log "$wline"; done               # Write Output to Log

    # Show Devices Status
    write_log " " 
    CMD="nmcli dev status" 
    write_log "Show actual network devices status :"
    $CMD | while read wline ; do write_log "$wline"; done               # Write Output to Log

    # Get current Active connection using the actual device (ETHDEV)
    write_log " " 
    #CUR_CONNAME=`nmcli dev status | grep "$ETHDEV" | awk '{print $4}'`
    CUR_CONNAME=`nmcli -g NAME,DEVICE connection| grep "$ETHDEV" | awk -F: '{print $1}'`
    write_log "Current connection name is '$CUR_CONNAME'"

    # Rename current Connection to $NEW_CONNAME
    write_log " " 
    
    if [ "${CUR_CONNAME}" == "--" ]                                        # If no connection name yet
        then write_log "Create a connection on device $ETHDEV name $NEW_CONNAME"
             CMD="nmcli con add type ethernet con-name $NEW_CONNAME ifname $ETHDEV"  
             write_log "Running : $CMD ... "
             nmcli con add type ethernet con-name $NEW_CONNAME ifname $ETHDEV
             if [ $? -ne 0 ] ; then write_err "[ ERROR ] Running $CMD" ; fi
        else write_log "Renaming connection from '$CUR_CONNAME' to '$NEW_CONNAME'"
             CMD="nmcli c modify '$CUR_CONNAME' connection.id '$NEW_CONNAME'"  
             write_log "Running : $CMD ... "
             nmcli c modify "$CUR_CONNAME" connection.id "$NEW_CONNAME" >> $LOG_FILE 2>&1
             if [ $? -ne 0 ] ; then write_err "[ ERROR ] Running $CMD" ; fi
    fi 

    # Now change the IP address.
    CMD="nmcli con mod "$NEW_CONNAME" ipv4.address ${IPADDR}/${NETCIDR}"  
    write_log "Running : $CMD ... "
    nmcli con mod "$NEW_CONNAME" ipv4.address ${IPADDR}/${NETCIDR} >> $LOG_FILE 2>&1
    if [ $? -ne 0 ] ; then write_err "[ ERROR ] Running $CMD" ; fi

     # Now change the gateway.
    CMD="nmcli con mod ${NEW_CONNAME} ipv4.gateway ${GATEWAY}" 
    write_log "Running : $CMD ... "
    nmcli con mod "$NEW_CONNAME" ipv4.gateway $GATEWAY >> $LOG_FILE 2>&1
    if [ $? -ne 0 ] ; then write_err "[ ERROR ] Running $CMD" ; fi

    # We need to change the IPv4 configuration mode on the interface to 'Manual'.
    CMD="nmcli con mod ${NEW_CONNAME} ipv4.method manual"
    write_log "Running : $CMD ... "
    nmcli con mod "$NEW_CONNAME" ipv4.method manual >> $LOG_FILE 2>&1
    if [ $? -ne 0 ] ; then write_err "[ ERROR ] Running $CMD" ; fi

    # You get the same result by specifying PEERDNS=no in the network configuration files.
    CMD="nmcli con mod ${NEW_CONNAME} ipv4.ignore-auto-dns yes" 
    write_log "Running : $CMD ... "
    nmcli con mod "$NEW_CONNAME" ipv4.ignore-auto-dns yes >> $LOG_FILE 2>&1
    if [ $? -ne 0 ] ; then write_err "[ ERROR ] Running $CMD" ; fi

    # Update the DNS1 server
    DNS1=`echo $NAME_SERVERS | awk '{print $1}'`
    CMD="nmcli con mod ${NEW_CONNAME} +ipv4.dns $DNS1"
    write_log "Running : $CMD ... "
    nmcli con mod "$NEW_CONNAME" +ipv4.dns $DNS1>> $LOG_FILE 2>&1
    if [ $? -ne 0 ] ; then write_err "[ ERROR ] Running $CMD" ; fi

    # Update the DNS2 server
    DNS2=`echo $NAME_SERVERS | awk '{print $2}'`
    CMD="nmcli con mod ${NEW_CONNAME} +ipv4.dns $DNS2"
    write_log "Running : $CMD ... "
    nmcli con mod "$NEW_CONNAME" +ipv4.dns $DNS2 >> $LOG_FILE 2>&1
    if [ $? -ne 0 ] ; then write_err "[ ERROR ] Running $CMD" ; fi

    # Update the DNS3 server
    DNS3=`echo $NAME_SERVERS | awk '{print $3}'`
    if [ "$DNS3" != "" ]
        then CMD="nmcli con mod ${NEW_CONNAME} +ipv4.dns $DNS3"
             write_log "Running : $CMD ... "
             nmcli con mod "$NEW_CONNAME" +ipv4.dns $DNS3 >> $LOG_FILE 2>&1
             if [ $? -ne 0 ] ; then write_err "[ ERROR ] Running $CMD" ; fi
    fi

    # Main domain to search
    CMD="nmcli con mod ${NEW_CONNAME} +ipv4.dns-search ${DOMAIN}"
    write_log "Running : $CMD ... "
    nmcli con mod "$NEW_CONNAME" +ipv4.dns-search ${DOMAIN} >> $LOG_FILE 2>&1
    if [ $? -ne 0 ] ; then write_err "[ ERROR ] Running $CMD" ; fi

    # Enable the interface to start automatically on the system boot.
    CMD="nmcli con mod ${NEW_CONNAME} autoconnect yes"
    write_log "Running : $CMD ... "
    nmcli con mod "$NEW_CONNAME" autoconnect yes >> $LOG_FILE 2>&1
    if [ $? -ne 0 ] ; then write_err "[ ERROR ] Running $CMD" ; fi

    # To reload all profiles:
    CMD="nmcli con reload"
    write_log "Running : $CMD ... "
    nmcli con reload >> $LOG_FILE 2>&1
    if [ $? -ne 0 ] ; then write_err "[ ERROR ] Running $CMD" ; fi

    # Show configurtion
    write_log " " 
    write_log "nmcli device show ${ETHDEV} (New config will become effective upon reboot)."
    nmcli device show ${ETHDEV} | while read wline ; do write_log "$wline"; done 
    write_log " " 
    write_log "nmcli connection show" 
    nmcli connection show | while read wline ; do write_log "$wline"; done  # Write Output to Log

}




# ==================================================================================================
# N24 Setup Ubuntu, Mint Network
# ==================================================================================================
function n24_ubuntu_network_setup()
{
    subsection_title "${FUNCNAME[0]} - $OS_NAME - v${OS_VERSION} ${SYS_ARCH}" 

    # Rename all yaml file in /etc/netplan
    write_log " "
    if [ -d /etc/netplan ] 
       then cd /etc/netplan 
            for wfile in `find . -type f -name "*.yaml" -exec basename {} \;`
                do 
                write_log "rename $wfile to ${wfile}.$(date +%Y%m%d%H%M%S)" 
                mv $wfile ${wfile}.$(date +%Y%m%d%H%M%S)
                done
            cd -
    fi 

    # Create our own network yaml file
    F="/etc/netplan/11-sadm-config.yaml"                                # Name our yaml config file
    write_log " " 
    write_log "Creating $F ..."  
    echo "# Created by ${PN} v${PIVER} on `date`"  >$F
    echo "#" >> $F 
    echo "network:"     >>$F
    echo "  version: 2" >>$F
    echo "  renderer: NetworkManager" >>$F
    echo "  ethernets:" >>$F 
    echo "    ${ETHDEV}:" >>$F
    echo "      dhcp4 : No" >>$F
    echo "      addresses : [${IPADDR}/${NETCIDR}]" >>$F
    if [ "$OS_VERSION" -gt 20 ] 
        then echo "      routes:" >>$F
             echo "       - to:  default" >>$F
             echo "         via: ${GATEWAY}" >>$F
        else echo "      gateway4 : ${GATEWAY}" >>$F
    fi 
    echo "      nameservers :" >>$F
    echo "        search : [ ${DOMAIN} ]" >>$F
    DNS1=`echo $NAME_SERVERS | awk '{print $1}'`
    DNS2=`echo $NAME_SERVERS | awk '{print $2}'` 
    DNS3=`echo $NAME_SERVERS | awk '{print $3}'` 
    if [ "$DNS3" != "" ] 
       then echo "        addresses: [${DNS1}, ${DNS2}, ${DNS3}]" >>$F
       else echo "        addresses: [${DNS1}, ${DNS2} ]" >>$F
    fi
    write_log "Modified $F content:"
    write_log " " ; write_log "**********" 
    cat $F | while read wline ; do write_log "$wline"; done
    write_log " " ; write_log "**********" 

    write_log " "
    netplan try 
    if [ $? -ne 0 ]                                            # If not there
        then write_log "'netplan try' [ OK ]"
        else write_err "'netplan try' [ ERROR ]"
    fi      
}




# ==================================================================================================
# N26 Setup Raspbian Network
# ==================================================================================================
function n26_raspbian_network_setup()
{
    subsection_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION}" 

    netfile="/etc/dhcpcd.conf"                                          # File that we modify
    if [ -f "$netfile" ]                                                # If dhcpcd.conf file Exist
       then save_original "$netfile"                                    # Save original dhcpcd file
            write_log " "
    fi

    # Do we have a Wireless Interface
    if [ -d "/sys/class/net" ]                                          # Net dir exist in sys/class
       then ls -1 /sys/class/net |grep "wlan0" >/dev/null 2>&1          # wlan0 device exist 
            if [ $? -eq 0 ]                                             # If wlan0 present on system
               then echo "#"                                        >> $netfile               
                    echo "# Wireless network interface"             >> $netfile 
                    echo "interface wlan0"                          >> $netfile 
                    echo "static ip_address=${WIPADDR}/${NETCIDR}"  >> $netfile 
                    echo "static routers=$GATEWAY"                  >> $netfile 
                    echo "static domain_name_servers=$NAME_SERVERS" >> $netfile 
                    echo "static domain_search=$DOMAIN"             >> $netfile 
                    write_log " "
                    write_log "Lines added to $netfile for wlan0:"
                    tail -7 $netfile | while read wline ;do write_log "$wline" ;done
            fi
    fi

    # Raspbian Wired Network Interface
    if [ -d "/sys/class/net" ] 
       then ls -1 /sys/class/net |grep "$ETHDEV" >/dev/null 2>&1 # $ETHDEV exist ?
            if [ $? -eq 0 ] 
               then echo "#"                                        >> $netfile                       
                    echo "# Wired $ETHDEV Network Interface"        >> $netfile
                    echo "interface $ETHDEV"                        >> $netfile 
                    echo "static ip_address=${IPADDR}/${NETCIDR}"   >> $netfile 
                    echo "static routers=$GATEWAY"                  >> $netfile 
                    echo "static domain_name_servers=$NAME_SERVERS" >> $netfile 
                    echo "static domain_search=$DOMAIN"             >> $netfile 
                    write_log " "
                    write_log "Lines added to $netfile for ${ETHDEV}:"
                    tail -7 $netfile | while read wline ; do write_log "$wline" ; done
            fi
    fi
    
    chmod 0644 $netfile ; chown root:root $netfile
    
    write_log " " 
    write_log "Updated content of $netfile :"                           # User Header for file id.
    write_log "**********"
    cat $netfile | while read wline ; do write_log "$wline" ; done      # Show Original file content
    write_log "**********"
    return
}



# ==================================================================================================
# N28 Setup Debian Network
# ==================================================================================================
function n28_debian_network_setup()
{
    subsection_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION}" 

    # Save original file and show content before changing it.
    netfile="/etc/network/interfaces"                                   # File that we modify
    if [ -f "$netfile" ]                                                # If interfaces file Exist
       then save_original "$netfile"                                    # Save original network file
            write_log " " 
            write_log "**********"
            write_log "Original content of $netfile :"
            grep -Ev "^$|^#"  $netfile | nl | while read wline ; do write_log "$wline"; done
            write_log "**********"
    fi

    # Create new version of the interfaces file.
    echo " "                                    >> $netfile             # Insert Blank Line
    echo "# The primary network interface"      >> $netfile             # Interface Header
    echo "auto            $ETHDEV"              >> $netfile             # Auto configure 
    echo "iface           $ETHDEV inet static"  >> $netfile             # Interface Statis Cfg
    echo "address         $IPADDR"              >> $netfile             # Interface IP Address
    echo "gateway         $GATEWAY"             >> $netfile             # Interface Gateway
    echo "netmask         $NETMASK"             >> $netfile             # Interface Netmask
    echo "network         $NETWORK"             >> $netfile             # Interface Network
    echo "broadcast       $BROADCAST"           >> $netfile             # Broadcast IP
    echo "dns-nameservers $NAME_SERVERS"        >> $netfile             # DNS Servers of Interface
    echo "dns-search      $DOMAIN"              >> $netfile             # Domain search name
    echo " "                                    >> $netfile             # Blank Line
    
    write_log " " 
    write_log "**********"
    write_log "Final content of $netfile :"
    grep -Ev "^$|^#"  $netfile | nl | while read wline ; do write_log "$wline"; done
    write_log "**********"

    chmod 0644 $netfile ; chown root:root $netfile                      # Change Protection & Owner
    return
}


# ==================================================================================================
# Setup DNS and Domain for name resolution, depending of the O/S installed.
# ==================================================================================================
function n30_setup_resolv_conf()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION}" 

    n32_setup_systemd_resolved                                          # Set & Activate resolved
    n34_setup_resolv_conf                                               # Update /etc/resolv.conf
}



# ==================================================================================================
# Setup systemd resolved service - Configure the file /etc/systemd/resolved.conf
# ==================================================================================================
function n32_setup_systemd_resolved()
{
    subsection_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION}" 

    # Check if resolved config file don't exist, return to caller
    wfile="/etc/systemd/resolved.conf"                                  # systemd-resolved cfg file
    if [ ! -f "$wfile" ] 
        then write_log "File $wfile doesn't exist."
             write_log "No update of systemd-resolved possible."
             return 0
    fi 

    save_original "$wfile"                                              # Backup Original
    write_log " "

    # Make sure "DNS=" is set properly, if not already set.
    grep "^DNS=" $wfile >/dev/null 2>&1                                 # DNS= Line already there ?
    if [ $? -ne 0 ]                                                     # If not there
       then echo -e "\n# Line inserted by ${PN} v${PIVER} on `date`" >>$wfile 
            echo -e "DNS=${NAME_SERVERS}" >> $wfile                     # Add DNS= LIne
    fi 
    
    # Make sure "Domains=" is set properly, if not already set.
    grep "^Domains=" $wfile >/dev/null 2>&1                             # Domains= Line there ?
    if [ $? -ne 0 ]                                                     # If not there
       then echo -e "Domains=${DOMAIN}" >> $wfile                       # Add Domains= Line
    fi 
    
    write_log " " 
    write_log "Content of $wfile after modification:"                   # Write new Updated file
    write_log "**********" 
    grep -Ev "^$|^#"  $wfile | while read wline ; do write_log "$wline" ; done 
    write_log "**********" 
    
    # Prevent Network Manager from updating the /etc/resolv.conf 
    write_log "Prevent NetworkManager from updating /etc/resolv.conf"
    wd="/etc/NetworkManager/conf.d" 
    if [ -d "$wd" ]                                                     # Id Dir. Exist
        then wfile="${wd}/90-dns-none.conf"                 
             if [ ! -f "$wfile" ]
                then echo "[main]" > $wfile
                     echo "dns=none" >> $wfile 
                     write_log " " 
                     write_log "Content of $wfile after modification:"
                     write_log "**********" 
                     grep -Ev "^$|^#"  $wfile | while read wline ; do write_log "$wline" ; done 
                     write_log "**********" 
             fi
    fi


    #service_enable systemd-resolved                                     # Enable systemd-resolved
    #systemctl is-enabled systemd-resolved  > /dev/null 2>&1             # Is Service enabled ? 
    #if [ $? -ne 0 ] ; then service_enable systemd-resolved ; fi         # No, enable the service
    #service_restart systemd-resolved                                    # Restart systemd-resolved
}


# ==================================================================================================
# Configure the file /etc/resolv.conf
# ==================================================================================================
function n34_setup_resolv_conf()
{
    subsection_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION}" 

    wfile="/etc/resolv.conf"                                            # File to update
    write_log "Content of $wfile before modification:"                   # Write new Updated file
    write_log "**********" 
    grep -Ev "^$|^#"  $wfile | while read wline ; do write_log "$wline" ; done 
    write_log "**********"     
    save_original "$wfile"                                              # Backup Original

    # remove link to /run/systemd/resolve/stub-resolv.conf
    write_log "Removing $wfile file"                                    # show that file is a link
    rm -f "$wfile" >/dev/null 2>&1                                      # Remove Link
    
    # Create New /etc/resolv.conf
    echo "domain $DOMAIN"        > $wfile                               # Set Host Domain
    echo "search $DOMAIN"       >> $wfile                               # Set Host Search Domain
    for ns in $NAME_SERVERS                                             # For DNS Server specified
        do
        echo "nameserver $ns "   >> $wfile                              # Insert DNS Server IP
        done

    # Make file immutable (Cannot be change)
    write_log "Make $wfile immutable - chattr +i $wfile"
    chattr +i "$wfile" >>$LOG_FILE 2>&1
    
    # Show new content
    write_log " " 
    write_log "Content of $wfile after modification:"                   # Write new Updated file
    write_log "**********" 
    grep -Ev "^$|^#"  $wfile | while read wline ; do write_log "$wline" ; done 
    write_log "**********"         
}



# ================================================================================================
# Setup Swap Space - Size of Swap Space is determine by the system memory size.
# The minimum swap space size is 4096.
# When memory size is greater than 4096 than swap space is equal to memory size.
# ================================================================================================
function n40_setup_swap_space()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION}" 

    write_log "Current situation of swap space" 
    write_log "free --mega" ; free --mega |while read wline ; do write_log "$wline" ; done  
    write_log " "

    # Get Memory Size in MB (mem_size) and the actual Swap Space size (swap_size)
    mem_size=` free --mega | grep '^Mem:'  | awk '{print$2}'`           # Memory Size 
    if [ $mem_size -gt 1024 ] 
        then mem_size=`echo "$mem_size"| awk '{print sprintf("%.0f",$0/1024)*1024}'`  # nearest 1024
    fi
    swap_size=`free --mega | grep '^Swap:' | awk '{print$2}'`           # Actual Swap Size 
    if [ $swap_size -gt 1024 ] 
        then swap_size=`echo "$swap_size"|awk '{print sprintf("%.0f",$0/1024)*1024}'` # nearest 1024
    fi 
    write_log "Actual Memory Size is ${mem_size}MB and Swap Space at ${swap_size}MB"

    # Calculate the new swap space size.
    new_size="$MINSWAP"                                                 # Minimum Swap Size is 4GB
    if [ $mem_size -gt 4096 ] ; then new_size=$mem_size ;fi             # If Mem>4GB, Swap=Mem Size
    if [ $mem_size -lt 1024 ] ; then new_size=$new_size ;fi             # If Mem<1GB, Swap=MinSwap
    write_log "New Swap Size set to be ${new_size}MB"                   # Show new Swap Size

    # No more than 4GB can be allocated for the swap space.
    if [ $new_size -gt $MAXSWAP ]                                       # Actual Swap > MaxSwap Size 
        then write_log "Swap size cannot exceed ${MAXSWAP}MB (MAXSWAP)." 
             new_size="${MAXSWAP}" 
             write_log "New swap space size is set to ${new_size}"
    fi 

    # Determine the amount in MB that need to be added to swap space.
    inc_size=$(( $new_size - $swap_size ))                              # New SwapSize - ActualSize
    if [ $inc_size -lt 1 ]                                              # Incr. is lower than 1 
       then write_log "Swap space doesn't need to be increase [ OK ]"
            return 0 
    fi 

    # If swap size already equal or greater than calculated value.
    if [ $new_size -lt $swap_size ]                                     # Size is already OK
        then write_log "Swap size is already ok at $swap_size ...\n"    # Advise user 
             write_log "No change is needed."
             return 0                                                   # Return to caller 
    fi 
    
    # Swap space increase space versus amount of free space in /var
    varfree=`df -m /var | grep '/var' | awk '{ print $4 }'`             # MB Free in /var
    if [ "$varfree" = "" ]                                              # When no /var filesystem
       then varfree=`df -m / | grep '/' |  awk '{ print $4 }'`          # Used free space in / then
    fi 
    write_log "Free space left ${varfree}MB on disk."                   # Should Free MB in / or/var
    write_log "The swap space need to be increased by ${inc_size}MB."
    if [ "$inc_size" -gt "$varfree" ] 
       then write_err "Can't increase swap space, only ${varfree}MB left in /var & need ${inc_size}MB"
            write_err "Actual Memory Size is ${mem_size}MB and swap space left at ${swap_size}MB"
            return 1
    fi 

    # Adjust swap space depending of O/S 
    case "$OS_NAME" in
        "RASPBIAN")
            n42_raspbian_swap_space_update "$swap_size" "$new_size" "$inc_size"
            ;;

        "REDHAT"|"CENTOS"|"ALMA"|"ROCKY")
            n44_add_swapfile "$swap_size" "$new_size" "$inc_size"
            ;;
        
        "FEDORA")
            #n44_add_swapfile "$swap_size" "$new_size" "$inc_size"
            write_log "Starting with Fedora 33, Fedora use zRam (Dynamic Swap)."
            write_log "No change were made to the swap space." 
            write_log "  - To increase the size of your ZRAM device, create the file "
            write_log "    /etc/systemd/zram-generator.conf and include the below configuration "
            write_log "    example. (You can also leave an empty file in this path to disable ZRAM.)"
            write_log "    [zram0]" 
            write_log "    zram-fraction=0.5"
            write_log "    max-zram-size=4096"  
            write_log "  - The limits given above matches the default limits (50 % of available " 
            write_log "    memory or 4 GB in MB.)"
            write_log "  - Run 'systemctl restart swap-create@zram0.service' to recreate "
            write_log "    the ZRAM device without rebooting."
            write_log "  - Use 'free' and 'zramctl' to view swap space."
            write_log "  - Use 'free' and 'zramctl' to view swap space."
            write_log " "
            write_log "free --mega" ; free --mega |while read wline ; do write_log "$wline" ; done  
            write_log "zramctl "    ; zramctl     |while read wline ; do write_log "$wline" ; done  
            write_log " "
            ;;

        "UBUNTU"|"DEBIAN"|"MINT")
            n44_add_swapfile "$swap_size" "$new_size" "$inc_size"
            ;;

        "*" )
            write_log "OS $OS_NAME not supported in ${FUNCNAME[0]} at line number ${BASH_LINENO[0]}"
            write_log "No change were made to the swap space." 
            ;;
    esac

    write_log " "
    write_log "After adjusting the swap space, the actual situation is : " 
    write_log "free --mega" ; free --mega |while read wline ; do write_log "$wline" ; done  

}



# ================================================================================================
# Update Swap Space Size by recreating it .
# ================================================================================================
function n42_raspbian_swap_space_update()
{
    actual_size=$1
    new_size=$2
    inc_size=$3
    subsection_title "${FUNCNAME[0]}"
    write_log "Actual Size: $1 - New_Size: $new_size - Incr Size: $inc_size"
    write_log " "
    swap_cfg="/etc/dphys-swapfile"                                      # Swap Space Config file.
    save_original "$swap_cfg"                                           # Save Original swap config.
    
    swap_tmp="/etc/dphys-swapfile.tmp"                                  # New swap config tmp file
    if [ -f $swap_tmp ] ; then rm -f $swap_tmp ; fi                     # Remove tmp if exist

    if [ -f $swap_cfg ]                                                 # Swap File Config exist
       then write_log " "
            write_log "Adjusting Swap Space from ${actual_size}MB to ${new_size}MB in $swap_cfg"
            while read wline                                            # Read swap file config 
                do 
                echo "$wline" | grep 'CONF_SWAPSIZE=' >/dev/null 2>&1
                if [ $? -eq 0 ]                                         # If swap size line
                   then echo "CONF_SWAPSIZE=$new_size" >> $swap_tmp     # Change Size
                        continue                                        # Continue with next line
                fi 
                echo "$wline" | grep 'CONF_MAXSWAP=' >/dev/null 2>&1
                if [ $? -eq 0 ]                                         # If swap size limit
                   then echo "CONF_MAXSWAP=$new_size" >> $swap_tmp      # Change swap limit Size
                        continue                                        # Continue with next line
                fi 
                echo "$wline" >> $swap_tmp                              # Write unmodified line 
                done < $swap_cfg                                        # end of while loop
            mv $swap_tmp $swap_cfg                                      # Replace old cfg by new one

            write_log " " 
            write_log "Content of $swap_cfg after modification:"        # Write new Updated file
            write_log "**********" 
            grep -Ev "^#|^[[:space:]]*$" $swap_cfg | while read wline ;do write_log "$wline" ;done
            write_log "**********" 

            write_log " "
            write_log "Turn Swap OFF - dphys-swapfile swapoff" 
            dphys-swapfile swapoff >>$LOG_FILE 2>&1                     # De-Activate swap space
            if [ $? -eq 0 ] 
                then write_log "Return Code = 0" 
                else write_err "[ ERROR ] Could not De-Activate Swap file"
            fi 

            write_log " "
            write_log "Recreate swap file - dphys-swapfile setup" 
            dphys-swapfile setup | while read wline ; do write_log "$wline" ;done
            if [ $? -eq 0 ] 
                then write_log "Return Code = 0" 
                else write_err "[ ERROR ] Could not create new Swap file."
            fi 

            write_log " "
            write_log "Turn Swap ON - dphys-swapfile swapon" 
            dphys-swapfile swapon >>$LOG_FILE 2>&1                      # Activate new swap space
            if [ $? -eq 0 ] 
                then write_log "Return Code = 0" 
                else write_err "[ ERROR ] Could not Activate Swap file."
            fi 
       else write_log "The swap configuration file ($swap_cfg) don't exist."
            write_log "No change were made to the swap space." 
    fi 
    return 0 
}




# ================================================================================================
# Update Swap Space Size by adding another swap space.
# ================================================================================================
function n44_add_swapfile()
{
    actual_size=$1
    new_size=$2
    inc_size=$3
    subsection_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION}" 

    # Allocate space for the new swap file (Fill it with zeroes).
    write_log "Creating a swap file container of ${inc_size}MB."
    count=$(( $inc_size * 1024 ))                                       # Calc Nb of 1024b Chunk 
    swapfile="/var/swap01"                                              # New swap file name
    if [ -f "$swapfile" ]                                               # If Swap Already exist 
       then write_log "The Swap File $swapfile already exist." 
            write_log "We will not created an additionnal one."
            return 0
    fi 
    write_log "dd if=/dev/zero of==$swapfile bs=1024 count=$count"      # Show command executed
    dd if=/dev/zero of=$swapfile bs=1024 count=$count                   # Create empty swap file
    #write_log "fallocate -l ${inc_size}MB $swapfile "                   # Show command executed
    #fallocate -l ${inc_size}MB $swapfile                                # Allocate space for Swap
    if [ $? -eq 0 ]                                                     # Check if everything is ok
       then write_log "Swap file container created successfully."
       else write_err "Error creating container file for swap file ($swapfile)."
    fi 
    write_log "chmod 600 $swapfile"
    chmod 600 $swapfile                                                 # Permission for new swap

    # Formating the swap file
    write_log " " 
    write_log "Formatting the swap file, (mkswap $swapfile)"            # Advise user what we do
    mkswap $swapfile                                                    # Format the Swap File
    if [ $? -eq 0 ]                                                     # Check if everything is ok
       then write_log "Swap file formatted successfully."
       else write_err "Error formatting Swap file."
    fi 

    # Update the /etc/fstab - so it is activated on reboot
    write_log " " 
    write_log "Update the /etc/fstab"                                   # Advise user what we do
    write_log "echo \"$swapfile swap swap defaults 0 0\" >> /etc/fstab" 
    echo "$swapfile swap swap defaults 0 0" >> /etc/fstab

    # Activate Swap file
    write_log " " 
    write_log "Activating the swap file, (swapon $swapfile)"            # Advise user what we do
    swapon $swapfile >> $LOG_FILE 2>&1
    if [ $? -eq 0 ]                                                     # Check if everything is ok
       then write_log "Swap file now activated."
       else write_err "Error while activating the Swap file."
    fi 

    # Show Swap Space
    write_log " " 
    write_log "Show Additionnal Swap Space"                             # Advise user what we do
    ls -lh $swapfile | while read wline ;do write_log "$wline" ;done  

    return 0 
}



# ================================================================================================
# Update the /etc/host.conf (Give priority to host file over DNS for name resolution)
# ================================================================================================
function n50_update_etc_host_conf()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION}" 

    MFILE="/etc/host.conf"
    save_original "$MFILE"                                              # Backup Original

    write_log " " 
    write_log "Original content of $MFILE :"
    write_log "**********"
    cat  $MFILE | while read wline ; do write_log "$wline"; done
    write_log "**********"

    echo "multi on" > $MFILE                                            # Create file
    echo "order hosts,bind" >> $MFILE                                   # Append to file

    write_log " " 
    write_log "Modified content of $MFILE :"
    write_log "**********"
    cat  $MFILE | while read wline ; do write_log "$wline"; done
    write_log "**********"
}




# ==================================================================================================
# Perform the initial O/S system Update
# ==================================================================================================
function s02_perform_os_update()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION}" 

    # For O/S Operating system using 'rpm' packaging.
    if [ "$PACKTYPE" = "rpm" ] 
       then case "$OS_NAME" in
                "REDHAT"|"CENTOS"|"ALMA"|"ROCKY")
                        if [ "$OS_VERSION" -lt 8 ]
                           then write_log "yum -y update"
                                #yum -y update >>$LOG_FILE 2>&1
                                yum -y update >/dev/null 2>&1
                                EC=$? ; write_log "Return Code of Update is $EC"
                                return $EC
                           else write_log "dnf -y update"
                                #dnf -y update >>$LOG_FILE 2>&1
                                dnf -y update >/dev/null 1>&1
                                EC=$? ; write_log "Return Code of Update is $EC"
                                return $EC
                        fi 
                        ;;

                "FEDORA")
                        write_log "dnf -y update"
                        #dnf -y update >>$LOG_FILE 2>&1
                        dnf -y update >/dev/null 2>&1
                        EC=$? ; write_log "Return Code of Update is $EC"
                        return $EC
                        ;;

                * )     write_log "$OS_NAME is not yet supprted" 
                        write_log "O/S Update couldn't be done."
                        return 1
                        ;;                        
            esac
    fi 

    # For O/S Operating system using 'deb' packaging.
    if [ "$PACKTYPE" = "deb" ] 
       then write_log "Running : apt-get -y update"
            #apt-get -y update >>$LOG_FILE 2>&1
            apt-get -y update >/dev/null 2>&1
            EC=$? ; write_log "Return Code is $EC" 
            write_log " "
            write_log "Running : apt-get -y dist-upgrade"
            apt-get -y dist-upgrade >/dev/null 2>&1
            #apt-get -y dist-upgrade >>$LOG_FILE 2>&1
            EC=$? ; write_log "Return Code is $EC"
            write_log " "
            return $EC
    fi 
}


#===================================================================================================
#  Install EPEL 7 Repository for Redhat / CentOS / Rocky / Alma Linux / 
#===================================================================================================
add_epel_7_repo()
{
    if [ ! -r /etc/yum.repos.d/epel.repo ]  
       then write_log "Adding V7 EPEL repository ..." 
            EPEL="https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm"
            yum install -y $EPEL >>$SLOG 2>&1
            if [ $? -ne 0 ]
                then write_err "[ ERROR ] Adding EPEL 7 repository."
                     return 1
            fi
            write_log "Subscribe to optional extra rhel-ha"
            subscription-manager repos --enable rhel-*-optional-rpms \
                           --enable rhel-*-extras-rpms \
                           --enable rhel-ha-for-rhel-*-server-rpms

            printf "Disabling EPEL Repository (yum-config-manager --disable epel) " |tee -a $SLOG
            yum-config-manager --disable epel >/dev/null 2>&1
            if [ $? -ne 0 ]
               then write_err "Couldn't disable EPEL 7 for version $OS_VERSION" | tee -a $SLOG
                    return 1
               else write_log "[ OK ]"
            fi 
            return 0
    fi
}


#===================================================================================================
# Add EPEL Repository on Redhat / CentOS 8 (but do not enable it)
#===================================================================================================
add_epel_8_repo()
{

    if [ "$OS_NAME" = "REDHAT" ] 
       then echo "On RHEL 8, it's required to also enable codeready-builder ..." 
            echo "Since EPEL packages may depend on packages from it." 
            subscription-manager repos --enable "codeready-builder-for-rhel-8-$(arch)-rpms"
            if [ $? -ne 0 ]
               then echo "[ WARNING ] Couldn't enable EPEL codeready-builder repository" |tee -a $SLOG
               else echo " [ OK ]"
            fi 
            wurl="https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm"
            dnf -y install $wurl 
            if [ $? -ne 0 ]
               then echo "[ WARNING ] Couldn't enable EPEL 8 repository" |tee -a $SLOG
                    return 1
               else echo " [ OK ]"
            fi 
    fi

    if [ "$OS_NAME" = "CENTOS" ] || [ "$OS_NAME" = "ALMA" ] || [ "$OS_NAME" = "ROCKY" ]
       then printf "On CentOS 8, it's recommended to also enable the EPEL PowerTools Repository.\n"  
            printf "dnf config-manager --set-enabled powertools" 
            dnf config-manager --set-enabled powertools
            if [ $? -ne 0 ]
               then write_err "[ WARNING ] Couldn't enable EPEL PowerTools repository."
               else echo " [ OK ]"
            fi 
            echo "dnf -y dnf install epel-release epel-next-releae"
            dnf -y dnf install epel-release epel-next-release >>$SLOG 2>&1 
            if [ $? -ne 0 ]
               then write_err "[ WARNING ] Couldn't enable EPEL 8 repository" 
                    return 1
               else echo "Enable EPEL 8 repository [ OK ]"
            fi 
    fi

    write_log "Disabling EPEL Repository (yum-config-manager --disable epel) "
    dnf config-manager --disable epel >/dev/null 2>&1
    if [ $? -ne 0 ]
       then write_err "Couldn't disable EPEL for version $OS_VERSION"
            return 1
       else echo "[ OK ]"
    fi 
}





#===================================================================================================
# Add EPEL Repository on Redhat / CentOS 9 (but do not enable it)
#===================================================================================================
add_epel_9_repo()
{
    if [ "$OS_NAME" = "ROCKY" ] ||  [ "$OS_NAME" = "ALMA" ] || [ "$OS_NAME" = "CENTOS" ] 
        then 
             # Install 'crb' (Code Ready Builder) 
             dnf repolist enabled | grep -q "^crb "                     # Check crb already enable
             if [ $? -ne 0 ]                                            # If not enable
                then write_log "Enable 'crb' EPEL repository on $OS_NAME ..."
                     dnf config-manager --set-enabled crb 
                     if [ $? -ne 0 ]
                        then write_err "[ ERROR ] Couldn't enable 'crb' repository."
                             return 1 
                        else write_log "[ OK ] Repository 'crb' is now enable."
                     fi
                else write_log "Repository 'crb' already enable."
             fi 
             
             # Install epel repository 
             dnf repolist enabled | grep -q "^epel "                    # Check epel already enable
             if [ $? -ne 0 ]  
                 then write_log "Install epel-release on $OS_NAME V9 ..." 
                      dnf -y install epel-release
                      if [ $? -ne 0 ]
                         then write_err "[ ERROR ] Adding epel-release V9 repository."
                              return 1 
                         else write_log "[ OK ] Repository 'epel-release' is now enable."
                              dnf config-manager --enable epel
                      fi
                 else write_log "Repository 'epel' already enable."
             fi
    fi

    # Install epel-next repository only on CentOS 
    if [ "$OS_NAME" = "CENTOS" ] 
        then dnf repolist enabled | grep -q "^epel-next "               # epel-next already enable?
             if [ $? -ne 0 ]  
                then write_log "Install epel-next-release on $OS_NAME V9 ..." 
                     dnf -y install epel-next-release
                     if [ $? -ne 0 ]
                        then write_err "[ ERROR ] Adding 'epel-next' V9 repository."
                             return 1 
                        else write_log "[ OK ] Repository 'epel-next' is now enable."
                     fi
                     dnf config-manager --enable epel-next              # Make sure it's enable
                else write_log "Repository 'epel-next' already enable."
             fi
             return 0 
    fi 

    if [ "$OS_NAME" = "REDHAT" ] 
        then dnf repolist enabled | grep -q "^codeready-builder-for-rhel-9" # Repo already configure
             if [ $? -ne 0 ]  
                then subscription-manager repos --enable codeready-builder-for-rhel-9-$(arch)-rpms >>$SLOG 2>&1 
                     if [ $? -ne 0 ]
                        then write_err "[ ERROR ] Subscribing to 'codeready-builder-for-rhel-9-$(arch)-rpms'."
                             return 1 
                        else write_log "[ OK ] Repository 'codeready-builder-for-rhel-9-$(arch)-rpms' is now enable."
                     fi
                else write_log "[ OK ] Repository 'codeready-builder-for-rhel-9-$(arch)-rpms' is already installed."
                     return 0
             fi 

             dnf repolist enabled | grep -q "^rhel-9-for-x86_64-baseos" # Repo already configure
             if [ $? -ne 0 ]  
                then write_log "Installing 'epel-release-latest-9.noarch.rpm' CentOS/Redhat v9 ..." 
                     dnf -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm >>$SLOG 2>&1
                     if [ $? -ne 0 ]
                        then write_err "[ ERROR ] Adding 'epel-release-latest-9.noarch.rpm' v9 repository."
                             return 1 
                        else write_log"[ OK ] Repository 'rhel-9-for-x86_64-baseos' installed." 
                     fi
                else write_log "Repository 'rhel-9-for-x86_64-baseos' already enable."
             fi
    fi 
}



#===================================================================================================
#  Install EPEL Repository for Redhat / CentOS / Rocky / Alma Linux / 
#===================================================================================================
s08_add_epel_repository()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION}" 

    # If not Red Hat or CentOS, just return to caller 
    if [ "$OS_NAME" !=  "REDHAT" ] && [ "$OS_NAME" !=  "CENTOS" ] && \
       [ "$OS_NAME" != "ALMA"    ] && [ "$OS_NAME" != "ROCKY"   ]
        then write_log "No EPEL repository for $OS_NAME" 
             return 0 
    fi

    if [ "$OS_VERSION" -eq 7 ] 
       then add_epel_7_repo 
            if [ $? -ne 0 ] ; then return 1 ; fi
    fi 

    if [ "$OS_VERSION" -eq 8 ] 
       then add_epel_8_repo 
            if [ $? -ne 0 ] ; then return 1 ; fi
    fi 

    if [ "$OS_VERSION" -eq 9 ] 
       then add_epel_9_repo 
            if [ $? -ne 0 ] ; then return 1 ; fi
    fi 
    return 0 
}





# ==================================================================================================
# Install Additionnal Standard Software From Distribution Repository
# ==================================================================================================
function s12_install_additional_software()
{
    section_title "${FUNCNAME[0]} - ${OS_NAME} - V${OS_VERSION}" 

    # If 'rpm' packaging
    if [ "$PACKTYPE" = "rpm" ] 
       then subsection_title "Install Selected EPEL Repository Packages" 
            EPEL_RPM="figlet xrdp smartmontools terminator nmon iperf3 chrony rsync"
            EPEL_RPM="$EPEL_RPM dmidecode util-linux bc perl-DateTime vim NetworkManager-tui"
            EPEL_RPM="$EPEL_RPM postfix lshw gawk ethtool bind-utils rsyslog tldr parted"
            EPEL_RPM="$EPEL_RPM qbittorrent mutt curl gparted git at tree wget gpg"
            EPEL_RPM="$EPEL_RPM rear genisoimage syslinux inxi ncdu remmina keychain"
            EPEL_RPM="$EPEL_RPM grub2-efi-x64-modules grub2-tools-extra cryptsetup"
            EPEL_RPM="$EPEL_RPM kernel-devel kernel-headers gcc make bzip2 perl dkms flatpak"
            write_log " "
            for WRPM in $EPEL_RPM
                do
                rpm -q $WRPM > /dev/null 2>&1
                if [ $? -ne 0 ]                                         # If Package not installed
                   then dnf -y --enablerepo=epel install $WRPM >> /dev/null 2>&1
                        EC=$?
                        if [ $EC -ne 0 ]                                # Error installing package
                            then write_err "[ ERROR ] Installing package '$WRPM'."
                            else write_log "[ SUCCESS ] Installing package '$WRPM'."
                        fi
                   else write_log "[ OK ] Package '$WRPM' already installed."
                fi                     
                done
            flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
    fi 

    # If 'deb' packaging
    if [ "$PACKTYPE" = "deb" ] 
        then write_log "Refresh Package cache - Running 'apt -y update'."
             apt -y update  > /dev/null 2>&1
             EC=$?
             write_log "Return code : $EC"
             write_log " " 
             UDEB="figlet xrdp smartmontools terminator nmon iperf3 chrony rsync cryptsetup"
             UDEB=" $UDEB krusader dmidecode util-linux bc parted libdatetime-perl libwww-perl "
             UDEB=" $UDEB postfix fdupes gawk mailutils ethtool remmina screenfetch ncdu "
             UDEB=" $UDEB synaptic openssh-server apt-file dnsutils at tree keychain vim tldr "
             UDEB=" $UDEB qbittorrent mutt curl gparted git inxi wget gpg apt-transport-https"
             UDEB=" $UDEB build-essential dkms rsyslog linux-headers-$(uname -r) "
             UDEB=" $UDEB flatpak gnome-software-plugin-flatpak apt-transport-https network-manager"
             UDEB=" $UDEB fonts-roboto fonts-noto fonts-firacode fonts-freefont-ttf fonts-ibm-plex fonts-liberation"
             if [ "$SYS_ARCH" = "amd64" ] || [ "$SYS_ARCH" = "x86_64" ] ;then UDEB=" $UDEB rear" ;fi
             write_log " "
             for WDEB in $UDEB
                do
                #write_log "apt -y install $WDEB" "NOLF"
                DEBIAN_FRONTEND=noninteractive apt -y install $WDEB >/dev/null 2>&1
                EC=$?
                if [ $EC -ne 0 ]
                    then write_err "[ ERROR $EC ] apt -y install $WDEB"
                    else write_log "[ OK ] apt -y install $WDEB"
                fi
                done
             flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
    fi        
    write_log " " 
    write_log "End of section ${FUNCNAME[0]}..."    
}



# ==================================================================================================
# Add Microsoft VSCode Repository
# ==================================================================================================
function s20_add_vscode_repository()
{
    section_title "${FUNCNAME[0]} - $OS_NAME v${OS_VERSION} $PACKTYPE $SYS_ARCH" 

    # Install VsCode repository and install it (CentOS, Fedora, Red Hat)
    if [ "$PACKTYPE" = "rpm" ]
        then write_log "rpm --import https://packages.microsoft.com/keys/microsoft.asc"
             rpm --import https://packages.microsoft.com/keys/microsoft.asc
             W="/etc/yum.repos.d/vscode.repo"
             write_log " " 
             write_log "Creating the repository file $W"
             echo "[code]"                                                   >  $W
             echo "name=Visual StudioCode"                                   >> $W
             echo "baseurl=https://packages.microsoft.com/yumrepos/vscode"   >> $W
             echo "enabled=1 "                                               >> $W
             echo "gpgcheck=1"                                               >> $W
             echo "gpgkey=https://packages.microsoft.com/keys/microsoft.asc" >> $W

             write_log "Final content of $W :"
             write_log "**********" 
             grep -Ev "^$|^#"  $W | while read wline ; do write_log "$wline"; done 
             write_log "**********"     

             write_log " " 
             write_log "dnf -y install code"
             dnf -y install code > /dev/null 2>&1
             RC=$? ; write_log "Return code : $RC" 
    fi

    # Install VsCode PPA and install it (Ubuntu, Debian, ...) but not on ARM CPU not supported.
    if [ "$PACKTYPE" = "deb" ] 
        then MSURL="https://packages.microsoft.com/keys/microsoft.asc" 
              write_log "wget -qO- $MSURL | gpg --dearmor > packages.microsoft.gpg"
              wget -qO- $MSURL | gpg --dearmor > packages.microsoft.gpg
              #
              write_log "install -D -o root -g root -m 644 packages.microsoft.gpg /etc/apt/keyrings"
              install -D -o root -g root -m 644 packages.microsoft.gpg /etc/apt/keyrings/packages.microsoft.gpg

              vslist="/etc/apt/sources.list.d/vscode.list"
              write_log " " 
              write_log "Creating the repository file $vslist"
              echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > $vslist
              rm -f packages.microsoft.gpg

              write_log "Content of $vslist after creation:" 
              write_log "**********" 
              grep -Ev "^$|^#"  $vslist | while read wline ; do write_log "$wline"; done 
              write_log "**********"     

              write_log "apt -y install apt-transport-https"
              apt -y install apt-transport-https >>$LOG_FILE 2>&1
              RC=$? ; write_log "Return code of previous command is $RC" ; write_log " "

              write_log "Refresh cache - running 'apt update'."
              apt update >>$LOG_FILE 2>&1
              RC=$? ; write_log "Return code of previous command is $RC" ; write_log " "
              write_log "apt -y install code"
              #
              apt -y install code >>$LOG_FILE 2>&1
              RC=$? ; write_log "Return code of previous command is $RC" ; write_log " "
    fi 
    return
}



# ================================================================================================
# Make sure that we can have a nologin shell
# ================================================================================================
function s30_create_nologin_shell()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION}"

    # Make sure 'nologin' shell is present on system
    which nologin >/dev/null 2>&1
    if [ $? -ne 0 ]
        then write_err "ERROR : The 'nologin' command is not present on this system."
             write_err "Will not be able to add it to '/etc/shells'."
        else NOLOGIN=`which nologin`
             write_log "The nologin shell is located at $NOLOGIN"
             write_log "Adding $NOLOGIN to /etc/shells"
             echo "$NOLOGIN" >> /etc/shells
    fi

    write_log " " 
    write_log "Content of /etc/shells after adding $NOLOGIN :"
    write_log "**********"
    cat /etc/shells | while read wline ; do write_log "$wline"; done
    write_log "**********"
}



# ================================================================================================
# Create Standard Local System Groups (sadmin)
# ================================================================================================
function s34_create_standard_groups()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION}"
    
    # Create 'sadmin' Group, if it doesn't exist.
    wgrp="sadmin" ; wgid=601                                            # sadmin admin group & id
    getent group "$wgrp" >/dev/null 2>&1                                # Group exist ?
    if [ $? -ne 0 ]                                                     # If Group don't exist 
        then write_log "Creating '$wgrp' group with a gid of $wgid"
             write_log "groupadd -g ${wgid} ${wgrp}"                    # Write action to Screen/Log
             groupadd -g ${wgid} ${wgrp}  >> $LOG_FILE 2>&1             # Add Group 
             if [ $? -ne 0 ] ; then write_err "[ ERROR ] Creating group ${wgrp}" ;fi
        else write_log "[ OK ] Group 'sadmin' already exist."
    fi 
}




# ================================================================================================
# Create Standard Local users (jacques)
# ================================================================================================
function s38_create_standard_users()
{
    section_title "${FUNCNAME[0]} ${OS_NAME} v${OS_VERSION} ${SYS_ARCH}"

    # Create 'sadmin' account and make it part of 'sadmin' group.
    wgrp="sadmin" ; wuid=601 ; wusr="sadmin"                            # Set sadmin Account config
    write_log "Setting the '$wusr' account."
    id "$wusr" >/dev/null 2>&1                                          # User Account Exist ?
    if [ $? -ne 0 ]                                                     # If User don't exist 
       then write_log " "
            write_log "Creating 'sadmin' account."
            accname="sadmin administrator account"                      # Set Account Description
            write_log "useradd -m -c '$accname' -e '' -s '/bin/bash' -g $wgrp -u $wuid $wusr"
            useradd -m -c "$accname" -e '' -s '/bin/bash' -g $wgrp -u $wuid $wusr  > /dev/null 2>&1
            if [ $? -ne 0 ] 
                then write_err "[ ERROR ] Creating user '${wusr}'" 
                else write_log "[ OK ] User '$wusr' created with UID '$wuid'" 
            fi
            write_log "Setting temporary password for user '$wusr' " 
            usermod -p '$6$.VciX6502LQyEkCI$Pp893mw.wcPrPfGKjPSSLWhOUOpPUKrUdlIO0miuIltjrO9gC.3ZoGg2LlEP4DCGFD.ZZyiiGTqqZzWK2PnKq0' $wusr >> $LOG_FILE 2>&1
            if [ $? -ne 0 ] 
                then write_err "[ ERROR ] Setting temporary password of user '${wusr}'" 
                else write_log "[ OK ] Password now set for user '${wusr}'." 
            fi
       else write_log "Account 'sadmin' already exist."
            write_log " "
    fi
    chmod -v 700 /home/$wusr

    # Create System Administrator $SYS_ACCOUNT (If not exist) and make it part of 'sadmin' group.
    wgrp="sadmin" ; wuid=604 ; wusr="$SYS_ACCOUNT"                      # Set Sysadmin Account
    id "$wusr" >/dev/null 2>&1                                          # User Account Exist ?
    if [ $? -ne 0 ]                                                     # If User don't exist 
       then write_log " "
            write_log "Creation SysAdmin account '$SYS_ACCOUNT' - $SYS_NAME"
            write_log "useradd -c '$SYS_NAME' -g $wgrp -u $wuid $wusr"
            useradd -m -c "$SYS_NAME" -e '' -s '/bin/bash' -g $wgrp -u $wuid $wusr  > /dev/null 2>&1
            if [ $? -ne 0 ] 
                then write_err "[ ERROR ] Creating user ${wusr}" 
                else write_log "[ OK ] User '$wusr' created with UID $wuid" 
            fi
       else write_log "Make sure $wusr is part of the group $wgrp" 
            usermod -a -G $wgrp $wusr
            if [ $? -ne 0 ] 
                then write_err "[ ERROR ] Adding group $wgrp to user ${wusr}" 
                else write_log "[ OK ] User '$wusr' was added to the group $wgrp" 
            fi
    fi


    # Set '$SYS_ACCOUNT' Account password 
    # Use command below to generate your own encrypted password for usermod command :
    # python -c 'import crypt,getpass;pw=getpass.getpass();print(crypt.crypt(pw) if (pw==getpass.getpass("Confirm: ")) else exit())'
    #write_log " "
    #write_log "Setting password for '$wusr' account."
    #usermod -p '$1$OMikOlq8$mUJPnNgyuzhLvg7e9Al07/' $wusr >> $LOG_FILE 2>&1
    usermod -p '$6$G2NKctpk.WDaZeFz$iSWJZxMvcUy0Q8tq8EwlQPOKX2uPpTKYgJgSvpas3jG2KQhLAB9FniDujoglV2wZkibxL.c96J88ZVN.nQauM.' $wusr >> $LOG_FILE 2>&1
    if [ $? -ne 0 ] 
       then write_err "[ ERROR ] Setting '$wusr' password - id='$wuid'." 
       else write_log "[ OK ] Password now set for user '${wusr}'."
    fi 

    # Making '$SYS_ACCOUNT' part of sudo group.
    #write_log " "
    #write_log "Making '$wusr' account part of sudo/admin group"
    if [ "$OS_NAME" =  "REDHAT" ] || [ "$OS_NAME" =  "CENTOS" ] || [ "$OS_NAME" =  "FEDORA" ] || [ "$OS_NAME" =  "ALMA" ] || [ "$OS_NAME" =  "ROCKY" ]
        then write_log "gpasswd -a $wusr wheel"                        # Cmd for Redhat Family
             gpasswd -a $wusr wheel | while read wline ; do write_log "$wline"; done              
             if [ $? -ne 0 ]
                then write_err "[ ERROR ] Making '$wusr' part of sudo group 'wheel'."
                else write_log "[ OK ] User '$wusr' is now part of sudo group 'wheel'."
             fi
        else write_log "usermod -a -G sudo '$wusr'"                     # Cmd Debian Family
             usermod -a -G sudo $wusr >> $LOG_FILE 2>&1                 # Make user part of sudo Grp
             if [ $? -ne 0 ]
                then write_err "[ ERROR ] Making '$wusr' part of sudo group 'sudo'."
                else write_log "[ OK ] User '$wusr' is now part of sudo group 'sudo'."
             fi
    fi

    # Setting 'jacques' non-expirable.
    write_log " "
    write_log "Setting '$wusr' account to non-expirable"
    write_log "usermod -e '' '$wusr' "                                  # Password do not expire
    usermod -e '' $wusr > /dev/null 2>&1                                # Password do not expire
    if [ $? -ne 0 ]
       then write_err "[ ERROR ] Setting '$wusr' to non-expirable."
       else write_log "[ OK ] User '$wusr' is now set to non-expirable."
    fi

    # Set root password
    write_log " "
    write_log "Setting password for 'root' account."
    usermod -p '$6$BcMPKh8fEY34Tu91$05rtVycGGleCWLyg6lwmf0n4SBNpHSQugQRkvLHZ1.ml4QY.Egc7oK0jO81inTyMvqyqZtfrhsX5QhqrRmv/1.' root >> $LOG_FILE 2>&1
    if [ $? -ne 0 ] 
       then write_err "[ ERROR ] Setting 'root' password." 
       else write_log "[ OK ] Password was set for user 'root'."
    fi 

    # Root password to not expire
    write_log " "
    write_log "Setting root account to non-expirable"
    write_log "usermod -e '' root"
    usermod -e '' root  > /dev/null 2>&1
    if [ $? -ne 0 ]
       then write_err "[ ERROR ] setting 'root' to non-expirable."
       else write_log "[ OK ] User 'root' is now set to non-expirable."
    fi

    # Set root password exipration to 90 days
    #/usr/bin/chage -M 90 root
}



# ==================================================================================================
# Create the OpenSSH Keys for user name received
# ==================================================================================================
function s42_create_user_ssh_keys()
{

    SUSER=$1                                                            # Save UserName to process
    SGROUP=`id -gn ${SUSER}`                                            # Get the user Primary Group
    SSH_DIR="/home/${SUSER}/.ssh"                                       # SSH Directory of user
    section_title "${FUNCNAME[0]} - Usr: $SUSER  Grp: $SGROUP  SSH_DIR: $SSH_DIR" 

    # If .ssh directory doesn't exist for ${SUSER} user - create it
    if [ ! -d "$SSH_DIR" ]
       then write_log "Create $SSH_DIR directory."
            mkdir -p $SSH_DIR  >> $LOG_FILE 2>&1
            if [ $? -ne 0 ] ; then write_err "[ ERROR ] Creating ${SSH_DIR}." ; fi
    fi

    # Set Directory Permission and Owner of /home/${USER}/.ssh
    chmod -v 700 $SSH_DIR  >> $LOG_FILE 2>&1
    if [ $? -ne 0 ] ; then write_err "[ ERROR ] Changing permission to 700 on ${SSH_DIR}" ; fi
    chown -v ${SUSER}:${SGROUP} ${SSH_DIR}  >> $LOG_FILE 2>&1
    if [ $? -ne 0 ] ; then write_err "[ ERROR ] On 'chown ${SUSER}:${SGROUP} ${SSH_DIR}'" ; fi
    ls -la /home/${SUSER} | while read wline ; do write_log "$wline"; done


    # If ${SUSER} user ssh private key doesn't exist - generate it
    F="$SSH_DIR/id_rsa"                                                 # Private Key NAme
    KT="rsa"                                                            # Key Type 
    if [ ! -f "$F" ]
       then write_log " " 
            write_log "Creating '$SUSER' user '$KT' keys ..."
            which ssh-keygen >>/dev/null 2>&1
            if [ $? -eq 0 ] 
               then kg=`which ssh-keygen` 
                    kc="${SUSER}@${HOSTNAME}"                           # Key Comment
                    write_log "$kg -t rsa -b 4096 -C '${kc}' -N '' -q -f $F"
                    $kg -t rsa -b 4096 -C "${kc}" -N '' -q -f $F >> $LOG_FILE 2>&1
                    if [ $? -ne 0 ] ; then write_err "[ ERROR ] ssh-keygen for ${SUSER}." ; fi
               else write_err "Missing 'ssh-keygen' can't generate ssh keys for ${USER}."
                    return 1
            fi 
    fi

    # Set Permission and Owner Private Key
    write_log " "
    write_log "chmod 600 $F" ; chmod 600 $F >> $LOG_FILE 2>&1
    if [ $? -ne 0 ] ; then write_err "[ ERROR ] On 'chmod 600 $F'" ; fi
    write_log "chown ${SUSER}:${SGROUP} $F" 
    chown ${SUSER}:${SGROUP} $F >> $LOG_FILE 2>&1
    if [ $? -ne 0 ] ; then write_err "[ ERROR ] On 'chown ${SUSER}:${SGROUP} $F'" ; fi

    # Set Permission and Owner Public Key
    write_log " "
    F="$SSH_DIR/id_${KT}.pub" 
    write_log "chmod 644 $F" ; chmod 644 $F >> $LOG_FILE 2>&1
    if [ $? -ne 0 ] ; then write_err "[ ERROR ] On 'chmod 600 $F'" ; fi
    write_log "chown ${SUSER}:${SGROUP} $F" 
    chown ${SUSER}:${SGROUP} $F >> $LOG_FILE 2>&1
    if [ $? -ne 0 ] ; then write_err "[ ERROR ] On 'chown ${SUSER}:${SGROUP} $F'" ; fi

    # Make a copy of the ${SUSER} public key under a hostname.pub
    write_log " "
    F1="$SSH_DIR/id_${KT}.pub" ; F2="$SSH_DIR/${SUSER}@${HOSTNAME}.pub"
    write_log "cp $F1 $F2"
    cp $F1 $F2 >> $LOG_FILE 2>&1
    if [ $? -ne 0 ] ;then write_err "[ ERROR ] Copying public key to $F2" ;fi
    write_log "chown ${SUSER}:${SGROUP} $F2"
    chown ${SUSER}:${SGROUP} $F2 >> $LOG_FILE 2>&1
    if [ $? -ne 0 ] ;then write_err "[ ERROR ] On 'chown ${SUSER}:${SGROUP} $F2'" ;fi
    write_log "chmod 664 $F2"
    chmod 644 $F2 >> $LOG_FILE 2>&1
    if [ $? -ne 0 ] ;then write_err "[ ERROR ] on 'chmod 644 $F2'" ;fi

    # Change permission and Owner of authorized_keys file exist.
    F="/home/${SUSER}/.ssh/authorized_keys" 
    if [ -f "$F" ]
       then write_log "chmod 600 $F" 
            chmod 600 $F
            write_log "chown ${SUSER}:${SGROUP} $F"
            chown ${SUSER}:${SGROUP} $F 
    fi 

    # Create environment file used when we will be doing ssh to this server
    # ~/.ssh/environment file can be used to set variables you want available for remote commands.
    # You will have to enable PermitUserEnvironment in the sshd configuration.
    F="/home/${SUSER}/.ssh/environment" 
    if [ -f "$F" ]
       then grep -qE 'SADMIN=' $F
            if [ $? -ne 0 ] ; then echo "SADMIN=${SADMIN}" >>$F ; fi
            grep -qE 'SA='     $F
            if [ $? -ne 0 ] ; then echo "SA=$SA"           >>$F ; fi
       else echo "SADMIN=${SADMIN}"  >$F
            echo "SA=$SA"           >>$F
    fi 
    write_log "chmod 600 $F" 
    chmod 600 $F
    write_log "chown ${SUSER}:${SGROUP} $F"
    chown ${SUSER}:${SGROUP} $F 

    # Make user 'jacques' part of SADMIN group
    if [ "${SUSER}" == "jacques" ]
       then write_log "usermod -a -G sadmin ${SUSER}"
            usermod -a -G sadmin ${SUSER} 
    fi 
       
    # List SSH directory
    write_log " "
    write_log "So here is the content of .ssh directory of '$SUSER' user."
    ls -latr ${SSH_DIR} | while read wline ; do write_log "$wline"; done

    write_log " "
    write_log "Show User ID and Group"
    write_log "$(id $SUSER)" 
    write_log " "
}



# ==================================================================================================
# Create the OpenSSH Keys for user name received
# ==================================================================================================
function s43_create_sudoer_file()
{
    F="/etc/sudoers.d/033_sadmin"
    section_title "${FUNCNAME[0]} - Creating $F sudo file." 

    echo "Defaults  !requiretty"              > $F
    echo "sadmin    ALL=(ALL) NOPASSWD: ALL"  >> $F
    echo "jacques   ALL=(ALL) NOPASSWD: ALL"  >> $F
    echo "Defaults  env_keep += "SADMIN""     >> $F
    echo "Defaults  env_keep += "DSADM""      >> $F
    echo "Defaults  env_keep += "DSADMBIN""   >> $F
    echo "Defaults  env_keep += "DSA""        >> $F
    echo "Defaults  env_keep += "DSABIN""     >> $F
    echo "Defaults  env_keep += "SA""         >> $F
    SUDO_PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:${SA}/bin:${SADMIN}/bin"
    echo "Defaults  secure_path=$SUDO_PATH"   >> $F

    # Show new content
    write_log " " 
    write_log "Content of $F :"                   # Write new Updated file
    write_log "**********" 
    grep -Ev "^$|^#"  $F | while read wline ; do write_log "$wline" ; done 
    write_log "**********"  
}



# ================================================================================================
# Create standard mount point in /mnt
# ================================================================================================
function s50_create_mnt_mount_point()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - v${OS_VERSION}"

    mount_point=( /mnt/iso /mnt/usb /mnt/nfs /mnt/usbkey /mnt/cdrom )
    for WDIR in "${mount_point[@]}"
        do
        if  [ ! -d "$WDIR" ]
           then write_log "Creating ${WDIR}..."
                mkdir -p ${WDIR}
                if [ $? -ne 0 ]
                    then write_err "[ ERROR ] Creating Directory ${WDIR}."
                    else write_log "   - Directory ${WDIR} created."
                fi
           else write_log "Mount point $WDIR already exist."
        fi
        write_log "   - chmod 755 $WDIR"
        chmod 775 $WDIR
        if [ $? -ne 0 ]
           then write_err "[ ERROR ] Changing persission of $WDIR to 755."
        fi
        write_log "   - chown root:root $WDIR" 
        chown root:root $WDIR
        if [ $? -ne 0 ]
           then write_err "[ ERROR ] Changing owner & group on directory ${WDIR}."
        fi
        done
    write_log " "
    write_log "List of directories in /mnt :"
    ls -l /mnt | while read wline ; do write_log "$wline"; done
}



# ==================================================================================================
# Function to put standard warning in /etc/issue and /etc/issue.net (Received in parameter)
# ==================================================================================================
function s54_update_etc_issue()
{
    F="$1"                                                          # File that will be overwritten
    section_title "${FUNCNAME[0]} - $OS_NAME - Update $F"           # Write standard Section Heading
    if [ -f "$F" ] ; then save_original "$F" ; fi                   # Save Original before restore

    echo "***AVERTISSEMENT*AVERTISSEMENT*AVERTISSEMENT*AVERTISSEMENT*AVERTISSEMENT**"  > $F
    echo "*                                                                        *" >> $F
    echo "*      L'accès à ce systeme doit être authorisé!                         *" >> $F
    echo "*      Toute activité est sujette à une surveillance!                    *" >> $F
    echo "*                                                                        *" >> $F
    echo "*      Toute tentative d'accès non-autorisé à ce système est             *" >> $F
    echo "*      strictement défendu.                                              *" >> $F
    echo "*________________________________________________________________________*" >> $F
    echo "*                                                                        *" >> $F
    echo "*      This system is for authorized use only!                           *" >> $F
    echo "*      All activity may be subject to monitoring!                        *" >> $F
    echo "*                                                                        *" >> $F
    echo "*      Any unauthorized attempt and access to this system is             *" >> $F
    echo "*      strictly prohibited.                                              *" >> $F
    echo "*                                                                        *" >> $F
    echo "**WARNING**WARNING**WARNING**WARNING**WARNING**WARNING**WARNING**WARNING**" >> $F

    write_log " " 
    write_log "Content of $F after updating it :"
    write_log "**********"
    cat  $F | while read wline ; do write_log "$wline"; done
    write_log "**********"
}



# ================================================================================================
# Create custom /etc/motd
# ================================================================================================
function s58_update_etc_motd()
{
    F="/etc/motd"                                                       # File will be overwritten
    section_title "${FUNCNAME[0]} - $OS_NAME - Update $F"               # Write std Section Heading

    if [ -f "$F" ] ; then save_original "$F" ; fi                       # Save Original before
    FIGNAME=$(echo "$HOSTNAME" | tr [a-z] [A-Z])                        # Put hostname in uppercase
    figlet "$FIGNAME" > $F                                              # Put Big HostName in file

    write_log " " 
    write_log "Content of $F after updating it :"
    write_log "**********"
    cat  $F | while read wline ; do write_log "$wline"; done
    write_log "**********"
}
 





# ================================================================================================
# Copy the customized files to their appropriate location
# ================================================================================================
function s62_extract_std_files()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION}" 

    # Get SysAdmin UserName and Primary Group Name.
    grpName=`id -gn $SYS_ACCOUNT`                                       # Get SysAdmin group name
    userName="${SYS_ACCOUNT}"                                           # Set User Name

    # Save Important original files before overwrite them.
    F="/root/.bash_profile"             ; if [ -f $F ] ;then save_original "$F" ;fi 
    F="/root/.bashrc"                   ; if [ -f $F ] ;then save_original "$F" ;fi 
    F="/root/.dir_colors"               ; if [ -f $F ] ;then save_original "$F" ;fi 
    F="/home/${userName}/.bash_profile" ; if [ -f $F ] ;then save_original "$F" ;fi 
    F="/home/${userName}/.bashrc"       ; if [ -f $F ] ;then save_original "$F" ;fi 
    write_log " "

    # Extracting the file /install/files/files.tar.gz onto / of the system
    if [ ! -f "${STD_FILES}/files.tar.gz" ]
        then    write_log ""  ; write_log "" ; write_log ""
                write_log "Content of the directory ${STD_FILES}"
                ls -l ${STD_FILES} >> $LOG_FILE 2>&1
                write_log ""
                write_err "[ ERROR ] Missing Standard Fileset - ${STD_FILES}/files.tar.gz"
                write_err "CREATE OR RECREATE THE FILE AND RUN THE INSTALLATION AGAIN"
                write_err "1- cd /install"
                write_err "2- run this script : ./sadm_create_installation_tgz.sh "
                write_err "3- Run installation again"
                return 1
        else    cd /
                write_log ""  ;
                write_log "Permission on /, /root & /home before restoring of our custom files are:"
                ls -ld /     >> $LOG_FILE 2>&1
                ls -ld /root >> $LOG_FILE 2>&1
                ls -ld /home >> $LOG_FILE 2>&1
                write_log ""  ;
                write_log ""  ;
                write_log "Extracting files from ${STD_FILES}/files.tar.gz to server"
                tar -xvzf ${STD_FILES}/files.tar.gz | while read wline ; do write_log "$wline"; done
                if [ $? -eq 0 ] 
                    then write_log "[OK] Files extracted with success"
                    else write_err "[ ERROR ] We had problem extracting file (see $LOG_FILE)" 
                fi
                write_log ""  ;
    fi
    write_log " " ; write_log "**********"

    # Set / owner and Permission
    write_log " "
    write_log "Setting Owner and permission on / "
    chmod -v 0755 /  >> $LOG_FILE 2>&1
    chown -v root:root /  >> $LOG_FILE 2>&1

    # Set Root Directory Owner and permission
    F="/root"
    if [ -d $F ] 
       then write_log " "
            write_log "Setting Owner and permission on $F"
            write_log "   - chown -R root:root $F "
            chown -Rv root:root $F  >> $LOG_FILE 2>&1
            write_log "   - chmod 750 $F "
            chmod -v 700 $F  >> $LOG_FILE 2>&1 

            # Root .ssh directories and files.
            F="/root/.ssh"
            write_log "   - chmod 700 $F" 
            chmod -v 700 $F  >> $LOG_FILE 2>&1

            F="/root/.ssh/authorized_keys" 
            write_log "   - chmod 600 $F"
            chmod -v 600 $F  >> $LOG_FILE 2>&1

            # Redirect root email to the SysAdmin
            echo "$SYS_EMAIL" > /root/.forward
            F="/root/.forward"
            write_log "   - chmod 600 $F" 
            chmod -v 600 $F  >> $LOG_FILE 2>&1

            # Set Permission and ownership on files included in tgz
            F="/root/.bash_profile"
            write_log "   - chmod 640 $F"
            chmod -v 600 $F  >> $LOG_FILE 2>&1
            #
            F="/root/.bashrc"
            chmod -v 600 $F  >> $LOG_FILE 2>&1
            write_log "   - chmod 640 $F"
            #
            F="/root/.dir_colors"
            chmod -v 640 $F  >> $LOG_FILE 2>&1
            write_log "   - chmod 640 $F" 
            #
            write_log " "
            ls -la /root | while read wline ; do write_log "$wline"; done
            write_log " "
            #tree /root | while read wline ; do write_log "$wline"; done
    fi 


    # Set /home Directory Owner and permission
    F="/home"
    if [ -d $F ] 
       then write_log " "
            write_log "Setting Owner and permission on $F"
            write_log "   - chown root:root $F "
            chown -v root:root $F
            write_log "   - chmod 700 $F"
            chmod -v 700 $F 
            write_log " "
            ls -la $F | while read wline ; do write_log "$wline"; done
            write_log " "
    fi 

    # Set file permission and owner in Sysadmin Home Directory
    F="/home/${userName}"
    if [ -d $F ] 
       then write_log " "
            write_log "Setting Owner and permission on $F."
            write_log "   - chown -R ${userName}:${grpName} $F"
            chown -R ${userName}:${grpName} $F   >> $LOG_FILE 2>&1
            write_log "   - chmod 700 $F"
            chmod -v 700 $F  >> $LOG_FILE 2>&1

            F="/home/${userName}/.bash_profile"         ; write_log "   - chmod -v 700 $F" ;chmod -v 700 $F 
            F="/home/${userName}/.bashrc"               ; write_log "   - chmod -v 700 $F" ;chmod -v 700 $F 
            F="/home/${userName}/.dir_colors"           ; write_log "   - chmod -v 640 $F" ;chmod -v 640 $F 
            F="/home/${userName}/.vimrc"                ; write_log "   - chmod -v 640 $F" ;chmod -v 640 $F
            F="/home/${userName}/.ssh"                  ; write_log "   - chmod -v 700 $F" ;chmod -v 700 $F 
            F="/home/${userName}/.ssh/authorized_keys"  ; write_log "   - chmod -v 600 $F" ;chmod -v 600 $F 
            F="/home/${userName}/.ssh/config"           ; write_log "   - chmod -v 600 $F" ;chmod -v 600 $F 
            F="/home/${userName}/.ssh/environment"      ; write_log "   - chmod -v 600 $F" ;chmod -v 600 $F 
            F="/home/${userName}/bin"                   ; write_log "   - chmod -v 750 $F" ;chmod -v 750 $F 
            F="/home/${userName}/bin/keychain.sh"       ; write_log "   - chmod -v 750 $F" ;chmod -v 750 $F 
            F="/home/${userName}/.config"               ; write_log "   - chmod -v 755 $F" ;chmod -v 755 $F 
            F="/home/${userName}/.config/Code"          ; write_log "   - chmod -v 750 $F" ;chmod -v 750 $F 
            F="/home/${userName}/.config/Code/User"     ; write_log "   - chmod -v 750 $F" ;chmod -v 750 $F 
            F="/home/${userName}/.config/Code/User/settings.json" ; write_log "   - chmod -v 644 $F"
            chmod -v 644 $F 
            write_log " "
            ls -lad /home | while read wline ; do write_log "$wline"; done
            ls -la  /home/${userName} | while read wline ; do write_log "$wline"; done
            write_log " "
            #tree /home/${userName} | while read wline ; do write_log "$wline"; done
    fi 
}




# ================================================================================================
# Configure SSH Server files and client
# ================================================================================================
function s65_ssh_configuration()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION}" 

    # SSH Banner File 
    F="/etc/ssh/banner.txt"                                             # File to configure
    write_log "Creating a new $F"                                       # Show what happening
    if [ -f "$F" ] ; then save_original "$F" ; fi                       # Save Original file before
    printf "${HOSTNAME}\n" >$F                                          # Create new banner.txt file

    write_log "chmod -v 644 $F" ; chmod -v 644 $F                       # Set Permission    
    if [ $? -ne 0 ] ;then write_err "[ ERROR ] chmod of $F failed." ;fi # Show the chmod failed

    write_log "chown -v root:root $F" ; chown root:root $F                 # Set file Owner & Group    
    if [ $? -ne 0 ] ;then write_err "[ ERROR ] Setting Owner and Group of $."  ;fi 

    write_log " " 
    write_log "Content of $F after modification :"                      # Advise what is following
    write_log "**********"
    cat  $F | while read wline ; do write_log "$wline"; done            # Show file final content
    write_log "**********"

    # SSH Client configuration file  
    write_log " "
    F="/etc/ssh/ssh_config"                                             # File to configure
    if [ -f "$F" ] ; then save_original "$F" ; fi                       # Save Original file before
    printf "# File created by $PN v${PIVER} on `date`\n" >$F            # Advertise  lines
    #echo "ForwardX11                  no" >>$F                          # Permit GUI App. to be used
    echo "PubkeyAuthentication        yes" >>$F                         # Allow public Key Auth.
    echo "Port                        32"  >>$F                         # SSH Port is now 32 not 22
    echo "Protocol                    2"   >>$F                         # Insure Protocol 2 is use
    #echo "MaxAuthTries                3"   >>$F                         # Max 3 pwd retries
    write_log "chmod -v 640 $F" ; chmod -v 644 $F                       # Set Permission    
    if [ $? -ne 0 ] ;then write_err "[ ERROR ] chmod of $F failed." ;fi # Show the chmod failed

    write_log "chown -v root:root $F" ; chown -v root:root $F           # Set file Owner & Group    
    if [ $? -ne 0 ] ;then write_err "[ ERROR ] Setting Owner and Group of $."  ;fi 
    
    write_log " " 
    write_log "Content of $F after modification :"                      # Advise what is following
    write_log "**********"
    cat  $F | while read wline ; do write_log "$wline"; done            # Show file final content
    write_log "**********"


    # SSH Server configuration file  
    write_log " " ; write_log " "
    F="/etc/ssh/sshd_config"                                            # File to configure
    if [ -f "$F" ] ; then save_original "$F" ; fi                       # Save Original file before
    #
    printf "# File created by $PN v${PIVER} $(date)\n" >$F              # Advertise following lines
    echo "PubkeyAuthentication             yes                    # Permit Public Key Auth." >>$F
    echo "Port                             32                     # SSH Port will be 32" >>$F
    echo "Protocol                         2                      # Assure using protocol 2" >>$F
    echo "PermitRootLogin                  prohibit-password      # No Root with password" >>$F
    echo "SyslogFacility                   AUTH                   # Facility code in Log" >>$F
    echo "LogLevel                         INFO                   # Log in info mode" >>$F
    echo "LoginGraceTime                   60                     # time to login successfully" >>$F
    echo "PasswordAuthentication           yes                    # Allow Password Authen."       >>$F
    echo "PermitEmptyPasswords             no                     # No empty password."           >>$F
    echo "HostbasedAuthentication          no                     # Disable Host Based Auth."     >>$F
    echo "IgnoreRhosts                     yes                    # Ignore .rhost & .shost"       >>$F
    echo "StrictModes                      yes                    # check permissions in \$HOME"  >>$F
    echo "AllowTcpForwarding               yes                    # Permit tunneling"             >>$F
    echo "X11Forwarding                    no                     # Permit X11 GUI Forward"       >>$F
    echo "#Banner                          /etc/ssh/banner.txt    # Warning msg before auth."     >>$F
    echo "UsePAM                           yes                    # Uses PAM for auth. users"     >>$F
    echo "ChallengeResponseAuthentication  no                     # ChallengeResponse auth.OFF"   >>$F
    echo "PermitUserEnvironment            yes                    # Allow Usr changing their env.">>$F
    echo "AddressFamily                    inet                   # Restrict SSH to IP v4 Only"   >>$F
    echo "AuthorizedKeysFile               .ssh/authorized_keys   # only check .ssh/authorized_keys" >>$F
    echo "MaxAuthTries                     5                      # Max. Auth retries is 5" >>$F
    echo "MaxSessions                      5                      # Max. simultanious sessions is 5" >>$F
    echo "ClientAliveInterval              10800                  # 3hrs nb sec conn. can remain inactive." >>$F
    echo "ClientAliveCountMax              3                      # Nb. time sending alive msg before disconnect." >>$F
    #echo "AllowGroups                      sadmin                # Only this group to access SSH." >>$F
    #echo "AllowUsers                       jacques sadmin        # Only these users to access SSH." >>$F
    
    

    #
    if [ "$PACKTYPE" = "rpm" ]                                          # RedHat/Fedora/CentOS
        then echo "# SFTP Server (rpm)" >>$F
             echo "Subsystem sftp /usr/libexec/openssh/sftp-server">>$F # SFTP Server Path differ
        else echo "# SFTP Server (deb)" >>$F                            # Debian/Raspbian/Ubuntu ...
             echo "Subsystem sftp /usr/lib/openssh/sftp-server " >>$F   # SFTP Server Path differ
    fi 
    write_log "chmod -v 640 $F" ; chmod 644 $F                             # Set Permission    
    if [ $? -ne 0 ] ;then write_err "[ ERROR ] chmod of $F failed." ;fi # Show the chmod failed

    write_log "chown -v root:root $F" ; chown root:root $F                 # Set file Owner & Group    
    if [ $? -ne 0 ] ;then write_err "[ ERROR ] Setting Owner and Group of $."  ;fi 

    write_log " " 
    write_log "Content of $F after modification :"                      # Advise what is following
    write_log "**********"
    cat  $F | while read wline ; do write_log "$wline"; done            # Show file final content
    write_log "**********"
    write_log " "

    systemctl list-units --type=service --no-pager | grep -q sshd.service
    if [ $? -eq 0 ] 
        then service_enable sshd 
        else service_enable ssh 
    fi
}



# ==================================================================================================
# Setup Setup /etc/sysctl.conf - Kernel & Network Parameters
# ==================================================================================================
function n67_save_sysctl_conf()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION}" 

    # Save original file and show content before changing it.
    sysfile="/etc/sysctl.conf"                                          # File that we modify
    if [ -f "$sysfile" ]                                                # If interfaces file Exist
       then save_original "$sysfile"                                    # Save original network file
            write_log " " 
            write_log "**********"
            write_log "Original content of $sysfile :"
            grep -Ev "^$|^#"  $sysfile | nl | while read wline ; do write_log "$wline"; done
            write_log "**********"
    fi

#    if [ -f "$sysfile" ] && [ -f /root/sysctl.conf ]
#        then f1="/root/sysctl.conf"
#             f2="/etc/sysctl.conf" 
#             write_log "Moving '$f1' to '$f2' ..." 
#             mv "$f1" "$f2" 
#             chmod -v 644 $F2 
#             chown -v root:root $f2
#    fi 
}






# ================================================================================================
# Configure ReaR site configuration file
# ================================================================================================
function s70_rear_site_conf()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - v${OS_VERSION}" 

    # ReaR only work on Intel Architecture
    if [ "$SYS_ARCH" != "x86_64" ] && [ "$SYS_ARCH" != "i686" ]
        then write_log "ReaR is not supported on '$SYS_ARCH' architecture."
             return 0
    fi 

    # ReaR Default configuration file 
    F="/etc/rear/site.conf"                                             # File to configure
    mkdir /etc/rear > /dev/null 2>&1                                    # Just in Case not there
    if [ -f "$F" ] ; then save_original "$F" ; fi                       # Save Original file before
    #
    write_log " "
    write_log "Creating the ReaR Backup config file ($F)" 
    printf  "# File created by $PN v${PIVER} on `date`\n" >$F           # Advertise following lines
    echo -e "#" >> $F 
    echo -e "# Default PostInstall Rear Client Configuration" >> $F 
    echo -e "# ----------------------------------------------------------------------" >> $F 
    echo -e "#" >> $F 
    echo -e "# Create a bootable ISO9660 image on disk as rear-$(hostname).iso" >> $F 
    echo -e "OUTPUT=ISO" >> $F 
    echo -e "#" >> $F 
    echo -e "# Internal backup method (NETFS=NFS) used to create a tar backup file." >>$F 
    echo -e "BACKUP=NETFS" >> $F 
    echo -e "#" >> $F 
    echo -e "#" >> $F 
    echo -e "# To backup to NFS disk, use BACKUP_URL=nfs://nfs-server-name/share/path" >> $F 
    echo -e "# See ReaR backup section in '$SADMIN/cfg/sadmin.cfg'"  >> $F
    echo -e "# Line below is generated before each backup, " >> $F
    echo -e "# based on 'SADM_BACKUP_NFS_SERVER' & 'SADM_BACKUP_NFS_MOUNT_POINT' "  >> $F
    echo -e "BACKUP_URL=\"nfs://${BATNAS}/volume1/backup_rear/\"" >> $F 
    echo -e "#" >> $F 
    echo -e "#" >> $F 
    echo -e "# Keep previous backup archive when new one are created (Y/N)" >> $F
    echo -e "NETFS_KEEP_OLD_BACKUP_COPY=Y " >> $F
    echo -e "#" >> $F 
    echo -e "# Directory within mount point where iso and tgz will be stored" >> $F 
    echo -e "NETFS_PREFIX=\"$HOSTNAME\"" >> $F 
    echo -e "#" >> $F 
    echo -e "# disable SELinux while the backup is running" >> $F 
    echo -e "BACKUP_SELINUX_DISABLE=1" >> $F 
    echo -e "#" >> $F 
    echo -e "# Prefix name for ISO images without the .iso suffix (rear_HOSTNAME.iso)" >> $F 
    echo -e "ISO_PREFIX=\"rear_$HOSTNAME\"" >> $F 
    echo -e "#" >> $F 
    echo -e "# Name of Backup (tar.gz) File" >> $F 
    echo -e "BACKUP_PROG_ARCHIVE=\"rear_${HOSTNAME}\"" >> $F 
    echo -e "#" >> $F 
    echo -e "#" >> $F 
    echo -e "# Exclude Volume Group (and filesystem they include)" >> $F 
    echo -e "# Do not forget to comment filesystem on excludevg in /etc/fstab prior to 1st reboot" >> $F 
    echo    "EXCLUDE_VG=(datavg)" >> $F 
    echo    "EXCLUDE_MOUNTPOINTS=(/lvol1)" >> $F 
    echo -e "" >> $F 
    #
    write_log "chmod 644 $F" ; chmod 644 $F                             # Set Permission    
    if [ $? -ne 0 ] ;then write_err "[ ERROR ] chmod of $F failed." ;fi # Show the chmod failed
    write_log "chown root:root $F" ; chown root:root $F                 # Set file Owner & Group    
    if [ $? -ne 0 ] ;then write_err "[ ERROR ] Setting Owner and Group of $."  ;fi 

    write_log " "
    write_log "Content of $F after modification :"                      # Advise what is following
    write_log "**********"
    cat  $F | while read wline ; do write_log "$wline"; done            # Show file final content
    write_log "**********"
}


# ================================================================================================
# Setup CFG2HTML site file
# ================================================================================================
function s74_cfg2html_site_file()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - v${OS_VERSION}" 

    # ReaR Default configuration file 
    F="/etc/cfg2html/files"                                             # File to configure
    mkdir /etc/cfg2html > /dev/null 2>&1                                # Just in Case not there
    if [ -f "$F" ] ; then save_original "$F" ; fi                       # Save Original file before
    #
    write_log " "
    write_log "Creating $F"
    printf  "# File created by $PN v${PIVER} on `date`\n" >$F           # Advertise following lines
    echo -e "#" >> $F 
    echo -e "# Default PostInstall cfg2html extension file" >>$F
    echo -e "# config files included here displayed without "#" as first character" >>$F
    echo -e "# with  egrep -v '^#|^ *$' <filename>" >>$F
    echo -e "# currently NO extra commandlines are supported!" >>$F
    echo -e "#" >>$F
    echo -e "# uncomment next line to test the example:" >>$F
    echo -e "#FILES="/etc/adjtime /etc/paper.config"" >>$F
    echo -e "FILES=\"/etc/profile.d/sadmin.sh /etc/profile.d/sa.sh /etc/environment /etc/ssh/sshd_config \"" >>$F
    echo -e "#" >>$F
    echo -e "# Future plans/enhancements:" >>$F
    echo -e "#CATFILES=\"/etc/motd /etc/issue\"" >>$F

    write_log " "
    write_log "Content of $F after modification :"                      # Advise what is following
    write_log "**********"
    cat  $F | while read wline ; do write_log "$wline"; done            # Show file final content
    write_log "**********"
    
    write_log "chmod 644 $F" ; chmod -v 644 $F  >> $LOG_FILE 2>&1       # Set Permission    
    if [ $? -ne 0 ] ;then write_err "[ ERROR ] chmod of $F failed." ;fi # Show the chmod failed
    
    write_log "chown root:root $F" ; chown -v root:root $F >> $LOG_FILE 2>&1 # Set file Owner & Group
    if [ $? -ne 0 ] ;then write_err "[ ERROR ] Setting Owner and Group of $."  ;fi 
}



# ==================================================================================================
# Setup chrony service
# Parameter receive is Chrony config type : [C]lient or [S]server
# ==================================================================================================
function s77_setup_chrony()
{
    CHRONY=`echo "$CHRONY" | tr [a-z] [A-Z]`                            # Make Chrony type Uppercase
    section_title "${FUNCNAME[0]} CHRONY TYPE : $CHRONY"
    
    if [ "$CHRONY" = "S" ]                                              # Configure Chrony [S]erver
       then write_log "Setup Chrony Server" 
            s79_setup_chrony_server                                     # Configure chrony Server
       else write_log "Setup Chrony Client" 
            s78_setup_chrony_client                                     # Configure chrony [C]lient
    fi 
}





# ==================================================================================================
# Setup chrony client service
# ==================================================================================================
function s78_setup_chrony_client()
{
    subsection_title "${FUNCNAME[0]}"
    D=$(date "+%Y/%m/%d %H:%M")                                          # Date of Change

    # If on rpm Package type, chrony config file is /etc/chrony.conf
    if [ "$PACKTYPE" = "rpm" ]
       then F="/etc/chrony.conf"                                        # Chrony Config file
            echo "# --- Added by $PN v${PIVER} script - $D" >>$F
            for ntp in $NTP_SERVERS
                do
                echo "server $ntp iburst"   >> $F
                write_log "server $ntp iburst"  
                done
    fi

    # If on deb Package type, chrony config file is /etc/chrony/chrony.conf
    if [ "$PACKTYPE" = "deb" ]
       then F="/etc/chrony/chrony.conf"
            echo "# --- Added by $PN v${PIVER} script - $D" >>$F
            for ntp in $NTP_SERVERS
                do
                echo "pool $ntp iburst prefer"   >> $F
                write_log "pool $ntp iburst prefer"
                done
    fi

    write_log " "
    write_log "Content of $F after modification :"                      # Advise what is following
    write_log "**********"
    grep -Ev "^$|^#" $F | nl | while read wline ; do write_log "$wline"; done  # Show file content
    write_log "**********"

    write_log " "
    systemctl is-enabled chronyd  > /dev/null 2>&1                      # Is Service enabled ? 
    if [ $? -ne 0 ] ; then service_enable chronyd ; fi                  # Enable chronyd service
    service_restart chronyd                                             # Stop/Start chronyd service

    # Check the time source to ensure the client is reaching to correct NTP server.
    write_log " "
    write_log "***********************"
    write_log "Check the time source to ensure the client is reaching to correct NTP server." 
    write_log "chronyc -n sources" 
    chronyc -n sources | tee -a $LOG_FILE 2>&1

    # Adjust the clock manually without waiting for the next time sync polling
    write_log " "
    write_log " "
    write_log "***********************"
    write_log "Adjust the clock manually without waiting for the next time sync polling."
    write_log "chronyc makestep"
    chronyc makestep | while read wline ; do write_log "$wline"; done

    # See what server chrony is tracking with and performance metrics from that server
    write_log " "
    write_log " "
    write_log "***********************"
    write_log "See what server chrony is tracking with and performance metrics from that server."
    write_log "chronyc tracking"
    chronyc tracking | while read wline ; do write_log "$wline"; done


}



# ==================================================================================================
# Setup chrony server service 
# ==================================================================================================
function s79_setup_chrony_server()
{
    subsection_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION} CHRONY ($CHRONY)"

    D=`date "+%Y/%m/%d %H:%M"`                                          # Date of Change
    service_unmask "chronyd"                                            # In case Chrony Src is mask

    # If on rpm Package type, chrony config file is /etc/chrony.conf
    if [ "$PACKTYPE" = "rpm" ] ; then F="/etc/chrony.conf"        ; fi  # CentOS,Redhat,Fedora,...
    if [ "$PACKTYPE" = "deb" ] ; then F="/etc/chrony/chrony.conf" ; fi  # Debian,Ubuntu,Raspbian,...

    # Create Temporary file with lines that will be added at the end of chrony configuration file.
    echo "# " >$TMP_FILE1                                               # Space line
    echo "# --- Added by $PN v${PIVER} script - $D" >>$TMP_FILE1        # Insert Signature in File
    echo "local stratum 10" >> $TMP_FILE1 
    # sed -i "s/#local stratum 10/local stratum 10/g" /etc/chrony.conf
    # sed -i "s/#allow 192.168.0.0\/16/allow 192.168.0.0\/16/" /etc/chrony.conf
    # To enable a local server, specify the network and subnet to allow connections from
    echo "allow $NETWORK_WITH_MASK" >> $TMP_FILE1                       # Net Allow to Query NTP Srv
    echo "# " >>$TMP_FILE1                                              # Space line

    # Update chrony configuration file
    write_log "Updating chrony configuration file ($F)."
    write_log " "
    write_log "Lines added to $F :" 
    cat $TMP_FILE1                                                      # Show user lines added
    cat $TMP_FILE1 >> $F                                                # Add Lines in chrony.conf
    
    write_log " "
    write_log "Content of $F after modification :"                      # Advise what is following
    write_log "**********"
    cat  $F | while read wline ; do write_log "$wline"; done            # Show file final content
    write_log "**********"

    # Firewall rules
    write_log "Update Firewall rules."
    write_log "firewall-cmd --permanent --add-service=ntp"
    firewall-cmd --permanent --add-service=ntp
    if [ $? -eq 0 ] ;then write_log "Rules added with success." ;fi     
    if [ $? -ne 0 ] ;then write_err "[ ERROR ] Adding firewall rules." ;fi 
    write_log "firewall-cmd --reload"
    firewall-cmd --reload
    if [ $? -eq 0 ] ;then write_log "Firewall reloaded with success." ;fi     
    if [ $? -ne 0 ] ;then write_err "[ ERROR ] Reloading firewall rules." ;fi 

    service_enable  chronyd                                             # Enable chronyd service
    service_restart chronyd                                             # Stop/Start chronyd service

    # command to check how many clients are accessing the NTP server.
    write_log "Check how many clients are accessing the NTP Server (chronyc clients)"
    chronyc clients
}




# ==================================================================================================
# Setup timeone
# ==================================================================================================
s80_setup_timezone()
{
    section_title "${FUNCNAME[0]} - $OS_NAME"

    write_log "Actual TimeZone, running timedatectl :"
    timedatectl | while read wline ; do write_log "$wline"; done

    write_log " "
    write_log "List /etc/localtime" 
    ls -l /etc/localtime | while read wline ; do write_log "$wline"; done
    write_log " "
    if [ -f /usr/share/zoneinfo/America/Montreal ] 
        then write_log "ln -sf /usr/share/zoneinfo/America/Montreal /etc/localtime"
             ln -sf /usr/share/zoneinfo/America/Montreal /etc/localtime
        else if [ -f /usr/share/zoneinfo/America/Toronto ] 
                then write_log "ln -sf /usr/share/zoneinfo/America/Toronto /etc/localtime"
                     ln -sf /usr/share/zoneinfo/America/Toronto /etc/localtime
                else write_err "[ ERROR ] TimeZone could not be set."
                     write_log "Use 'timedatectl list-timezones' to list available timezone."
             fi 
    fi 
    write_log " "
    write_log "List /etc/localtime" 
    ls -l /etc/localtime | while read wline ; do write_log "$wline"; done

    write_log " "
    write_log "timedatectl set-timezone America/Montreal"
    timedatectl set-timezone America/Montreal

#    service_restart systemd-timesyncd
    write_log " "
    write_log "timedatectl --adjust-system-clock"
    timedatectl --adjust-system-clock | while read wline ; do write_log "$wline"; done

    write_log " "

    write_log "TimeZone after modification, running timedatectl : "
    timedatectl | while read wline ; do write_log "$wline"; done
}



# ==================================================================================================
# Setup Postfix Configuration
# ==================================================================================================
s81_setup_postfix()
{
    F="/etc/postfix/main.cf"                                        # Postfix Main Config file
    section_title "${FUNCNAME[0]} - $OS_NAME - Update $F"           # Write std Section Heading

    # If file not exist, not install
    if [ ! -f "$F" ] ; then write_err "No postfix configuration file '$F'." ; return 0 ; fi  
    if [   -f "$F" ] ; then save_original "$F" ; fi                 # Save Original before update

    # Create password file and encrypt it
    F="/etc/postfix/sasl_passwd" 
    write_log "Create password file $F and encrypt it."
    write_log "$MAIL_RELAYHOST ${MAIL_SENDER}:${MAIL_PASSWD}"
    echo "$MAIL_RELAYHOST ${MAIL_SENDER}:${MAIL_PASSWD}" >$F
    write_log "postmap $F"
    postmap $F 

    write_log "chmod -v 600 $F"                 ; chmod -v 600 $F 
    if [ $? -ne 0 ] ; then write_err "[ ERROR ] Changing permission 600 on ${F}"            ;fi
    write_log "chmod -v 600 ${F}.db"            ; chmod -v 600 ${F}.db 
    if [ $? -ne 0 ] ; then write_err "[ ERROR ] Changing permission 600 on ${F}.db"         ;fi
    write_log "chown root:root ${F} ${F}.db"    ; chown root:root ${F} ${F}.db
    if [ $? -ne 0 ] ; then write_err "[ ERROR ] Changing owner root:root to ${F} & ${F}.db" ;fi

    # Remove and add lines that we need to configurate
    F="/etc/postfix/main.cf"                                            # Postfix Main Config file
    sed -i '/^relayhost/d' $F                                           # Delete '^relayhost' line
    echo "relayhost = $MAIL_RELAYHOST"  >> $F                           # Add my relay host
    #postconf -e "relayhost = [smtp.gmail.com]:587"                     # Another way doing it

    sed -i '/^smtp_use_tls/d' $F 
    echo "smtp_use_tls = yes" >> $F
    #postconf -e "smtp_use_tls = yes"

    sed -i '/^smtp_sasl_auth_enable/d' $F 
    echo "smtp_sasl_auth_enable = yes" >> $F 
    #postconf -e "smtp_sasl_auth_enable = yes"

    sed -i '/^smtp_sasl_security_options/d' $F 
    echo "smtp_sasl_security_options = noanonymous" >> $F 
    #postconf -e "smtp_sasl_security_options = noanonymous"

    sed -i '/^smtp_sasl_password_maps/d' $F 
    echo "smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd" >> $F 
    #postconf -e "smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd"

    sed -i '/^myorigin/d' $F 
    echo "myorigin  = ${DOMAIN}" >> $F                         
    sed -i '/^myhostname/d' $F 
    echo "myhostname = ${HOSTNAME}.${DOMAIN}" >> $F 
    sed -i '/^inet_interfaces/d' $F 
    echo "inet_interfaces = all" >> $F 
    sed -i '/^mynetworks/d' $F 
    echo "mynetworks = 127.0.0.0/8, ${NETWORK_WITH_MASK}" >> $F 
    echo " " >> $F    
    
    write_log " " 
    write_log "Content of file $F " 
    write_log "********************" 
    grep -Ev "^$|^#" $F | nl | while read wline ; do write_log "$wline"; done 
    write_log "********************" 

    # Set configuration file permission and ownership /etc/postfix/main.cf
    write_log " " 
    write_log "chmod 644 $F"
    chmod 644 $F  >> $LOG_FILE 2>&1
    if [ $? -ne 0 ] ; then write_err "[ ERROR ] Changing permission (644) on $F" ;fi
    write_log "chown root:root $F"
    chown root:root $F >> $LOG_FILE 2>&1
    if [ $? -ne 0 ] ; then write_err "[ ERROR ] Changing owner 'root:root' on $F" ;fi

    write_log " " 
    service_enable  postfix
    service_restart postfix
    write_log " " 
}






# ==================================================================================================
# Configure Rsyslog
# ================================================================================================
function s85_setup_rsyslog()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION}" 

    F="/etc/rsyslog.conf" 
    if [ -f $F ] 
       then #write_log "Adding this line at the end of /etc/rsyslog.conf"
            #write_log "Adding '#*.*    @syslog.maison.ca:514'  to /etc/rsyslog.conf"
            # Use Two @ for TCP Protocol
            #echo "*.* @@syslog.maison.ca:514"    >> /etc/rsyslog.conf      
            #write_log " "
            write_log " " 
            write_log "Content of file $F " 
            write_log "********************" 
            grep -Ev "^$|^#" $F | nl | while read wline ; do write_log "$wline"; done 
            write_log "********************" 
            service_enable rsyslog
       else write_log "'$F' file not present, package probably not installed."
    fi 
}




# ==================================================================================================
# Create file for root user
# ==================================================================================================
function s88_create_crontab()
{
    CFILE="/etc/cron.d/sa_system"
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION} - $CFILE" 

    echo "# =========================================================================="      >$CFILE
    echo "# System '$HOSTNAME' specific cron jobs.  "                                       >>$CFILE
    echo "#"                                                                                >>$CFILE
    echo "# Min, Hrs, Date, Mth, Day, User, Script"                                         >>$CFILE
    echo "# 0,7=Sun 1=Mon 2=Tue 3=Wed 4=Thu 5=Fri 6=Sat"                                    >>$CFILE
    echo "# =========================================================================="     >>$CFILE
    echo "# Cron does not get a Path when it is run - So We put a standard PATH here"       >>$CFILE
    echo "PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:"               >>$CFILE
    echo "SADMIN=${SADMIN}"                                                                 >>$CFILE
    echo "SA=${SA}"                                                                         >>$CFILE
    echo "MAILTO=$SYS_EMAIL"                                                                >>$CFILE
    echo "#"                                                                                >>$CFILE
    echo "#"                                                                                >>$CFILE
    echo "# HouseKeeping for our Development Environment."                                  >>$CFILE
    echo "#08 20 * * *  root \${SA}/bin/sa_your_script.sh >/dev/null 2>&1"                  >>$CFILE
    echo "#"                                                                                >>$CFILE

    write_log " "
    write_log "Content of system crontab file $CFILE :"
    write_log "***********************"
    cat  $CFILE | while read wline ; do write_log "$wline"; done
    write_log "***********************"
}





# ==================================================================================================
# Setup for Remote Desktop (XRDP) 
# ==================================================================================================
function s92_setup_xrdp()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION} - $SYS_ARCH" 

    # To correct a problem Under Ubuntu, Debian, Raspbian (Package must be install in certain order)
    if [ "$PACKTYPE" = "deb" ] 
       then write_log "To solve a problem under Ubuntu, Debian, Raspbian, need remove/install package."
            write_log " "
            write_log "apt -y update"
            apt -y update >/dev/null 2>&1
            RC=$? ; write_log "Return code of previous command is $RC" ; write_log " "

            # Packages must be installed in order
            write_log "apt -y remove xrdp vnc4server tightvncserver"
            apt -y remove xrdp vnc4server tightvncserver >/dev/null 2>&1
            RC=$? ; write_log "Return code : $RC" ; write_log " "

            write_log "apt -y install tightvncserver"
            #apt -y install tightvncserver >> $LOG_FILE 2>&1
            apt -y install tightvncserver >/dev/null 2>&1
            RC=$? ; write_log "Return code : $RC" ; write_log " "

            write_log "apt -y install xrdp"
            #apt -y install xrdp >> $LOG_FILE 2>&1
            apt -y install xrdp > /dev/null 2>&1
            RC=$? ; write_log "Return code : $RC" ; write_log " "

            write_log "Make xrdp user part of ssl-cert group" 
            write_log "usermod -a -G ssl-cert xrdp" 
            usermod -a -G ssl-cert xrdp >> $LOG_FILE 2>&1

    fi

    # Remove Authentication popup when run vnc or xrdp
    write_log " "
    write_log "Remove Authentication popup when run vnc or xrdp"
    F="/etc/xdg/autostart/gnome-software-service.desktop"
    if [ -f "$F" ] 
       then write_log "echo 'X-GNOME-Autostart-enabled=false' >> $F"
            echo "X-GNOME-Autostart-enabled=false" >> $F
            write_log " "
            write_log "Content of file $F :"
            write_log "***********************"
            cat  $F | while read wline ; do write_log "$wline"; done
            write_log "***********************"
    fi

    # Make sure Xrdp/VNC Password Dir exist
    write_log " "
    SYS_GROUP=$(id -gn "$SYS_ACCOUNT")                                  # Get group name of SysAdmin
    F="/home/${SYS_ACCOUNT}/.vnc"
    write_log "Making sure $F exist and with right permission."
    if [ ! -d "$F" ]
        then write_log "mkdir -p $F"
             mkdir -p "$F"  >> "$LOG_FILE" 2>&1
    fi
    write_log "chmod 750 '$F'" 
    chmod 750 "$F" >> "$LOG_FILE" 2>&1
    write_log "chown ${SYS_ACCOUNT}:${SYS_GROUP} $F"
    chown "${SYS_ACCOUNT}":"${SYS_GROUP}" "$F" >>"$LOG_FILE" 2>&1
    ls -ld "$F" | while read -r wline ; do write_log "$wline"; done

    # Change Cross Cursor to Normal Arrow Cursor on Desktop (Bug)
    #write_log " "
    #write_log "Change Cross Cursor to Normal Arrow Cursor on Desktop (Bug)"
    #F="/home/${SYS_ACCOUNT}/.xsessionrc"
    #write_log "echo 'xsetroot -cursor_name left_ptr &' > $F"
    #echo "xsetroot -cursor_name left_ptr &" > "$F"
    #chmod 750 "$F" >> "$LOG_FILE" 2>&1
    #chown "${SYS_ACCOUNT}":"${SYS_GROUP}" "$F" >> "$LOG_FILE" 2>&1
    #write_log " "
    #write_log "Content of file $F :"
    #write_log "***********************"
    #cat  "$F" | while read -r wline ; do write_log "$wline"; done
    #write_log "***********************"

    write_log " "
    service_enable xrdp                                                 # Enable xrdp start on boot
    service_restart xrdp                                                # Start xrdp 
}



# ==================================================================================================
# Make sure login on console are not in GUI but in ASCII mode
# ==================================================================================================
function s93_turn_off_graphical_login()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION} - $SYS_ARCH" 
    write_log "Login in graphical mode : "
    write_log "  - systemctl set-default -f graphical.target"
    write_log " "
    write_log "Login in text mode : "
    write_log "  - systemctl set-default -f multi-user.target"
    write_log " "
    write_log "To set login in text mode : "
    write_log "  - systemctl set-default -f multi-user.target"
    write_log " "
    write_log "Default in now set to : "
    systemctl get-default  | while read wline ; do write_log "$wline"; done
    write_log " "
}




# ==================================================================================================
# Setup Firewall, For the moment leave it OFF
# ==================================================================================================
function s94_setup_firewall()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION} - $SYS_ARCH" 

   # Firewall Setting for each Platform
    case "$OS_NAME" in
        "REDHAT"|"CENTOS"|"ALMA"|"ROCKY"|"FEDORA")      
                write_log "Disabling Firewall on ${OS_NAME}"
                service_disable firewalld                               # Disable Firewall
                write_log "Masking firewalld - Prevent other Service to Start it"   
                write_log "systemctl mask --now firewalld"              # Mask the service
                systemctl mask --now firewalld                          # Without this start on boot
                write_log " "
                ;; 
        "UBUNTU"|"DEBIAN"|"MINT"|"RASPBIAN")
                dpkg -l | grep ufw > /dev/null 2>&1
                if [ $? -eq 0 ] 
                   then write_log "Disabling ufw firewall on ${OS_NAME}"
                        service_disable ufw 
                        write_log "ufw status"
                        ufw status  >>"$LOG_FILE" 2>&1
                        write_log " "
                   else write_log "ufw firewall is not installed."
                fi 
                ;;
        "*" )   write_log "Firewall setting on '$OS_NAME' are left untouch."
                write_log " "
                ;;
    esac
}



# ==================================================================================================
# Disable unneeded Services
# ==================================================================================================
function s96_disable_unneeded_services()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION} - $SYS_ARCH" 

    # Disable unwanted service.
    systemctl is-enabled iscsi >/dev/null 2>&1
    if [ $? -eq 0 ] ; then service_disable iscsi ; service_stop iscsi ; fi 

    systemctl is-enabled iscsid >/dev/null 2>&1
    if [ $? -eq 0 ] ; then service_disable iscsid ; service_stop iscsid ; fi   

    systemctl is-enabled multipath  >/dev/null 2>&1
    if [ $? -eq 0 ] ; then service_disable multipathd ; fi     # Multipath Daemon Off

    write_log "" 
    subsection_title "List Running Services"
    write_log "systemctl list-units --type=service --state=running"
    systemctl list-units --type=service --state=running |nl |while read wline ; do write_log "$wline"; done
    #
    subsection_title "List Enabled Services"
    write_log "systemctl list-unit-files --type=service --no-pager | grep 'enabled' | nl" 
    systemctl list-unit-files --type=service --no-pager | grep 'enabled' | nl |while read wline ; do write_log "$wline"; done
    #
}


# ================================================================================================
# To prevent your Linux machine from suspending and hibernating, 
# disable the following targets at the systemd level:
# ================================================================================================
function s98_disable_hibernation_for_server()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION} - $SYS_ARCH" 

    # Continue only if on a laptop
    if [ "$LAPTOP" = "Y" ] 
        then write_log "For laptop, leave hibernation, suspend and sleep service active." 
             return 0 
    fi 

    # Mask Suspend, Sleep and Hibernation
    write_log "For Server & Desktop deactivate hibernation, suspend and sleep service." 
    write_log "systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target"
    systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target
    if [ $? -ne 0 ] 
       then write_err "[ ERROR ] Masking Suspend, Sleep and Hibernation on system."
            return 1
    fi 

    write_log " "
    write_log "Note: If doing it on command line, run the following command to apply the changes:"
    write_log "'systemctl restart systemd-logind.service'"
    return 0
}




# ==================================================================================================
# Remove certains package for security reason
# ==================================================================================================
function s100_remove_packages()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION} - $SYS_ARCH" 

    # Special Setup for specific O/S
    case "$OS_NAME" in
        "REDHAT"|"CENTOS"|"ALMA"|"ROCKY"|"FEDORA") 
                                write_log "No packages to remove for ${OS_NAME} for the moment." 
                                write_log " "
                                ;;
        "RASPBIAN")             write_log "Removing wolfram-engine and wolframscript packages ..."
                                apt -y remove wolfram-engine wolframscript >/dev/null 2>&1
                                write_log "Removing minecraft-pi package ..."
                                apt -y remove minecraft-pi  >/dev/null 2>&1
                                ;;
        "UBUNTU"|"DEBIAN"|"MINT")
                                write_log "No packages to remove for ${OS_NAME} for the moment."
                                write_log " "
                                ;;
        "*" )                   write_log "No packages to remove for ${OS_NAME} for the moment." 
                                write_log " "
                                ;;
    esac
}




# ==================================================================================================
# Remove unwanted files
# ==================================================================================================
function s103_remove_unwanted_files()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION} - $SYS_ARCH" 
    FILE_REMOVED=0                                                      # No File Deleted Flag

    # Remove default ReaR cron file.
    F="/etc/cron.d/rear"                                                # ReaR Default Cron Job
    if [ -f "$F" ]                                                      # If crontab job exist
        then write_log "Remove ReaR default cron job file ($F)."        # Advise User
             rm -f $F > /dev/null 2>&1                                  # Delete file
             FILE_REMOVED=1                                             # Activate File remove Flag 
             write_log " "
    fi       

    #F="/etc/cron.daily/00-logwatch" 
    #if [ -f "$F" ]                                                      # If crontab job exist
    #    then write_log "Remove logwatch cron file ($F)."                # Advise User
    #         rm -f $F > /dev/null 2>&1                                  # Delete file
    #         FILE_REMOVED=1                                             # Activate File remove Flag 
    #         write_log " "
    #fi       

    if [ "$FILE_REMOVED" -eq 0 ] ; then write_log "No file(s) to remove for the moment." ; fi 
}





# ==================================================================================================
# Remove unwanted users and groups (For security reason)
# ==================================================================================================
function s105_remove_unwanted_users_and_groups()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION} - $SYS_ARCH" 

    # Remove unwanted users (if they exist)
    id uucp >/dev/null 2>&1     && s106_remove_user "uucp"     
    id news >/dev/null 2>&1     && s106_remove_user "news" 
    #id games >/dev/null 2>&1    && s106_remove_user "games"
    id sync >/dev/null 2>&1     && s106_remove_user "sync" 
    id shutdown >/dev/null 2>&1 && s106_remove_user "shutdown" 
    id halt >/dev/null 2>&1     && s106_remove_user "halt" 
    id operator >/dev/null 2>&1 && s106_remove_user "operator" 

    # Remove unwanted group (if they exist)
    id -g news  >/dev/null 2>&1 && s107_remove_group "news"  
    id -g uucp  >/dev/null 2>&1 && s107_remove_group "uucp"  

}



# ==================================================================================================
# Remove user name received
# ==================================================================================================
function s106_remove_user()
{
    wusr=$1
    id "$wusr" >/dev/null 2>&1                                          # User exist ?
    if [ $? -eq 0 ]                                                     # If User Exist 
        then write_log "userdel $wusr"                                  # Show Removing User 
             userdel "$wusr" >> "$LOG_FILE" 2>&1                        # Remove user
             if [ $? -ne 0 ] ; then write_err "Error removing user '$wusr'." ; fi 
        else write_err "Can't delete non existing user '$wusr'"
    fi

}



# ================================================================================================
# Remove group name received
# ================================================================================================
function s107_remove_group()
{
    wgrp=$1
    id -g "$wgrp" >/dev/null 2>&1                                       # Group exist ?
    if [ $? -eq 0 ]                                                     # If Group Exist 
        then write_log "groupdel $wgrp"                                 # Show Removing Group 
             groupdel "$wgrp" >> "$LOG_FILE" 2>&1                       # Remove Group
             if [ $? -ne 0 ] ; then write_err "Error removing group '$wgrp'." ; fi 
        else write_err "Can't delete non existing group '$wgrp'"
    fi

}



# ==================================================================================================
# Special setup for each Linux distribution
# ==================================================================================================
function s110_special_setup()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION} - $SYS_ARCH" 

    case "$OS_NAME" in
        "REDHAT"|"CENTOS"|"ALMA"|"ROCKY"|"FEDORA")      
            write_log "Disable CTRL+ALT+DEL"
            write_log "systemctl disable ctrl-alt-del.target"
            systemctl disable ctrl-alt-del.target
            if [ $? -ne 0 ] ; then write_err "Error disabling ctrl-alt-del." ; fi 
            write_log "systemctl mask ctrl-alt-del.target" 
            systemctl mask ctrl-alt-del.target
            grep -i "CtrlAltDelBurstAction=none" /etc/systemd/system.conf > /dev/null 2>&1
            if [ $? -ne 0 ]
               then echo "CtrlAltDelBurstAction=none" >> /etc/systemd/system.conf 
                    write_log "CtrlAltDelBurstAction=none added to /etc/systemd/system.conf" 
            fi
            ;;

        "RASPBIAN"|"UBUNTU"|"DEBIAN"|"MINT")             
            if [ id pi >/dev/null 2>&1 ] 
                then #write_log "No special Setup for ${OS_NAME}" ; write_log " "
                     write_log "Change 'pi' user shell to nologin ..." 
                     write_log "usermod -s /usr/sbin/nologin pi"
                     usermod -s /usr/sbin/nologin pi >> $LOG_FILE 2>&1
            fi 
            ;;

        * ) 
            write_log "No Special Setup for $OS_NAME"
            ;;
    esac
}



# ==================================================================================================
# Function will Sleep for a number of seconds.
# Parameters:
#   - 1st is the total time to sleep in seconds.
#   - 2nd is the interval in seconds, user will be showed the remaining seconds at each interval.
# ==================================================================================================
function s115_sleep() {
    SLEEP_TIME=$1                                                       # Nb. Sec. to Sleep 
    SLEEP_INTERVAL=$2                                                   # Interval show user count
    TIME_LEFT=$SLEEP_TIME                                               # Time Slept in Seconds
    printf "%d" $TIME_LEFT                                              # Time left to sleep
    while [ $TIME_LEFT -gt 0 ]                                          # Loop Sleep time Exhaust
        do
        sleep $SLEEP_INTERVAL                                           # Sleep Interval Nb. Seconds
        TIME_LEFT=$(( $TIME_LEFT - $SLEEP_INTERVAL ))                   # Inc Slept Time by Interval
        printf "...%s" $TIME_LEFT                                       # Indicate Sec. Slept
        done
}


# ==================================================================================================
# End of Script - Send log to Sysadmin by email
# ==================================================================================================
function s120_end_of_script()
{
    section_title "${FUNCNAME[0]} - $OS_NAME - V${OS_VERSION} - $SYS_ARCH" 

    write_log " - Preparing to send the install logs to $SYS_EMAIL ."
    write_log " - Reboot 30 seconds after the email is sent."
    write_log " "
    service_restart "postfix"  

    # Rapatriate the SADMIN setup log at the same time
    cd /root || exit
    if [ -f "$SADMIN/setup/log/sadm_setup.log" ] 
        then cp $SADMIN/setup/log/*.log /root 
    fi
    
    # List the logs that will emailed.
    write_log " "
    write_log "List of the logs included in the email."
    ls -l *.log | nl | tee -a "$LOG_FILE"

    write_log " "
    TGZ_NAME="$(date '+%Y_%m_%d')_sadm_postinstall_${OS_NAME}_$(hostname -s).tgz"
    write_log " "
    write_log "Creating the tar compressed file : '$TGZ_NAME'"
    write_log "Sending $TGZ_NAME to '$SYS_EMAIL'" 
    write_log "Will reboot in 30 seconds - End of Post-Install."
    write_log " "
    tar -cvzf $TGZ_NAME ./*.log
    #ls -lh $TGZ_NAME | tee -a "$LOG_FILE"

    # Send email to Sysadmin
    body="Logs of post_installation ran on $(hostname -s)."             # Body of email.
    ws="PostInstall logs of $(hostname -s) on $(date '+%Y_%m_%d')"      # Subject of email.
    echo "$body" | mutt -s "$ws" "$SYS_EMAIL" -a "$TGZ_NAME" 
    RC=$?

    write_log " "
    write_log "Status return code of Mutt is $RC"
    sleep 10 ; write_log " " ; mailq ; 
    write_log " "
    write_log "20 seconds before reboot"
    sync; sync; sync; sync;
    s115_sleep 20 2                                                     # Let Time to send email
    shutdown --reboot now                                               # Shutdown / Reboot
}


# ==================================================================================================
#                          S T A R T   O F   T H E   M A I N   S C R I P T
# ==================================================================================================
    
    # First Clear all Logs
    if [ -f "$LOG_FILE" ] ; then rm -f "$LOG_FILE" >/dev/null 2>&1 ;fi  # Delete previous log if any
    if [ -f "$ERR_FILE" ] ; then rm -f "$ERR_FILE" >/dev/null 2>&1 ;fi  # Del. previous Err. log

    # If current user is not 'root', exit to O/S with error code 1
    if ! [ $(id -u) -eq 0 ]                                           # If Cur. user is not root 
        then printf "Script can only be run by the root user, process aborted.\n"
             exit 1                                                     # Exit To O/S
    fi

    # Read SADMIN Configuration file and put value in Global Variables.
    a10_set_env                                                         # Set Script Env. Variables
    a12_load_config_file                                                # Load sadm_postinstall.cfg
    a14_cmd_options "$@"                                                # Check command-line Options   
    a16_show_env                                                        # Show Script Env. Variables
    a20_validation                                                      # Prompt before & check tgz
    
    # Network Sections
    n08_set_selinux                                                     # Disable SELinux if needed
    n10_setup_host_name                                                 # Set /etc/hosts /etc/hostname
    n20_setup_network                                                   # Set network interface file
    n30_setup_resolv_conf                                               # Setup /etc/resolv.conf
    n40_setup_swap_space                                                # Setup Swap Space
    #n50_update_etc_host_conf                                           # Prioritize host over DNS

    # O/S Update, Add repositories/PPA, install favorites software 
    s02_perform_os_update                                               # Install latest O/S update
    s08_add_epel_repository                                             # Add EPEL on CentOS/RHEL
    s12_install_additional_software                                     # Inst. Rear,postfix,chrony
    s20_add_vscode_repository                                           # Add Microsoft VsCode Repo 

    # Setup Users and Groups
    s30_create_nologin_shell                                            # Create nologin shell
    s34_create_standard_groups                                          # Create sadmin group
    s38_create_standard_users                                           # Create jacques user
    s42_create_user_ssh_keys "sadmin"                                   # Create sadmin SSH key
    s42_create_user_ssh_keys "$SYS_ACCOUNT"                             # Create SysAdmin SSH Key
    s43_create_sudoer_file                                              # Create sudo file 033_sad..

    # Setup Custom Configuration files and directories
    s50_create_mnt_mount_point                                          # Create /mnt mount point
    s54_update_etc_issue "/etc/issue"                                   # Update /etc/issue
    s54_update_etc_issue "/etc/issue.net"                               # Update /etc/issue.net
    s58_update_etc_motd                                                 # Create standard /etc/motd 
    s62_extract_std_files                                               # Extract /home /root
    s65_ssh_configuration                                               # Setup SSH Config files.
    n67_save_sysctl_conf                                                # Create backup sysctl.conf
    s70_rear_site_conf                                                  # Setup ReaR Config files.
    s74_cfg2html_site_file                                              # Setup cfg2html site file.
    s77_setup_chrony                                                    # Setup Chrony service
    s80_setup_timezone                                                  # Setup the timezone
    s81_setup_postfix                                                   # Setup postfix cfg & service
    s85_setup_rsyslog                                                   # Configure rsyslog.conf
    s88_create_crontab                                                  # Create system cron template
    s92_setup_xrdp                                                      # Setup for Remote Desktop
    s93_turn_off_graphical_login                                        # Turn off login in GUI Mode
    s94_setup_firewall                                                  # Setup Firewall 
    s96_disable_unneeded_services                                       # Disable unused Services
    s98_disable_hibernation_for_server                                  # Disable Server Hibernation

    # Securing the Environment
    s100_remove_packages                                                # Remove for security reason
    s103_remove_unwanted_files                                          # Remove unwanted files
    #s105_remove_unwanted_users_and_groups                               # Del unneeded users & group

    # Special Setup for specific O/S
    s110_special_setup                                                  # Special Distribution Setup 

    # End of Post-installation - Send Email
    s120_end_of_script                                                  # Send Email to SysAdmin
